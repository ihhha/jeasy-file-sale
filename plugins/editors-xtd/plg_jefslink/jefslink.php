<?php
/**
 * @version		JEasy File Sale Editor Button Plugin v1.0.1
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:joomalungma@gmail.com Support Email
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgButtonJefsLink extends JPlugin
{
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param 	object $subject The object to observe
	 * @param 	array  $config  An array that holds the plugin configuration
	 * @since 1.5
	 */
	function plgButtonJefsLink(& $subject, $config)
	{
		parent::__construct($subject, $config);
	}

	function onDisplay($name)
	{
		$mainframe = &JFactory::getApplication();

		$doc 		=& JFactory::getDocument();
		$template 	= $mainframe->getTemplate();
		$style	= '.button2-left .jefslink { background: url('.JURI::root().'administrator/components/com_jefs/assets/images/aclink-button.png) 100% 0 no-repeat; } ';
		$doc->addStyleDeclaration($style);

		$link = 'index.php?option=com_jefs&amp;view=files&amp;tmpl=component&amp;e_name='.$name;

		JHTML::_('behavior.modal');

		$button = new JObject();
		$button->set('modal', true);
		$button->set('link', $link);
		$button->set('text', JText::_('JEasy File Sale Link'));
		$button->set('name', 'jefslink');
		$button->set('options', "{handler: 'iframe', size: {x: 570, y: 400}}");

		return $button;
	}
}