﻿<?php
/**
* @version 1.0.1
* @package jefs! QiWi v. 1.0.0
* @copyright (C) 2011 http://joomalungma.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
 *@Platform Joomla 1.7
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.input' );

if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php'))
	require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');

class plgjefsjefsqiwi extends JPlugin
{
	var $_db;
    var $_params;
    var $_input;
	
	function canRun()
	{
        return file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');
	}
    
    function plgjefsjefsqiwi(&$subject, $config)
	{
		parent::__construct($subject, $config);
        
        $this->_plugin =& JPluginHelper::getPlugin('jefs', 'jefsqiwi');
        $this->_params = new JRegistry;
        $this->_params->loadString($this->_plugin->params, 'JSON');
        $this->_input = new JInput();
        if (!$this->canRun()) return;
		jefsHelper::addPlugin('QIWI', 'jefsqiwi');
		$this->_db = JFactory::getDBO();
        jefsHelper::addLogger();
	}

    function onPurchase($type,$acid,$url,$source = '')
    {
        jefsHelper::logEntry('qiwi: OnPurchase - started',JLog::INFO,'payment');
        $mainframe = JFactory::getApplication();

        $params =JComponentHelper::getParams('com_jefs');		// get params
        // check if user is logged in, if not they will be sent to login/registration
        $user		=JFactory::getUser();
        $userid		= (int) $user->get('id', 0);
        $loginlink = $params->def('loginlink', 'index.php?option=com_users&view=login');

        // Get siteroot used for links
        $uri =JFactory::getURI();
        $uriroot = $uri->root();
        $siteroot = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));

        $db =JFactory::getDBO();
        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($acid).' AND published = '.$db->quote(1);
        $db->setQuery( $query );
        $file = $db->loadObject();

        if($file->link_type == 'inherit') {
            $file = jefsHelper::inheritParent($file, $file->parent);
            $type = $file->link_type;
        }

        $testing = $this->_params->def('sandbox', '1');
        $price_options = unserialize($file->price_options);


        require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
        $priceHelper = new PriceHelper();
        $currencycode = JRequest::getWord('ACcurrency_'.$source.$file->id, $priceHelper->getCurrencyCode($params, $price_options), 'post');

        // Get final price taking into account all options using priceHelper class
        $pricearray = $priceHelper->getPrices($price_options, $currencycode);
        $amount = $pricearray[$currencycode]['amount'];
        // Check donation minimum price and adjust if necessary
        if($file->link_type == 'donate') {
            $donateamount = JRequest::getVar('ACamount_'.$source.$file->id, $amount);
            if($donateamount > $amount || $amount == '') {
                if($currencycode == 'HUF' || $currencycode == 'JPY') $roundby = 0;
                else $roundby = 2;
                $amount = number_format($donateamount, $roundby, '.', '');
            }
        }

        //$cancelurl = $siteroot.'index.php?option=com_jefs&task=cancel&requesturl='.urlencode($url);
        $cancelurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=cancel&requesturl='.urlencode($url));
        jefsHelper::logEntry('qiwi: cancelURL = ' . $cancelurl,JLog::INFO,'payment');
        //if($file->return_page == '1') $url = $sitehost.JRoute::_( 'index.php?option=com_jefs&view=download&acid='.$file->id.':'.$file->alias );
        if($file->return_page == '1') $url = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&view=download&acid='. $file->id );

        switch($type)
        {
            case 'donate':
                $cmd = '_donations';
                break;
            case 'purchase':
            default:
                $cmd = '_xclick';
                break;
        }
        $p = new qiwi($cmd);
        // Until qiwi sort out the donations return url bug, we have to use the following url
        // $returnurl = $siteroot.'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type='.$type;
        $returnurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type='.$type);
        jefsHelper::logEntry('qiwi: returnURL = ' . $returnurl,JLog::INFO,'payment');
        if($testing == '1') {
            // if testing use sandbox details
            $p->qiwi_url = 'https://www.sandbox.qiwi.com/cgi-bin/webscr';
            $p->addVar('business', $this->_params->def('sandboxemail'));
        }
        else {
            $p->qiwi_url = 'https://www.qiwi.com/cgi-bin/webscr';
            $p->addVar('business', $this->_params->def('qiwiemail'));
        }

        //$notifyurl = $siteroot.'index.php?option=com_jefs&task=ipn';
        $notifyurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=validate&payment=jefsqiwi' );
        jefsHelper::logEntry('qiwi: notifyURL = ' . $notifyurl,JLog::INFO,'payment');

        $p->addVar('return', $returnurl);
        $p->addVar('cancel_return', $cancelurl);
        $p->addVar('notify_url', $notifyurl);
        $p->addVar('item_name', $file->title);
        $p->addVar('amount', $amount);
        $p->addVar('currency_code', $currencycode);
        $p->addVar('custom', 'fid='.$acid.'&id='.$userid.'&url='.urlencode($url).'&type='.$type);	// These will be used by the ipn to grant access.

        jefsHelper::logEntry('qiwi: OnPurchase - finished',JLog::INFO,'payment');
        return array('paymenturl' => $p->qiwi_url, 'vars' => $p->vars);

    }

    function validate(){

        jefsHelper::logEntry('qiwi: Validate Started!!!',JLog::INFO,'payment');
        $mainframe = JFactory::getApplication();
            //require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'qiwi.class.php' );
    		$p = new qiwi;
    		$params =JComponentHelper::getParams('com_jefs');		// get params
    		$testing = $this->_params->def('sandbox', '0');

            jefsHelper::logEntry('qiwi: Testing or not. Sandbox = ' . $testing ,JLog::INFO,'payment');

    		if($testing == '1') $p->qiwi_url = 'https://www.sandbox.qiwi.com/cgi-bin/webscr';	// if testing use sandbox details
    		else $p->qiwi_url = 'https://www.qiwi.com/cgi-bin/webscr';

    		if ($p->validate_ipn()){

    			// get file id that was just purchased
    			$return = $p->ipn_data;
    			$data = explode('&',$return['custom']);

    			$item = explode('=',$data[0]);
    			$fileid = $item[1];	// the id

                jefsHelper::logEntry('qiwi: FileID = ' . $fileid ,JLog::INFO,'payment');

    			$user = explode('=',$data[1]);
    			$userid = $user[1];
                jefsHelper::logEntry('qiwi: User ID  = ' . $userid ,JLog::INFO,'payment');

    			$urlparam = explode('=',$data[2]);
    			$url = urldecode($urlparam[1]);
                jefsHelper::logEntry('qiwi: URL  = ' . $url ,JLog::INFO,'payment');

    			$type = explode('=',$data[3]);
    			$type = $type[1];
                jefsHelper::logEntry('qiwi: Type  = ' . $type ,JLog::INFO,'payment');

    			$amount = $return['mc_gross'];
    			$currency = $return['mc_currency'];
    			if($currency == 'HUF' || $currency == 'JPY') $roundby = 0;
    			else $roundby = 2;
    			$amount = number_format($amount, $roundby, '.', '');
                jefsHelper::logEntry('qiwi: AMOUNT  = ' . $amount ,JLog::INFO,'payment');

                jefsHelper::fileProcess($fileid,$userid,$amount, $currency,$type, $url,$return, 'qiwi' );
            }
        jefsHelper::logEntry('qiwi: Validate Finished!!!',JLog::INFO,'payment');
    }


	function getLimitations()
	{
        $lang = JFactory::getLanguage();
        $lang->load(strtolower('plg_' . $this->_plugin->type . '_' . $this->_plugin->name), JPATH_PLUGINS . DS . $this->_plugin->type . DS . $this->_plugin->name, null, false, false);
        return JText::_('COM_JEFS_qiwi_LIMITATIONS');
	}
	
}