﻿<?php
/**
* @version 1.0.0
* @package jefs! libertyr v. 1.0.0
* @copyright (C) 2011 http://joomalungma.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
 *@Platform Joomla 1.7
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.input' );

if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php'))
	require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');

class plgJefsjefsLibertyr extends JPlugin
{
	var $_db;
    var $_params;
    var $_input;
	
	function canRun()
	{
        return file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');
	}
    
    function plgjefsjefsLibertyr(&$subject, $config)
	{
		parent::__construct($subject, $config);
        
        $this->_plugin =& JPluginHelper::getPlugin('jefs', 'jefslibertyr');
        $this->_params = new JRegistry;
        $this->_params->loadString($this->_plugin->params, 'JSON');
        $this->_input = new JInput();
        if (!$this->canRun()) return;
		jefsHelper::addPlugin('Liberty Reserve', 'jefslibertyr');
		$this->_db = JFactory::getDBO();
	}

    function onPurchase($type,$acid,$url,$source = '')
    {

        $db =JFactory::getDBO();
        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($acid).' AND published = '.$db->quote(1);
        $db->setQuery( $query );
        $file = $db->loadObject();

        if($file->link_type == 'inherit') {
            $file = jefsHelper::inheritParent($file, $file->parent);
            $type = $file->link_type;
        }

        $conf_merchantAccountNumber = $this->_params->get('merchantID');
        $conf_merchantStoreName    = $this->_params->get('storeName');
        $price_options = unserialize($file->price_options);
        $params =JComponentHelper::getParams('com_jefs');		// get params

        require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
        $priceHelper = new PriceHelper();
        $jinput = JFactory::getApplication()->input;
        $rnd = $jinput->getInt('rnd','');
        $currencycode = $jinput->getCmd('ACcurrency_'. $rnd, $priceHelper->getCurrencyCode($params, $price_options));

        // Get final price taking into account all options using priceHelper class
        $pricearray = $priceHelper->getPrices($price_options, $currencycode);
        $amount = $pricearray[$currencycode]['amount'];
        // Check donation minimum price and adjust if necessary
        if($file->link_type == 'donate') {
            $donateamount = JRequest::getVar('ACamount_'.$source.$file->id, $amount);
            if($donateamount > $amount || $amount == '') {
                if($currencycode == 'HUF' || $currencycode == 'JPY') $roundby = 0;
                else $roundby = 2;
                $amount = number_format($donateamount, $roundby, '.', '');
            }
        }
        $user		=JFactory::getUser();
        $userid		= (int) $user->get('id', 0);

        $libertyrUrl = 'https://sci.libertyreserve.com';
        // Get siteroot used for links
        $uri =JFactory::getURI();
        $uriroot = $uri->root();
        $siteroot = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
        $returnurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type='.$type);
        $cancelurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=cancel&requesturl='.urlencode($url));
        $notifyurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=validate&payment=jefslibertyr' );
        $custom = new stdClass();
        $custom->fileid = $acid;
        $custom->userid = $userid;
        $custom->type   = $type;
        $custom->currency = $currencycode;

        $vars = Array(
            'lr_acc'               =>  $conf_merchantAccountNumber,
            'lr_store'             =>  $conf_merchantStoreName,
            'lr_amnt'              =>  $amount,
            'lr_currency'          =>  'LRUSD',
            'lr_comments'          =>  urlencode(json_encode($custom)),
            'lr_status_url'        =>  $notifyurl,
            'lr_status_url_method '=> 'POST',
            'lr_success_url'       =>  $returnurl,
            'lr_success_url_method'=> 'LINK',
            'lr_fail_url'          =>  $cancelurl,
            'lr_fail_url_method'   =>  'LINK',
            'custom_json'          => $custom,
            'custom_url'           =>   urlencode($url),
        );

        jefsHelper::logEntry('libertyr: OnPurchase - finished',JLog::INFO,'payment');
        return array('paymenturl' => $libertyrUrl, 'vars' => $vars);

    }

    function validate(){

        jefsHelper::logEntry('libertyr: Validate Started!!!',JLog::INFO,'payment');
        if (!$this->canRun()) return;
        $skey = $this->_params->def('securitykey');
        $purse = $this->_params->def('purse');

        $post = $this->_input->getArray(array(
                                       'lr_paidto'     => 'string',
                                       'lr_paidby'         => 'string',
                                       'lr_store'        => 'string',
                                       'lr_amnt'       => 'string',
                                       'lr_transfer'     => 'string',
                                       'lr_currency'        => 'string',
                                       'custom_json'           => 'string',
                                       'custom_url'               => 'string',
                                       'lr_encrypted'			=> 'string'
                                   ));
        $baggageConcat = 'custom_json='.$post['custom_json'].';custom_url='.$post['custom_url'];

        foreach($post as $key => $val)
        {
            jefsHelper::logEntry('libertyr: ' . $key . ' = ' . $val,JLog::INFO,'payment');
        }

        $conf_merchantSecurityWord = $this->_params->get('secretWord');
        $str = $post["lr_paidto"].":".
                $post["lr_paidby"].":".
                stripslashes($post["lr_store"]).":".
                $post["lr_amnt"].":".
                $post["lr_transfer"].":".
                $baggageConcat . ":".
                $post["lr_currency"].":".
                $conf_merchantSecurityWord;
        $check = strtoupper(bin2hex(mhash(MHASH_SHA256, $str)));

        jefsHelper::logEntry('libertyr: check hash: ' . $check .' vs ' . $post['LMI_HASH'],JLog::INFO,'payment');
        $conf_merchantAccountNumber = $this->_params->get('merchantID');
        $conf_merchantStoreName    = $this->_params->get('storeName');

        if ((isset($post["lr_paidto"]) &&
            $post["lr_paidto"] == strtoupper($conf_merchantAccountNumber) &&
            isset($post["lr_store"]) &&
            stripslashes($post["lr_store"]) == $conf_merchantStoreName &&
            isset($post["lr_encrypted"]) &&
            $post["lr_encrypted"] == $check))
        {
            $custom = json_decode(urldecode($post['custom_json']));
            $fileid = $custom->fileid;
            $userid = $custom->userid;
            $url = urldecode($post['custom_url']);
            $type = $custom->type;
            $amount = $post['lr_amnt'];
            $currency = $custom->currency;
            $post['payer_email']='';
            $post['txn_id'] = '';
            $post['memo'] = '';

            jefsHelper::fileProcess($fileid,$userid,$amount, $currency,$type, $url,$post, 'libertyr' );

            jefsHelper::logEntry('libertyr: Validate Finished!!!',JLog::INFO,'payment');
        }
        jefsHelper::logEntry('libertyr: Checking failed!!!',JLog::INFO,'payment');
    }


	function getLimitations()
	{
        $lang = JFactory::getLanguage();
        $lang->load(strtolower('plg_' . $this->_plugin->type . '_' . $this->_plugin->name), JPATH_PLUGINS . DS . $this->_plugin->type . DS . $this->_plugin->name, null, false, false);
        return JText::_('COM_JEFS_LIBERTYR_LIMITATIONS');
	}
}