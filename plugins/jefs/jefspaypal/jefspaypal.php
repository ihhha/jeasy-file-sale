﻿<?php
/**
* @version 1.1.2
* @package jefs! PayPal v. 1.1.0
* @copyright (C) 2011 http://joomalungma.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
 *@Platform Joomla 2.5
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.input' );
//JPlugin::loadLanguage('plg_jefs_jefspaypal', JPATH_ADMINISTRATOR);

if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php'))
	require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');

class plgjefsjefspaypal extends JPlugin
{
	var $_db;
    var $_params;
    var $_input;
	
	function canRun()
	{
        return file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');
	}
    
    function plgjefsjefspaypal(&$subject, $config)
	{
		parent::__construct($subject, $config);
        
        $this->_plugin = JPluginHelper::getPlugin('jefs', 'jefspaypal');
        $this->_params = new JRegistry;
        $this->_params->loadString($this->_plugin->params, 'JSON');
        $this->_input = new JInput();
        if (!$this->canRun()) return;
		jefsHelper::addPlugin('Pay Pal', 'jefspaypal');
		$this->_db = JFactory::getDBO();
        jefsHelper::addLogger();
	}

    function onPurchase($type,$acid,$url,$source = '', $nonRegHash='')
    {
        jefsHelper::logEntry('PayPal: OnPurchase - started',JLog::INFO,'payment');
        $mainframe = JFactory::getApplication();

        $params =JComponentHelper::getParams('com_jefs');		// get params
        // check if user is logged in, if not they will be sent to login/registration
        $user		=JFactory::getUser();
        $userid		= (int) $user->get('id', 0);
        $loginlink = $params->def('loginlink', 'index.php?option=com_users&view=login');

        // Get siteroot used for links
        $uri =JFactory::getURI();
        $uriroot = $uri->root();
        $siteroot = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));

        $db =JFactory::getDBO();
        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($acid).' AND published = '.$db->quote(1);
        $db->setQuery( $query );
        $file = $db->loadObject();

        if($file->link_type == 'inherit') {
            $file = jefsHelper::inheritParent($file, $file->parent);
            $type = $file->link_type;
        }

        $testing = $this->_params->def('sandbox', '1');
        $price_options = unserialize($file->price_options);


        require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
        $priceHelper = new PriceHelper();
        $jinput = JFactory::getApplication()->input;
        $rnd = $jinput->getInt('rnd','');
        $currencycode = $jinput->getCmd('ACcurrency_'. $rnd, $priceHelper->getCurrencyCode($params, $price_options));
        // Get final price taking into account all options using priceHelper class
        $pricearray = $priceHelper->getPrices($price_options, $currencycode);
        $amount = $pricearray[$currencycode]['amount'];
        // Check donation minimum price and adjust if necessary
        if($file->link_type == 'donate') {
            $donateamount = JRequest::getVar('ACamount_'.$source.$file->id, $amount);
            if($donateamount > $amount || $amount == '') {
                if($currencycode == 'HUF' || $currencycode == 'JPY') $roundby = 0;
                else $roundby = 2;
                $amount = number_format($donateamount, $roundby, '.', '');
            }
        }

        //$cancelurl = $siteroot.'index.php?option=com_jefs&task=cancel&requesturl='.urlencode($url);
        $cancelurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=cancel&requesturl='.urlencode($url));
        jefsHelper::logEntry('PayPal: cancelURL = ' . $cancelurl,JLog::INFO,'payment');
        //if($file->return_page == '1') $url = $sitehost.JRoute::_( 'index.php?option=com_jefs&view=download&acid='.$file->id.':'.$file->alias );
        if($file->return_page == '1') $url = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&view=download&acid='. $file->id );

        switch($type)
        {
            case 'donate':
                $cmd = '_donations';
                break;
            case 'purchase':
            default:
                $cmd = '_xclick';
                break;
        }
        $vars = Array();
        // Until PayPal sort out the donations return url bug, we have to use the following url
        // $returnurl = $siteroot.'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type='.$type;
        $returnurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type='.$type);

        if(!empty($nonRegHash))$returnurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type=unregistered'.$type);
        jefsHelper::logEntry('PayPal: returnURL = ' . $returnurl,JLog::INFO,'payment');
        if($testing == '1') {
            // if testing use sandbox details
            $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
            $vars['business'] = $this->_params->def('sandboxemail');
        }
        else {
            $paypal_url  = 'https://www.paypal.com/cgi-bin/webscr';
            $vars['business'] = $this->_params->def('paypalemail');
        }

        //$notifyurl = $siteroot.'index.php?option=com_jefs&task=ipn';
        $notifyurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=validate&payment=jefspaypal' );
        jefsHelper::logEntry('PayPal: notifyURL = ' . $notifyurl,JLog::INFO,'payment');


        $vars['rm'] = '2';           	// Return method 0=GET all transactions  1=GET browser redirect no vars  2=POST browser redirect with vars
        $vars['cmd'] = $cmd;
        $vars['return'] =  $returnurl;
        $vars['cancel_return'] =   $cancelurl;
        $vars['notify_url'] =  $notifyurl;
        $vars['item_name'] = $file->title;
        $vars['amount'] =  $amount;
        $vars['currency_code'] = $currencycode;
        $custom = 'fid='.$acid.'&id='.$userid.'&url='.urlencode($url).'&type='.$type;
        if(!empty($nonRegHash))$custom .= '&nonRegHash=' . $nonRegHash;
        $vars['custom'] =  $custom;	// These will be used by the ipn to grant access.

        jefsHelper::logEntry('PayPal: OnPurchase - finished',JLog::INFO,'payment');
        return array('paymenturl' => $paypal_url, 'vars' => $vars);
    }

    function validate(){

        jefsHelper::logEntry('PayPal: Validate Started!!!',JLog::INFO,'payment');
        require_once( JPATH_PLUGINS.DS.'jefs'.DS.'jefspaypal'.DS.'assets'.DS.'ipnlistener.php' );
        $p = new IpnListener();
        $params =JComponentHelper::getParams('com_jefs');		// get params
        $testing = $this->_params->def('sandbox', '0');

        jefsHelper::logEntry('PayPal: Testing or not. Sandbox = ' . $testing ,JLog::INFO,'payment');

        if($testing == '1') $p->use_sandbox = true;

        jefsHelper::logEntry('PayPal: Starting to validate transaction. ',JLog::INFO,'payment');
        try {
            $p->requirePostMethod();
            $verified = $p->processIpn();
        } catch (Exception $e) {
            jefsHelper::logEntry('PayPal: Error - ' . $e->getMessage() ,JLog::INFO,'payment');
            exit(0);
        }
        if ($verified){
            // get file id that was just purchased


            $return = array();
            foreach ($_POST as $field=>$value) {
                $return["$field"] = $value;
                jefsHelper::logEntry('PayPal.class: ' . $field . ' = ' . $value ,JLog::INFO,'payment');
            }
            $data = explode('&',$return['custom']);

            $item = explode('=',$data[0]);
            $fileid = $item[1];	// the id

            jefsHelper::logEntry('PayPal: FileID = ' . $fileid ,JLog::INFO,'payment');

            $user = explode('=',$data[1]);
            $userid = $user[1];
            jefsHelper::logEntry('PayPal: User ID  = ' . $userid ,JLog::INFO,'payment');

            $urlparam = explode('=',$data[2]);
            $url = urldecode($urlparam[1]);
            jefsHelper::logEntry('PayPal: URL  = ' . $url ,JLog::INFO,'payment');

            $type = explode('=',$data[3]);
            $type = $type[1];
            jefsHelper::logEntry('PayPal: Type  = ' . $type ,JLog::INFO,'payment');

            $amount = $return['mc_gross'];
            $currency = $return['mc_currency'];
            if($currency == 'HUF' || $currency == 'JPY') $roundby = 0;
            else $roundby = 2;
            $amount = number_format($amount, $roundby, '.', '');
            jefsHelper::logEntry('PayPal: AMOUNT  = ' . $amount ,JLog::INFO,'payment');
            $hash = '';
            if(!empty($data[4]))
            {
                $hash = explode('=',$data[4]);
                $hash = $hash[1];
                jefsHelper::logEntry('PayPal: Hash  = ' . $hash ,JLog::INFO,'payment');
            }
            jefsHelper::fileProcess($fileid,$userid,$amount, $currency,$type, $url,$return, 'PayPal',$hash);
        }
        jefsHelper::logEntry('PayPal: Validate Finished!!!',JLog::INFO,'payment');
    }


	function getLimitations()
	{
        $lang = JFactory::getLanguage();
        $lang->load(strtolower('plg_' . $this->_plugin->type . '_' . $this->_plugin->name), JPATH_PLUGINS . DS . $this->_plugin->type . DS . $this->_plugin->name, null, false, false);
        return JText::_('COM_JEFS_paypal_LIMITATIONS');
	}

    function getCurrencySupported()
    {
        return array('USD','EUR','AUD','GBP','CAD','CZK','DKK','HKD','HUF','ILS','JPY','MXN','NZD','NOK','PLN','SGD','SEK','CHF');
    }
	
}