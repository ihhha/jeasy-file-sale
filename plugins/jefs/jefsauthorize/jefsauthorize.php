﻿<?php
/*
* @version 1.0.2
* @package jefs! Authorize v. 1.0.2
* @copyright (C) 2011 http://joomalungma.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
 *@Platform Joomla 2.5
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.input' );

if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php'))
		require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');
if (file_exists(JPATH_BASE.DS.'plugins'.DS.'jefs'.DS.'jefsauthorize'.DS.'assets'.DS.'sdk'.DS.'AuthorizeNet.php'))
	require_once(JPATH_BASE.DS.'plugins'.DS.'jefs'.DS.'jefsauthorize'.DS.'assets'.DS.'sdk'.DS.'AuthorizeNet.php');

class plgjefsJefsAuthorize extends JPlugin
{
	var $_db;
    var $_params;
    var $_input;
	
	function canRun()
	{
        if(!function_exists('curl_version')) return false;
        return file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');
	}
    function plgjefsjefsauthorize(&$subject, $config)
	{
		parent::__construct($subject, $config);
        
        $this->_plugin =& JPluginHelper::getPlugin('jefs', 'jefsauthorize');
        $this->_params = new JRegistry;
        $this->_params->loadString($this->_plugin->params, 'JSON');
        $this->_input = new JInput();
        if (!$this->canRun()) return;
		jefsHelper::addPlugin('Authorize.net', 'jefsauthorize');
		$this->_db = JFactory::getDBO();
	}

    function onPurchase($type,$acid,$url,$source = '', $nonRegHash='')
    {

        $db =JFactory::getDBO();
        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($acid).' AND published = '.$db->quote(1);
        $db->setQuery( $query );
        $file = $db->loadObject();

        if($file->link_type == 'inherit') {
            $file = jefsHelper::inheritParent($file, $file->parent);
            $type = $file->link_type;
        }

        $mode = $this->_params->def('mode');
        $apiLoginId = $this->_params->def('apiLoginId');
        $transactionKey = $this->_params->def('transactionKey');


        $price_options = unserialize($file->price_options);
        $params =JComponentHelper::getParams('com_jefs');		// get params

        require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
        $priceHelper = new PriceHelper();
        $jinput = JFactory::getApplication()->input;
        $rnd = $jinput->getInt('rnd','');
        $currencycode = $jinput->getCmd('ACcurrency_'. $rnd, $priceHelper->getCurrencyCode($params, $price_options));

        // Get final price taking into account all options using priceHelper class
        $pricearray = $priceHelper->getPrices($price_options, $currencycode);
        $amount = $pricearray[$currencycode]['amount'];
        // Check donation minimum price and adjust if necessary
        if($file->link_type == 'donate') {
            $donateamount = JRequest::getVar('ACamount_'.$source.$file->id, $amount);
            if($donateamount > $amount || $amount == '') {
                if($currencycode == 'HUF' || $currencycode == 'JPY') $roundby = 0;
                else $roundby = 2;
                $amount = number_format($donateamount, $roundby, '.', '');
            }
        }
        $user		=JFactory::getUser();
        $userid		= (int) $user->get('id', 0);

        // Get siteroot used for links
        $uri =JFactory::getURI();
        $uriroot = $uri->root();
        $siteroot = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
        $returnurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type='.$type);
        if(!empty($nonRegHash))$returnurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type=unregistered'.$type);

        $cancelurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=cancel&requesturl='.urlencode($url));
        $notifyurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=validate&payment=jefsauthorize' );
        $custom = new stdClass();
        $custom->fileid = $acid;
        $custom->userid = $userid;
        $custom->url    = urlencode($url);
        $custom->type   = $type;
        $custom->currency = $currencycode;
        $custom->returnurl = urlencode($returnurl);
        $custom->cancelurl = urlencode($cancelurl);
        $custom->amount = $amount;
        if(!empty($nonRegHash)) $custom->hash = $nonRegHash;

        $test = false;
        $post_url = "https://secure.authorize.net/gateway/transact.dll";
        if(!$mode){
            $post_url = "https://test.authorize.net/gateway/transact.dll";
            $test = true;
        }
        $fp_sequence = "123";
        $time = time();
        jefsHelper::logEntry('Authorize: action = ' . $post_url, JLog::INFO,'payment');
        $fingerprint = AuthorizeNetSIM_Form::getFingerprint($apiLoginId, $transactionKey, $amount, $fp_sequence, $time);
        $sim = new AuthorizeNetSIM_Form(
            array(
            'x_amount'        => $amount,
            'x_fp_sequence'   => $fp_sequence,
            'x_fp_hash'       => $fingerprint,
            'x_fp_timestamp'  => $time,
            'x_relay_response'=> "TRUE",
            'x_login'         => $apiLoginId,
            'x_method'        => 'cc',
            'x_version'       => '3.1',
            'x_show_form'     => 'payment_form',
            'x_test_request'  => $test,
            'x_relay_url'     => $notifyurl,
            'x_jefs_custom'     =>urlencode(json_encode($custom)),
            )
        );
        jefsHelper::logEntry('Authorize: SIM log',JLog::INFO,'payment');
        foreach($sim as $key => $val)
        {
            jefsHelper::logEntry('Authorize: ' . $key . ' = ' . $val,JLog::INFO,'payment');
        }

        $html = '<form method="post" name="jefspayment" action="'. $post_url .'">';
        $html .= $sim->getHiddenFieldString();



        jefsHelper::logEntry('Authorize: OnPurchase - finished',JLog::INFO,'payment');
        return array('html' => $html);

    }

    function validate(){
        if (!$this->canRun()) return;
        jefsHelper::logEntry('Authorize: Validate Started!!!',JLog::INFO,'payment');

        $response = new AuthorizeNetSIM;
        if ($response->isAuthorizeNet())
        {
            jefsHelper::logEntry('Authorize: Response recivied',JLog::INFO,'payment');
          if ($response->approved)
          {
              jefsHelper::logEntry('Authorize: Authorise transaction Approved!!!',JLog::INFO,'payment');
          }
        }
        jefsHelper::logEntry('Authorize: DUMP RESPONSE!',JLog::INFO,'payment');
        foreach($response as $key => $val)
        {
            jefsHelper::logEntry('Authorize: ' . $key . ' = ' . $val,JLog::INFO,'payment');
        }
        jefsHelper::logEntry('Authorize: Custom field jefs_custom = ' . $_POST['jefs_custom'],JLog::INFO,'payment');


        $custom = json_decode(urldecode($response->jefs_custom));

        // check MD5
        $md5hash = $this->_params->def('md5hash');
        $apiLoginId = $this->_params->def('apiLoginId');

        $transactionId = $response->transaction_id;
        $checkstr = $md5hash.$apiLoginId.$transactionId.$custom->amount; // @todo не рекомендуется, Исправить на x_amount
        jefsHelper::logEntry('Authorize: Check string ' . $checkstr ,JLog::INFO,'payment');
        $check = strtoupper(md5($checkstr));
        jefsHelper::logEntry('Authorize: Compare hash ' . $check . ' vs ' . $response->md5_hash,JLog::INFO,'payment');

        $app = JFactory::getApplication();

        if ($check != $response->md5_hash){
            $cancelurl = $custom->cancelurl;
            $cancelurl = urldecode($cancelurl);
            jefsHelper::logEntry('Authorize: Redirect to Cancel url ' . $cancelurl ,JLog::INFO,'payment');
            $app->redirect($cancelurl);
        }
        $returnurl = $custom->returnurl;
        $returnurl = urldecode($returnurl);
        $redirectTo = $this->_params->def('returnURL',$returnurl);

        $fileid = $custom->fileid;
        $userid = $custom->userid;
        $url = urldecode($custom->url);
        $type = $custom->type;
        $amount = $response->amount;
        $currency = $custom->currency;
        $hash = empty($custom->hash) ? '' : $custom->hash;
        $post['payer_email']='';
        jefsHelper::fileProcess($fileid,$userid,$amount, $currency,$type, $url,$response, 'Authorise',$hash,$redirectTo );
        jefsHelper::logEntry('Authorize: Validate Finished!!!',JLog::INFO,'payment');
        jefsHelper::logEntry('Authorize: Redirect to success url ' . $returnurl,JLog::INFO,'payment');
        exit();
    }
    function getLimitations()
	{
        $lang = JFactory::getLanguage();
        $lang->load(strtolower('plg_' . $this->_plugin->type . '_' . $this->_plugin->name), JPATH_PLUGINS . DS . $this->_plugin->type . DS . $this->_plugin->name, null, false, false);
        if(!$this->canRun()) return;
        $text = JText::_('COM_JEFS_AUTHORIZE_LIMITATIONS');
        return $text;
    }
    function getCurrencySupported()
    {
        return array('USD');
    }
}