﻿<?php
/**
* @version 1.0.0
* @package jefs!
* @copyright (C) 2012 http://joomalungma.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
 *@Platform Joomla 2.5
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.input' );

if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php'))
	require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');

class plgjefsjefs2co extends JPlugin
{
	var $_db;
    var $_params;
    var $_input;
	
	function canRun()
	{
        return file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');
	}
    
    function plgjefsjefs2co(&$subject, $config)
	{
		parent::__construct($subject, $config);
        
        $this->_plugin =& JPluginHelper::getPlugin('jefs', 'jefs2co');
        $this->_params = new JRegistry;
        $this->_params->loadString($this->_plugin->params, 'JSON');
        $this->_input = new JInput();
        if (!$this->canRun()) return;
		jefsHelper::addPlugin('2CHECKOUT', 'jefs2co');
		$this->_db = JFactory::getDBO();
	}

    function onPurchase($type,$acid,$url,$source = '', $nonRegHash='')
    {
        $db =JFactory::getDBO();
        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($acid).' AND published = '.$db->quote(1);
        $db->setQuery( $query );
        $file = $db->loadObject();

        if($file->link_type == 'inherit') {
            $file = jefsHelper::inheritParent($file, $file->parent);
            $type = $file->link_type;
        }

        $sid = $this->_params->def('sid');
        $paymentMethod = $this->_params->def('paymentMethod');

        $price_options = unserialize($file->price_options);
        $params =JComponentHelper::getParams('com_jefs');		// get params

        require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
        $priceHelper = new PriceHelper();
        $jinput = JFactory::getApplication()->input;
        $rnd = $jinput->getInt('rnd','');
        $currencycode = $jinput->getCmd('ACcurrency_'. $rnd, $priceHelper->getCurrencyCode($params, $price_options));

        // Get final price taking into account all options using priceHelper class
        $pricearray = $priceHelper->getPrices($price_options, $currencycode);
        $amount = $pricearray[$currencycode]['amount'];
        // Check donation minimum price and adjust if necessary
        if($file->link_type == 'donate') {
            $donateamount = JRequest::getVar('ACamount_'.$source.$file->id, $amount);
            if($donateamount > $amount || $amount == '') {
                if($currencycode == 'HUF' || $currencycode == 'JPY') $roundby = 0;
                else $roundby = 2;
                $amount = number_format($donateamount, $roundby, '.', '');
            }
        }
        $user		=JFactory::getUser();
        $userid		= (int) $user->get('id', 0);

        $coUrl = 'https://www.2checkout.com/checkout/purchase';
        // Get siteroot used for links
        $uri =JFactory::getURI();
        $siteroot = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
        $returnurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type='.$type);

        $custom = new stdClass();
        $custom->fileid = $acid;
        $custom->userid = $userid;
        $custom->url    = urlencode($url);
        $custom->type   = $type;
        $custom->currency = $currencycode;
        if(!empty($nonRegHash))$custom->nonRegHash = $nonRegHash;
        $order_id = md5(implode($custom) . time());
        $vars = Array(
            'sid'    =>  $sid,
            'total'      =>  $amount,
            'cart_order_id'        =>  $order_id,
            'id_type'       =>  1,
            'c_prod'          =>  $acid,
            'c_name'        =>  $file->title,
            'c_description'       =>  $file->filename,
            'c_price'          =>  $amount,
            'pay_method'    => $paymentMethod,
            'custom'           =>  urlencode(json_encode($custom)),
        );
        if($this->_params->def('demoMode'))$vars['demo'] = 'Y';

        jefsHelper::logEntry('Dumping Variable');
        jefsHelper::logEntry($vars);
        jefsHelper::logEntry('2co: OnPurchase - finished',JLog::INFO,'payment');
        return array('paymenturl' => $coUrl, 'vars' => $vars);
    }

    function validate(){

        jefsHelper::logEntry('2co: Validate Started!!!',JLog::INFO,'payment');
        if (!$this->canRun()) return;
        $sword = $this->_params->def('secretWord');
        jefshelper::logEntry('SWord = ' . $sword);

        $sid = $this->_params->def('sid');
        jefshelper::logEntry('SID = ' . $sid);

        $post = $this->_input->getArray(array(
            'sid'    =>  'string',
            'total'      =>  'string',
            'cart_order_id'        =>  'string',
            'id_type'       =>  'string',
            'c_prod'          =>  'string',
            'c_name'        =>  'string',
            'c_description'       =>  'string',
            'c_price'          =>  'string',
            'x_Receipt_Link_URL'=> 'string',
            'pay_method'    => 'string',
            'custom'           =>  'string',
            'key'       => 'string',
            'credit_card_processed' => 'string',
            'order_number'  => 'string',
            'cart_order_id' => 'string',
            'demo'          => 'string'
                    ));
        jefshelper::logEntry('Dumping POST data');
        jefshelper::logEntry($post);
        if($post['demo'] == 'Y') $post['order_number'] = 1;
        $string_to_hash = $sword . $sid . $post['order_number'] . $post['total'];
        $check_key = strtoupper(md5($string_to_hash));
        $custom = json_decode(urldecode($post['custom']));
        jefshelper::logEntry('Dumping Custom data');
        jefshelper::logEntry($custom);

        jefsHelper::logEntry('2co: check hash: ' . $check_key .' vs ' . $post['key'],JLog::INFO,'payment');
        if ($check_key != $post['key']) return;

        $fileid = $custom->fileid;
        $userid = $custom->userid;
        $url = urldecode($custom->url);
        $type = $custom->type;
        if(!empty($custom->nonRegHash)) $nonRegHash = $custom->nonRegHash;
        $amount = $post['total'];
        $currency = $custom->currency;
        $post['payer_email']='';
        $post['txn_id'] = $post['order_number'];
        unset ($post['order_number']);
        $post['memo'] = '';

        jefsHelper::fileProcess($fileid,$userid,$amount, $currency,$type, $url,$post, '2co', $nonRegHash );

        jefsHelper::logEntry('2co: Validate Finished!!!',JLog::INFO,'payment');
    }


	function getLimitations()
	{
        $lang = JFactory::getLanguage();
        $lang->load(strtolower('plg_' . $this->_plugin->type . '_' . $this->_plugin->name), JPATH_PLUGINS . DS . $this->_plugin->type . DS . $this->_plugin->name, null, false, false);
        return JText::_('COM_JEFS_2CO_LIMITATIONS');
	}
    function getCurrencySupported()
    {
        return array('USD','EUR');
    }
    protected function getLanguage()
    {
        $languages = array
        (
            //Chinese – zh, Danish – da, Dutch – nl, French – fr, German – gr, Greek – el, Italian – it, Japanese – jp, Norwegian – no, Portuguese – pt, Slovenian – sl, Spanish – es_ib, Spanish – es_la, Swedish – sv,
        );
    }
}