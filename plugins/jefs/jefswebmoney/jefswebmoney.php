﻿<?php
/**
* @version 1.1.1
* @package jefs! WebMoney v. 1.0.0
* @copyright (C) 2011 http://joomalungma.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
 *@Platform Joomla 1.7
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.input' );

if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php'))
	require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');

class plgjefsjefswebmoney extends JPlugin
{
	var $_db;
    var $_params;
    var $_input;
	
	function canRun()
	{
        return file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');
	}
    
    function plgjefsjefswebmoney(&$subject, $config)
	{
		parent::__construct($subject, $config);
        
        $this->_plugin =& JPluginHelper::getPlugin('jefs', 'jefswebmoney');
        $this->_params = new JRegistry;
        $this->_params->loadString($this->_plugin->params, 'JSON');
        $this->_input = new JInput();
        if (!$this->canRun()) return;
		jefsHelper::addPlugin('Web Money', 'jefswebmoney');
		$this->_db = JFactory::getDBO();
	}

    function onPurchase($type,$acid,$url,$source = '', $nonRegHash='')
    {
        $db =JFactory::getDBO();
        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($acid).' AND published = '.$db->quote(1);
        $db->setQuery( $query );
        $file = $db->loadObject();

        if($file->link_type == 'inherit') {
            $file = jefsHelper::inheritParent($file, $file->parent);
            $type = $file->link_type;
        }

        $purse = $this->_params->def('purse');
        $price_options = unserialize($file->price_options);
        $params =JComponentHelper::getParams('com_jefs');		// get params

        require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
        $priceHelper = new PriceHelper();
        $currencycode = JRequest::getWord('ACcurrency_'.$source.$file->id, $priceHelper->getCurrencyCode($params, $price_options), 'post');

        // Get final price taking into account all options using priceHelper class
        $pricearray = $priceHelper->getPrices($price_options, $currencycode);
        $amount = $pricearray[$currencycode]['amount'];
        // Check donation minimum price and adjust if necessary
        if($file->link_type == 'donate') {
            $donateamount = JRequest::getVar('ACamount_'.$source.$file->id, $amount);
            if($donateamount > $amount || $amount == '') {
                if($currencycode == 'HUF' || $currencycode == 'JPY') $roundby = 0;
                else $roundby = 2;
                $amount = number_format($donateamount, $roundby, '.', '');
            }
        }
        $user		=JFactory::getUser();
        $userid		= (int) $user->get('id', 0);

        $webmoneyUrl = 'https://merchant.webmoney.ru/lmi/payment.asp';
        // Get siteroot used for links
        $uri =JFactory::getURI();
        $uriroot = $uri->root();
        $siteroot = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
        $returnurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type='.$type);
        $cancelurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=cancel&requesturl='.urlencode($url));
        $notifyurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=validate&payment=jefswebmoney' );
        $custom = new stdClass();
        $custom->fileid = $acid;
        $custom->userid = $userid;
        $custom->url    = urlencode($url);
        $custom->type   = $type;
        $custom->currency = $currencycode;
        if(!empty($nonRegHash))$custom->nonRegHash = $nonRegHash;

        $vars = Array(
            'LMI_PAYMENT_AMOUNT'    =>  $amount,
            'LMI_PAYMENT_DESC'      =>  $type,
            'LMI_PAYMENT_NO'        =>  mt_rand(),
            'LMI_PAYEE_PURSE'       =>  $purse,
            'LMI_SIM_MODE'          =>  0,
            'LMI_RESULT_URL'        =>  $notifyurl,
            'LMI_SUCCESS_URL'       =>  $returnurl,
            'LMI_SUCCESS_METHOD'    =>  2, //0,1,2 - “GET”, “POST” или “LINK”
            'LMI_FAIL_URL'          =>  $cancelurl,
            'LMI_FAIL_METHOD '      =>  2, //0,1,2 - “GET”, “POST” или “LINK”
            'PB_ADD_INFO'           =>  urlencode(json_encode($custom)),
        );

        jefsHelper::logEntry('WebMoney: OnPurchase - finished',JLog::INFO,'payment');
        return array('paymenturl' => $webmoneyUrl, 'vars' => $vars);

    }

    function validate(){

        jefsHelper::logEntry('WebMoney: Validate Started!!!',JLog::INFO,'payment');
        if (!$this->canRun()) return;
        $skey = $this->_params->def('securitykey');
        $purse = $this->_params->def('purse');

        $post = $this->_input->getArray(array(
                                       'LMI_PAYMENT_AMOUNT'     => 'string',
                                       'LMI_PAYMENT_NO'         => 'string',
                                       'LMI_SYS_INVS_NO'        => 'string',
                                       'LMI_SYS_TRANS_NO'       => 'string',
                                       'LMI_SYS_TRANS_DATE'     => 'string',
                                       'LMI_PAYER_PURSE'        => 'string',
                                       'LMI_PAYER_WM'           => 'string',
                                       'LMI_HASH'               => 'string',
                                       'PB_ADD_INFO'			=> 'string'
                    ));
        if(JDEBUG)
        {
            foreach($post as $key => $val)
            {
                jefsHelper::logEntry('WebMoney: ' . $key . ' = ' . $val,JLog::INFO,'payment');
            }
        }
        $mode='0';
        if($this->_params->def('mode')== 0) $mode='1';
        $arr = array(
                'LMI_PAYEE_PURSE'       =>  $purse,
                'LMI_PAYMENT_AMOUNT'    =>  $post['LMI_PAYMENT_AMOUNT'],
                'LMI_PAYMENT_NO'        =>  $post['LMI_PAYMENT_NO'],
                'LMI_MODE'              =>  $mode,
                'LMI_SYS_INVS_NO'       =>  $post['LMI_SYS_INVS_NO'],
                'LMI_SYS_TRANS_NO'      =>  $post['LMI_SYS_TRANS_NO'],
                'LMI_SYS_TRANS_DATE'    =>  urldecode($post['LMI_SYS_TRANS_DATE']),
                'LMI_SECRET_KEY'        =>  $skey,
                'LMI_PAYER_PURSE'       =>  $post['LMI_PAYER_PURSE'],
                'LMI_PAYER_WM'          =>  $post['LMI_PAYER_WM'],
                );
        $check = strtoupper(md5(join('', array_values($arr))));
        jefsHelper::logEntry('WebMoney: check hash: ' . $check .' vs ' . $post['LMI_HASH'],JLog::INFO,'payment');
        if ($check != $post['LMI_HASH']) return;
        $custom = json_decode(urldecode($post['PB_ADD_INFO']));
        $fileid = $custom->fileid;
        $userid = $custom->userid;
        $url = urldecode($custom->url);
        $type = $custom->type;
        if(!empty($custom->nonRegHash)) $nonRegHash = $custom->nonRegHash;
        $amount = $post['LMI_PAYMENT_AMOUNT'];
        $currency = $custom->currency;
        $post['payer_email']='';
        $post['txn_id'] = $post['LMI_SYS_TRANS_NO'];
        unset ($post['LMI_SYS_TRANS_NO']);
        $post['memo'] = '';

        jefsHelper::fileProcess($fileid,$userid,$amount, $currency,$type, $url,$post, 'WebMoney', $nonRegHash );

        jefsHelper::logEntry('WebMoney: Validate Finished!!!',JLog::INFO,'payment');
    }


	function getLimitations()
	{
        $lang = JFactory::getLanguage();
        $lang->load(strtolower('plg_' . $this->_plugin->type . '_' . $this->_plugin->name), JPATH_PLUGINS . DS . $this->_plugin->type . DS . $this->_plugin->name, null, false, false);
        return JText::_('COM_JEFS_WEBMONEY_LIMITATIONS');
	}
    function getCurrencySupported()
    {
        return array('RUB');
    }
}