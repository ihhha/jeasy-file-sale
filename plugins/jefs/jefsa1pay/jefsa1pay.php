﻿<?php
/**
* @version 1.1.2
* @package jefs! a1pay v. 1.0.0
* @copyright (C) 2011 http://joomalungma.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
 *@Platform Joomla 2.5
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );
jimport( 'joomla.application.input' );

if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php'))
	require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');

class plgjefsjefsa1pay extends JPlugin
{
	var $_db;
    var $_params;
    var $_input;
	
	function canRun()
	{
        return file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php');
	}
    
    function plgjefsjefsa1pay(&$subject, $config)
	{
		parent::__construct($subject, $config);
        
        $this->_plugin =& JPluginHelper::getPlugin('jefs', 'jefsa1pay');
        $this->_params = new JRegistry;
        $this->_params->loadString($this->_plugin->params, 'JSON');
        $this->_input = new JInput();
        if (!$this->canRun()) return;
		jefsHelper::addPlugin('A1Pay', 'jefsa1pay');
		$this->_db = JFactory::getDBO();
        jefsHelper::addLogger();
	}

    function onPurchase($type,$acid,$url,$source = '', $nonRegHash='')
    {
        jefsHelper::logEntry('a1pay: OnPurchase - started',JLog::INFO,'payment');

        $db =JFactory::getDBO();
        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($acid).' AND published = '.$db->quote(1);
        $db->setQuery( $query );
        $file = $db->loadObject();

        if($file->link_type == 'inherit') {
            $file = jefsHelper::inheritParent($file, $file->parent);
            $type = $file->link_type;
        }

        $key = $this->_params->def('key');
        $price_options = unserialize($file->price_options);
        $params =JComponentHelper::getParams('com_jefs');		// get params

        require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
        $priceHelper = new PriceHelper();
        $jinput = JFactory::getApplication()->input;
        $rnd = $jinput->getInt('rnd','');
        $currencycode = $jinput->getCmd('ACcurrency_'. $rnd, $priceHelper->getCurrencyCode($params, $price_options));

        // Get final price taking into account all options using priceHelper class
        $pricearray = $priceHelper->getPrices($price_options, $currencycode);
        $amount = $pricearray[$currencycode]['amount'];
        // Check donation minimum price and adjust if necessary
        if($file->link_type == 'donate') {
            $donateamount = JRequest::getVar('ACamount_'.$source.$file->id, $amount);
            if($donateamount > $amount || $amount == '') {
                if($currencycode == 'HUF' || $currencycode == 'JPY') $roundby = 0;
                else $roundby = 2;
                $amount = number_format($donateamount, $roundby, '.', '');
            }
        }
        $user		=JFactory::getUser();
        $userid		= (int) $user->get('id', 0);

        $a1payUrl = 'https://partner.a1pay.ru/a1lite/input';

        $custom = new stdClass();
        $custom->fileid = $acid;
        $custom->userid = $userid;
        $custom->url    = urlencode($url);
        $custom->type   = $type;
        $custom->currency = $currencycode;
        if(!empty($nonRegHash))$custom->nonRegHash = $nonRegHash;
        $vars = Array(
            'key'   => $key,
            'cost'  => $amount,
            'name'  => $file->title,
            'order_id'  => 0,
            'comment'  => urlencode(json_encode($custom)),
        );
        jefsHelper::logEntry('A1pay: OnPurchase - finished',JLog::INFO,'payment');
        return array('paymenturl' => $a1payUrl, 'vars' => $vars);
    }

    function validate(){

        jefsHelper::logEntry('A1Pay: Validate Started!!!',JLog::INFO,'payment');
        if (!$this->canRun()) return;
        $skey = $this->_params->def('securitykey');
        $key = $this->_params->def('key');


        $post = $this->_input->getArray(array(
            'tid'     => 'string',
            'name'         => 'string',
            'comment'        => 'string',
            'partner_id'       => 'string',
            'service_id'     => 'string',
            'order_id'        => 'string',
            'type'           => 'string',
            'partner_income'               => 'string',
            'system_income'			=> 'string',
            'check'         => 'string'
        ));
        jefsHelper::logEntry($post);

        $arr = array(
            'tid'            =>  $post['tid'],
            'name'           =>  $post['name'],
            'comment'        =>  $post['comment'],
            'partner_id'     =>  $post['partner_id'],
            'service_id'     =>  $post['service_id'],
            'order_id'       =>  $post['order_id'],
            'type'           =>  $post['type'],
            'partner_income' =>  $post['partner_income'],
            'system_income'  =>  $post['system_income']
        );

        if( $this->_params->get('testmode') ) $arr['test']='1';
        jefsHelper::logEntry($arr);
        jefsHelper::logEntry($this->_params->get('securityKey'));
        $check = md5(join('', array_values($arr)) . $this->_params->get('securityKey'));
        jefsHelper::logEntry('Check sign comparing signature: ' . $check . ' vs ' . $post['check']);
        if ($check != $post['check']) jexit('Signature check error!!!');
        $custom = json_decode(urldecode($post['comment']));
        jefsHelper::logEntry($custom);
        $fileid = $custom->fileid;
        $userid = $custom->userid;
        $url = urldecode($custom->url);
        $type = $custom->type;
        if(!empty($custom->nonRegHash)) $nonRegHash = $custom->nonRegHash;
        $amount = $post['partner_income'];
        $currency = $custom->currency;
        $post['payer_email']='';
        $post['txn_id'] = $post['LMI_SYS_TRANS_NO'];
        //unset ($post['LMI_SYS_TRANS_NO']);
        $post['memo'] = '';

        jefsHelper::logEntry($post);
        jefsHelper::fileProcess($fileid,$userid,$amount, $currency,$type, $url,$post, 'A1Pay', $nonRegHash );

        jefsHelper::logEntry('A1Pay: Validate Finished!!!',JLog::INFO,'payment');
    }

	function getLimitations()
	{
        $lang = JFactory::getLanguage();
        $lang->load(strtolower('plg_' . $this->_plugin->type . '_' . $this->_plugin->name), JPATH_PLUGINS . DS . $this->_plugin->type . DS . $this->_plugin->name, null, false, false);
        return JText::_('COM_JEFS_A1PAY_LIMITATIONS');
	}

    function getCurrencySupported()
    {
        return array('RUB');
    }
}