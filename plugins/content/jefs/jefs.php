<?php
/**
 * @version		AC File Payments Content Plugin v1.0.1
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:joomalungma@gmail.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('=;)');

jimport('joomla.plugin.plugin');


        class plgContentjefs extends JPlugin
{

    public function onContentPrepare($context, &$row, &$params, $page = 0)
	{
        $this->jefs($row, $params,$page);
    }

	function jefs( &$row, &$params, $page=0 )
	{
		if( ! strpos($row->text, '{jefs'))
		{
			//--The tag is not found in content - abort..
			return;
		}
		require_once( JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php' );
		require_once( JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'buttonhelper.class.php' );
		$buttonHelper = new ButtonHelper();	
		
		// Get ID's
		$regex = '/{jefs\s*id=(.*?)}/i';
		preg_match_all( $regex, $row->text, $matches );
		
		if(isset($matches[1])) {
			$db =JFactory::getDBO();
			$ids = implode(',',$matches[1]);
			
			$query = ' SELECT *' .
					 ' FROM #__jefs_files' .
					 ' WHERE id IN('.$ids.')'.
					 ' AND published = '.$db->quote(1);
			$db->setQuery($query);
			$files = $db->loadObjectList('id');
			$buttons = array();
			if($files) {
				foreach($files as $file) {
					$buttons[$file->id] = $buttonHelper->getButton($file);
				}
				foreach($matches[0] as $key => $value) {
					$regex = '/'.$value.'/i';
					$fileid = $matches[1][$key];
					// Fixed in v1.0.1 to stop dollar signs being treated as variables
					if(isset($buttons[$fileid])) $replacement = str_replace('$', '&#36;',$buttons[$fileid]); 
					else $replacement = '';
					$row->text = preg_replace( $regex, $replacement, $row->text );
				}
			}
			
			// Make sure there are no link tags left
			foreach($matches[0] as $key => $value) {
				$regex = '/'.$value.'/i';
				$replacement = '';
				$row->text = preg_replace( $regex, $replacement, $row->text );
			}
		}
	
		return;
	}//function
}