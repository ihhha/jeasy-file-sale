<?php 
/**
 * @version		JEasy File Sale Module v1.0.1
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:joomalungma@gmail.com Support Email
 */

// no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<?php if ($button != '') : ?>
	<div class="acfpgroup<?php echo $params->get( 'moduleclass_sfx' ) ?>">
	
	<?php if ($headerText) : ?>
		<div class="acfpheader<?php echo $params->get( 'moduleclass_sfx' ) ?>">
			<?php echo $headerText ?>
		</div>
	<?php endif; ?>
	
		<div class="acfpitem<?php echo $params->get( 'moduleclass_sfx' ) ?>">
			<?php echo $button; ?>
			<div class="clr"></div>
		</div>
	
	<?php if ($footerText) : ?>
		<div class="acfpfooter<?php echo $params->get( 'moduleclass_sfx' ) ?>">
			 <?php echo $footerText ?>
		</div>
	<?php endif; ?>
	</div>
<?php endif; ?>