<?php
/**
 * @version		JEasy File Sale Module v1.0.1
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:joomalungma@gmail.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

class ModjefsHelper
{

    function getFile($fileid)
    {
        $db = JFactory::getDBO();

        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($fileid).' AND published = '.$db->quote(1);
        $db->setQuery($query);
        $file = $db->loadObject();
		
        return $file;
    }
 
}