<?php
/**
 * @version		JEasy File Sale Module v1.0.1
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:joomalungma@gmail.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

// include the helper file
require_once(dirname(__FILE__).DS.'helper.php');
 
// get parameters from the module's configuration
$fileid = $params->def('fileid', 0);
$headerText	= trim( $params->def( 'header_text' ) );
$footerText	= trim( $params->def( 'footer_text' ) );
 
// get the file to display from the helper
$file = ModjefsHelper::getFile($fileid);

if($file) {
	require_once( JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'jefshelper.class.php' );
	require_once( JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'buttonhelper.class.php' );
	$buttonHelper = new ButtonHelper();	
	$button = $buttonHelper->getButton($file,'mod');
}
else $button = '';

// include the template for display
require(JModuleHelper::getLayoutPath('mod_jefs'));