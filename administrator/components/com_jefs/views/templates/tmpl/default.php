<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');
$purchaseoptions = unserialize($this->templates['purchase']->options);
$emailoptions = unserialize($this->templates['email']->options);
$emailUnregisteredOptions = unserialize($this->templates['emailUnregistered']->options);
$adminemailoptions = unserialize($this->templates['adminemail']->options);
$donateoptions = unserialize($this->templates['donate']->options);
$registeroptions = unserialize($this->templates['register']->options);
$downloadoptions = unserialize($this->templates['download']->options);
$redirectoptions = unserialize($this->templates['redirect']->options);

$defsubject = '[site]: '.JText::_( 'Download Access for' ).' [title]';

// @todo Сделать кнопки добавления в шаблона Purchase & Donate тэга [paymentgateway] для

$js = "function resetDefault( editor ) {
			tinyMCE.getInstanceById(editor).getBody().innerHTML = document.getElementById('def'+editor).innerHTML;
		}
";
$doc =JFactory::getDocument();
$doc->addScriptDeclaration($js);
JHTML::_('behavior.tooltip');


?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<table width="100%">
	<tr>
		<td valign="top">
<?php 
jimport('joomla.html.pane');
$pane =& JPane::getInstance('tabs'); 
echo $pane->startPane( 'pane' );
echo $pane->startPanel( 'Purchase Button', 'purchasetab' ); 
?>
<table width="100%">
		<tr>
			<td valign="top">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Purchase Template' ); ?></legend>
				<?php echo $this->editor->display( 'purchase', $this->templates['purchase']->html, '100%', '450', '40', '20'); ?>
			</fieldset>
			</td>
			<td valign="top" align="center" width="200">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
				<input type="button" value="<<< <?php echo JText::_( 'Title' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[title]', 'purchase');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Filename' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filename]', 'purchase');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Price' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[price]', 'purchase');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Currency List' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[currencylist]', 'purchase');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Button' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[button]', 'purchase');" />
				<br /><br />
				<?php echo JText::_( 'Display Currency Code' ); ?>: <br />
				<?php echo JHTML::_('select.booleanlist', 'purchasecurr_code', 'class="inputbox"', $purchaseoptions['curr_code'] ); ?>
			</fieldset>
			<input type="button" value="<?php echo JText::_( 'Load Factory Default' ); ?>" style="width:160px" onClick="resetDefault( 'purchase' );" />
			</td>
		</tr>
</table>
<?php echo $pane->endPanel(); ?>
<?php echo $pane->startPanel( 'Donate Button', 'donatetab' ); ?>
<table width="100%">
		<tr>
			<td valign="top">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Donate Template' ); ?></legend>
				<?php echo $this->editor->display( 'donate', $this->templates['donate']->html, '100%', '450', '40', '20'); ?>
			</fieldset>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<span style="font-size:120%;"><?php echo '<img src="'.JURI::root().'includes/js/ThemeOffice/tooltip.png" />';  ?>
			&nbsp;
			<?php echo JText::_( 'Note: If inserting a download button, please make sure it is placed <strong>after</strong> all of the following elements' ); ?>: [amountbox], [currencylist], [button]</span>
			</td>
			<td valign="top" align="center" width="200">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
				<input type="button" value="<<< <?php echo JText::_( 'Title' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[title]', 'donate');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Filename' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filename]', 'donate');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Minimum Donation' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[mindonation]', 'donate');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Amount Box' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[amountbox]', 'donate');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Currency List' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[currencylist]', 'donate');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Donate Button' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[button]', 'donate');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Download Button' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[dlbutton]', 'donate');" /><br /><br />
				<?php echo JText::_( 'Display Currency Code' ); ?>: <br />
				<?php echo JHTML::_('select.booleanlist', 'donatecurr_code', 'class="inputbox"', $donateoptions['curr_code'] ); ?>
				<br /><br />
				<?php echo JText::_( 'Amount Box Size' ); ?>: <br />
				<input class="text_area" type="text" name="donatesize" id="donatesize" size="5" maxlength="250" value="<?php echo $donateoptions['boxsize']; ?>" />
				<br /><br />
				<?php echo JText::_( 'Minimum Donation Text' ); ?>: <br />
				<input class="text_area" type="text" name="donatetext" id="donatetext" size="35" maxlength="250" value="<?php echo htmlentities($donateoptions['button_text']); ?>" />
			</fieldset>
			<input type="button" value="<?php echo JText::_( 'Load Factory Default' ); ?>" style="width:160px" onClick="resetDefault( 'donate' );;" />
			</td>
		</tr>
</table>
<?php echo $pane->endPanel(); ?>
<?php echo $pane->startPanel( 'Register Button', 'registertab' ); ?>
<table width="100%">
		<tr>
			<td valign="top">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Register Template' ); ?></legend>
				<?php echo $this->editor->display( 'register', $this->templates['register']->html, '100%', '450', '40', '20'); ?>
			</fieldset>
			</td>
			<td valign="top" align="center" width="200">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
				<input type="button" value="<<< <?php echo JText::_( 'Title' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[title]', 'register');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Filename' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filename]', 'register');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Register Button' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[regbutton]', 'register');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Login Button' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[logbutton]', 'register');" /><br /><br />
				<?php echo JText::_( 'Register Button Text' ); ?>: <br />
				<input class="text_area" type="text" name="registerregtext" id="registerregtext" size="35" maxlength="250" value="<?php echo htmlentities($registeroptions['register_button_text']); ?>" />
				<br />
				<?php echo JText::_( 'Login Button Text' ); ?>: <br />
				<input class="text_area" type="text" name="registerlogtext" id="registerlogtext" size="35" maxlength="250" value="<?php echo htmlentities($registeroptions['login_button_text']); ?>" />
			</fieldset>
			<input type="button" value="<?php echo JText::_( 'Load Factory Default' ); ?>" style="width:160px" onClick="resetDefault( 'register' );" />
			</td>
		</tr>
</table>
<?php echo $pane->endPanel(); ?>
<?php echo $pane->startPanel( 'Download Button', 'downloadtab' ); ?>
<table width="100%">
		<tr>
			<td valign="top">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Download Template' ); ?></legend>
				<?php echo $this->editor->display( 'download', $this->templates['download']->html, '100%', '450', '40', '20'); ?>
			</fieldset>
			</td>
			<td valign="top" align="center" width="200">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
				<input type="button" value="<<< <?php echo JText::_( 'Title' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[title]', 'download');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Filename' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filename]', 'download');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Button' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[button]', 'download');" /><br /><br />
				<?php echo JText::_( 'Button Text' ); ?>: <br />
				<input class="text_area" type="text" name="downloadtext" id="downloadtext" size="35" maxlength="250" value="<?php echo htmlentities($downloadoptions['button_text']); ?>" />
			</fieldset>
			<input type="button" value="<?php echo JText::_( 'Load Factory Default' ); ?>" style="width:160px" onClick="resetDefault( 'download' );" />
			</td>
		</tr>
</table>
<?php echo $pane->endPanel(); ?>
<?php echo $pane->startPanel( 'Download Page', 'downloadpagetab' ); ?>
<table width="100%">
		<tr>
			<td valign="top">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Download Page Template' ); ?></legend>
				<?php echo $this->editor->display( 'downloadpage', $this->templates['downloadpage']->html, '100%', '450', '40', '20'); ?>
			</fieldset>
			</td>
			<td valign="top" align="center" width="200">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
				<input type="button" value="<<< <?php echo JText::_( 'Title' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[title]', 'downloadpage');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'File Name' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filename]', 'downloadpage');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'File Size' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filesize]', 'downloadpage');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Created Date' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[created]', 'downloadpage');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Modified Date' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[modified]', 'downloadpage');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Downloads' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[downloads]', 'downloadpage');" />
			</fieldset>
			<input type="button" value="<?php echo JText::_( 'Load Factory Default' ); ?>" style="width:160px" onClick="resetDefault( 'downloadpage' );" />
			</td>
		</tr>
</table>
<?php echo $pane->endPanel(); ?>
<?php echo $pane->startPanel( 'User Email', 'emailtab' ); ?>
<table width="100%">
		<tr>
			<td valign="top">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Email Template' ); ?></legend>
				<?php echo $this->editor->display( 'email', $this->templates['email']->html, '100%', '450', '40', '20'); ?>
			</fieldset>
			</td>
			<td valign="top" align="center" width="200">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
				<input type="button" value="<<< <?php echo JText::_( 'Title' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[title]', 'email');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Filename' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filename]', 'email');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'User Name' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[user]', 'email');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Site Name' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[site]', 'email');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Web Address' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[website]', 'email');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Site Email' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[email]', 'email');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Download URL' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[url]', 'email');" /><br /><br />
				<?php echo JText::_( 'Email Subject' ); ?>: 
				<?php if($emailoptions['subject'] == '') $emailoptions['subject'] = '[site]: '.JText::_( 'Download Access for' ).' [title]'; ?>
				<input class="text_area" type="text" name="emailsubject" id="emailsubject" size="35" maxlength="250" value="<?php echo $emailoptions['subject']; ?>" />
			</fieldset>
			<input type="button" value="<?php echo JText::_( 'Load Factory Default' ); ?>" style="width:160px" onClick="resetDefault( 'email' ); document.getElementById('emailsubject').value = '<?php echo $defsubject; ?>'" />
			</td>
		</tr>
</table>
<?php echo $pane->endPanel(); ?>
<?php echo $pane->startPanel( 'Unregistered User Email', 'emailtab' ); ?>
<table width="100%">
    <tr>
        <td valign="top">
            <fieldset class="adminform">
                <legend><?php echo JText::_( 'Unregistered Email Template' ); ?></legend>
                <?php echo $this->editor->display( 'emailUnregistered', $this->templates['emailUnregistered']->html, '100%', '450', '40', '20'); ?>
            </fieldset>
        </td>
        <td valign="top" align="center" width="200">
            <fieldset class="adminform">
                <legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
                <input type="button" value="<<< <?php echo JText::_( 'Title' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[title]', 'emailUnregistered');" /><br />
                <input type="button" value="<<< <?php echo JText::_( 'Filename' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filename]', 'emailUnregistered');" /><br />
                <input type="button" value="<<< <?php echo JText::_( 'Site Name' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[site]', 'emailUnregistered');" /><br />
                <input type="button" value="<<< <?php echo JText::_( 'Web Address' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[website]', 'emailUnregistered');" /><br />
                <input type="button" value="<<< <?php echo JText::_( 'Site Email' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[email]', 'emailUnregistered');" /><br />
                <input type="button" value="<<< <?php echo JText::_( 'Download URL' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[url]', 'emailUnregistered');" /><br /><br />
                <?php echo JText::_( 'Email Subject' ); ?>:
                <?php if($emailUnregisteredOptions['subject'] == '') $emailUnregisteredOptions['subject'] = '[site]: '.JText::_( 'Download Access for' ).' [title]'; ?>
                <input class="text_area" type="text" name="emailsubject" id="emailsubject" size="35" maxlength="250" value="<?php echo $emailUnregisteredOptions['subject']; ?>" />
            </fieldset>
            <input type="button" value="<?php echo JText::_( 'Load Factory Default' ); ?>" style="width:160px" onClick="resetDefault( 'email' ); document.getElementById('emailsubject').value = '<?php echo $defsubject; ?>'" />
        </td>
    </tr>
</table>
<?php echo $pane->endPanel(); ?>
<?php echo $pane->startPanel( 'Admin Email', 'adminemailtab' ); ?>
<table width="100%">
		<tr>
			<td valign="top">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Admin Email Template' ); ?></legend>
				<?php echo $this->editor->display( 'adminemail', $this->templates['adminemail']->html, '100%', '450', '40', '20'); ?>
			</fieldset>
			</td>
			<td valign="top" align="center" width="200">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
				<input type="button" value="<<< <?php echo JText::_( 'Title' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[title]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Filename' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filename]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'User Name' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[user]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'User Username' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[username]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'User ID' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[userid]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'User Email' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[useremail]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Payment Email' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[paymentemail]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Transaction ID' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[transactionid]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Time and Date' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[time]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Payment Type' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[type]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Amount Paid' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[amount]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Buyer Instructions' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[instructions]', 'adminemail');" /><br />
				<input type="button" value="<<< <?php echo JText::_( 'All PayPal Data' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[alldata]', 'adminemail');" /><br /><br />
				<?php 	if($adminemailoptions['subject'] == '') $adminemailoptions['subject'] = '[title] '.JText::_( 'Ordered via PayPal' );
				?>
				<?php echo JText::_( 'Email Subject' ); ?>: 
				<input class="text_area" type="text" name="adminemailsubject" id="adminemailsubject" size="35" maxlength="250" value="<?php echo $adminemailoptions['subject']; ?>" />
				<br /><br />
				<?php echo JText::_( 'Users to Email' ); ?>: <br />
				<?php echo $this->lists['adminemailto']; ?><br />
				<?php echo JText::_( 'Other Addresses (1 per line)' ); ?>:
				<textarea class="inputbox" rows="5" name="otheradminemailto" id="otheradminemailto" style="width:160px"><?php echo $adminemailoptions['otheremailto']; ?></textarea>
			</fieldset>
			<input type="button" value="<?php echo JText::_( 'Load Factory Default' ); ?>" style="width:160px" onClick="resetDefault( 'email' ); document.getElementById('emailsubject').value = '<?php echo $defsubject; ?>'" />
			</td>
		</tr>
</table>
<?php echo $pane->endPanel(); ?>
<?php echo $pane->startPanel( 'Paypal Redirect Template', 'redirecttab' ); ?>
<table width="100%">
		<tr>
			<td valign="top">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Paypal Redirect Template' ); ?></legend>
				<?php echo $this->editor->display( 'redirect', $this->templates['redirect']->html, '100%', '450', '40', '20'); ?>
			</fieldset>
			</td>
			<td valign="top" align="center" width="200">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
				<input type="button" value="<<< <?php echo JText::_( 'Button' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[button]', 'redirect');" /><br /><br />
				<?php echo JText::_( 'Button Text' ); ?>: <br />
				<input class="text_area" type="text" name="redirecttext" id="redirecttext" size="35" maxlength="250" value="<?php echo htmlentities($redirectoptions['button_text']); ?>" />
			</fieldset>
			<input type="button" value="<?php echo JText::_( 'Load Factory Default' ); ?>" style="width:160px;" onClick="resetDefault( 'redirect' );" />
			</td>
		</tr>
	</table>
<?php echo $pane->endPanel(); ?>
<?php echo $pane->endPane(); ?>
		</td>
		<td valign="top" align="center" bgcolor="#EEEEEE" width="250">
			<h3><?php echo JText::_( 'Button Previews' ); 
			echo '&nbsp;&nbsp;';
			echo JHTML::tooltip(JText::_( 'These are only rough previews of what you might expect your buttons to look like on your site. The final look will depend on the CSS styles used on the front end. Changes will only show here when the templates have been saved.'), JText::_( 'Button Previews' ), 'tooltip.png', ''); ?>
			</h3>
			<fieldset class="adminform" style="background-color:  #FFFFFF;">
				<legend><?php echo JText::_( 'Purchase Button' ); ?></legend>
				<?php echo $this->previews['purchase']; ?>
			</fieldset>
			<fieldset class="adminform" style="background-color:  #FFFFFF;">
				<legend><?php echo JText::_( 'Donate Button' ); ?></legend>
				<?php echo $this->previews['donate']; ?>
			</fieldset>
			<fieldset class="adminform" style="background-color:  #FFFFFF;">
				<legend><?php echo JText::_( 'Register Button' ); ?></legend>
				<?php echo $this->previews['register']; ?>
			</fieldset>
			<fieldset class="adminform" style="background-color:  #FFFFFF;">
				<legend><?php echo JText::_( 'Download Button' ); ?></legend>
				<?php echo $this->previews['download']; ?>
			</fieldset>
		</td>
	</tr>
</table>


<input type="hidden" name="option" value="com_jefs" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="templates" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
<div id="defpurchase" style="display:none;"><?php echo $this->templates['purchase']->default; ?></div>
<div id="defdonate" style="display:none;"><?php echo $this->templates['donate']->default; ?></div>
<div id="defregister" style="display:none;"><?php echo $this->templates['register']->default; ?></div>
<div id="defdownload" style="display:none;"><?php echo $this->templates['download']->default; ?></div>
<div id="defdownloadpage" style="display:none;"><?php echo $this->templates['downloadpage']->default; ?></div>
<div id="defemail" style="display:none;"><?php echo $this->templates['email']->default; ?></div>
<div id="defredirect" style="display:none;"><?php echo $this->templates['redirect']->default; ?></div>
