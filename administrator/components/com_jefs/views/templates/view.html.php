<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.view');

class jefsViewTemplates extends JView
{

	function display($tpl = null)
	{
		JToolBarHelper::title(   JText::_( 'JEasy File Sale - Template Manager' ), 'generic.png' );
		JToolBarHelper::save();
		$templates		=$this->get('Data');
		
		// Use defaults if not set
		if($templates['download']->html == '') 		$templates['download']->html = 		$templates['download']->default;
		if($templates['downloadpage']->html == '') 	$templates['downloadpage']->html = 	$templates['downloadpage']->default;
		if($templates['purchase']->html == '') 		$templates['purchase']->html = 		$templates['purchase']->default;
		if($templates['donate']->html == '') 		$templates['donate']->html = 		$templates['donate']->default;
		if($templates['register']->html == '') 		$templates['register']->html = 		$templates['register']->default;
		if($templates['email']->html == '') 		$templates['email']->html = 		$templates['email']->default;
		if($templates['emailUnregistered']->html == '') $templates['emailUnregistered']->html = $templates['emailUnregistered']->default;
		if($templates['adminemail']->html == '') 	$templates['adminemail']->html = 	$templates['adminemail']->default;
		if($templates['redirect']->html == '') 		$templates['redirect']->html = 		$templates['redirect']->default;
		
		$editor =JFactory::getEditor();
		$this->assignRef('editor', $editor);
		$this->assignRef('templates', $templates);
		
		// Build admin email
		$db =JFactory::getDBO();
		$query = ' SELECT id AS value, name AS text' .
				 ' FROM #__users' .
				 ' WHERE sendEmail = '.$db->quote(1).
				 ' AND block != '.$db->quote(1).
				 ' ORDER BY name ASC ';
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		if(!$rows) $rows = array();
		$options = array();
		// $options[] = JHTML::_('select.option', '0', '-- '.JText::_('Select Users').' --');
		$options = array_merge($options, $rows);
		$adminoptions = unserialize($this->templates['adminemail']->options);
		$lists['adminemailto'] = JHTML::_('select.genericlist', $options, 'adminemailto[]', 'class="inputbox" size="6" style="width:160px" multiple', 'value', 'text', $adminoptions['emailto']);
		
		$this->assignRef('lists', $lists);
		
		// Create buttons for preview
		$types = array('download','purchase','donate','register');
		$previews = array();
		foreach($types as $type){
			$options = unserialize($this->templates[$type]->options);
			$currencylist	= $this->getCurrencyList($type);
			$regbutton = '';
			$logbutton = '';
			$dlbutton = '';
			$mindonation = '';
			$preview = '<table width="100%"><tr><td align="center">';
			$preview  .= '<form name="'.$type.'form" action="#" method="get">';
			$purchaseimg = JURI::root().'administrator/components/com_jefs/assets/buttonimages/buynow/rounded_buynow_lg_cc.gif';
			$donateimg = JURI::root().'administrator/components/com_jefs/assets/buttonimages/donate/rounded_donate_lg_cc.gif';
			if($type == 'download') {
				$button = '<input type="submit" name="submitdownload" value="'.$options['button_text'].'" />';
			}
			elseif($type == 'register') {
				$regbutton = '<input type="submit" name="submitregister" value="'.$options['register_button_text'].'" />';
				$logbutton = '<input type="submit" name="submitlogin" value="'.$options['login_button_text'].'" />';
			}
			elseif($type == 'donate') {
				$dloptions = unserialize($this->templates['download']->options);
				$button = '<input type="image" src="'.$donateimg.'" name="image" />';
				$dlbutton = '<input type="submit" name="submitdownload" value="'.$dloptions['button_text'].'" />';
				if($currencylist['amount'] > 0) $mindonation = $options['button_text'];
			}
			else $button = '<input type="image" src="'.$purchaseimg.'" name="image" />';
			
			$template = $templates[$type]->html;
			$template = str_replace('[mindonation]', $mindonation, $template);
			$template = str_replace('[title]', 'File Title', $template);
			$template = str_replace('[filename]', 'filename.zip', $template);
			$template = str_replace('[button]', $button, $template);
			$template = str_replace('[dlbutton]', $dlbutton, $template);
			$template = str_replace('[regbutton]', $regbutton, $template);
			$template = str_replace('[logbutton]', $logbutton, $template);
			$template = str_replace('[price]', $currencylist['price'], $template);
			$template = str_replace('[currencylist]', $currencylist['list'], $template);
			if(isset($options['boxsize'])) {
				$amountbox = '<input type="text" name="amount" id="amount" size="'.$options['boxsize'].'" maxlength="250" value="" />';
				$template = str_replace('[amountbox]', $amountbox, $template);
			}
			
			$preview .= $template;
			$preview .= '</form>';
			$preview .= '</td></tr></table>';
			$previews[$type] = $preview;
		}
		$this->assignRef('previews', $previews);
		
		parent::display($tpl);
	}//function
	
	function getCurrencyList($type) {
		
		if($type != 'donate' && $type != 'purchase') return array('price' => '', 'list' => '');
		
		$default = 'USD';
		
		$options = unserialize($this->templates[$type]->options);
		$displaycode = ($options['curr_code'] == 0) ? false : true;	// Set whether to display the curr code
		if($displaycode == true) {
			$codedisplay = $default.' ';
			$jscodedisplay = 'curr + " " + price';
		}
		else {
			$codedisplay = '';
			$jscodedisplay = 'price';
		}
		
		$currency_array = array(	'Australian Dollar' => 'AUD', 
									'British Pound' => 'GBP', 
									'Canadian Dollar' => 'CAD', 
									'Czech Koruna' => 'CZK', 
									'Danish Krone' => 'DKK', 
									'Euro' => 'EUR', 
									'Hong Kong Dollar' => 'HKD', 
									'Hungarian Forint' => 'HUF', 
									'Israeli New Shekel' => 'ILS', 
									'Japanese Yen' => 'JPY', 
									'Mexican Peso' => 'MXN',
									'New Zealand Dollar' => 'NZD', 
									'Norwegian Krone' => 'NOK', 
									'Polish Zloty' => 'PLN', 
									'Singapore Dollar' => 'SGD', 
									'Swedish Krona' => 'SEK', 
									'Swiss Franc' => 'CHF', 
									'U.S. Dollar' => 'USD', 
									);
		// create a fake pricearray
		$pricearray = array('CAD' => array('amount' => 21.32, 'display' => true), 'GBP' => array('amount' => 12.16, 'display' => true), 'USD' => array('amount' => 20.00, 'display' => true));
		isset($pricearray[$default]['amount']) ? $amount = number_format($pricearray[$default]['amount'],2) : $amount = '0.00';
		$list = '<select id="ACcurrency_'.$type.'" name="ACcurrency_'.$type.'" class="inputbox" onchange="changeACprice_'.$type.'()">';
		foreach($currency_array as $key => $value) {	
			if(isset($pricearray[$value]['display'])) {
				if($default == $value) $list .= '<option value="'.$value.'" selected>'.$key.'</option>';
						else $list .= '<option value="'.$value.'">'.$key.'</option>';
			}
		}
		$list .= '</select>';
		if(!strstr($list,'<option')) {
			// if we have no options then don't display currency but replace with hidden value of currency
			$list = '<input type="hidden" id="ACcurrency_'.$type.'" name="ACcurrency_'.$type.'" value="'.$default.'" />';	
		}
		$symbols = array('AUD' => '$','GBP' => '&pound;','CAD' => '$','CZK' => '&#75;&#269;','DKK'  => '&#107;&#114;', 'EUR' => '&euro;', 'HKD' => '$', 'HUF'  => '&#70;&#116;', 'ILS'  => '&#8362;', 'JPY' => '&yen;', 'MXN' => '$', 'NZD' => '$', 'NOK'  => '&#107;&#114;', 'PLN'  => '&#122;&#322;', 'SGD' => '$','SEK' => '&#107;&#114;', 'CHF'  => '&#67;&#72;&#70;', 'USD' => '$');
		$price = '<span id="ACprice_'.$type.'">'.$codedisplay.$symbols[$default].$amount.'</span>';
		
		$js = 'function changeACprice_'.$type.'()
				{
					curr = document.getElementById("ACcurrency_'.$type.'").value;
					';
		foreach($pricearray as $key => $value) {
			$js .= 'if(curr == "'.$key.'") price = "'.$symbols[$key].$value['amount'].'";';
		}
		$js .= '	document.getElementById("ACprice_'.$type.'").innerHTML = '.$jscodedisplay.';
				}';
		$doc =JFactory::getDocument();
		$doc->addScriptDeclaration($js);
		
		return array('price' => $price, 'list' => $list, 'amount' => $amount);
	
	}

}//class