<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.view');

class jefsViewConfig extends JView
{

	function display($tpl = null)
	{
		JToolBarHelper::title(   JText::_( 'JEasy File Sale - Configuration' ), 'generic.png' );
		JToolBarHelper::preferences( 'com_jefs', '400','570','Configuration Settings' );

		parent::display($tpl);
	}//function

}//class