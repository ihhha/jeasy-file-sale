<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');
$db = JFactory::getDbo();
$q = 'SELECT manifest_cache FROM #__extensions WHERE element = "com_jefs"';
$db->setQuery($q);
$db->query();
$r = $db->loadResult();
$params = new JRegistry();
$params->loadString($r, 'JSON');
$ver = $params->def('version');
?>
<div class="width-60 fltlft">
    <fieldset class="adminform">
        <legend>Video Tutorial</legend>
        <div>
            <div class="article-content">
                <iframe width="560" height="315" src="http://www.youtube.com/embed/videoseries?list=PLA8E78E11C8FFB54D&amp;hl=ru_RU&amp;hd=1&amp;wmode=opaque" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </fieldset>
    <fieldset class="adminform">
        <a href="http://www.opensource-excellence.com/index.php?option=com_ose_mart&view=item&id=419&oafid=28776&obid=17" target="_blank">
            <img src="<?php echo JURI::base();?>components/com_jefs/assets/images/jefs-ose.png" alt="OSE Membership"/>
        </a>
    </fieldset>
</div>
<div class="width-40 fltrt">
    <fieldset class="adminform">
        <legend>About</legend>
        <div align="left">
            <h3 align="left">JEasy File Sale by <a href="http://joomalungma.com" target="_blank">Joomalungma</a><br />
          Version <?php echo $ver;?> </h3>
          <h3 align="left">Description</h3>
            <p>JEasy File Sale helps you to provide files to your users in a variety of secure ways. Whether you are asking for payment or a donation, or you require the user to register before they can download, you can restrict downloading until they have made the required action. Once a user has paid/donated/registered they will then have access to the file(s). You can even create an individual download page and add any other information or files they might require. Only users with authorised access will be able to download the files on offer.<br /></p>
        </div>
            <h3 align="left">Information</h3>
          <p align="left">
          If you require more information or help on using JEasy File Sale: <br />
          Please email: <a href="mailto:joomalungma@gmail.com">info@joomalungma.com</a> <br />
                or visit <a href="http://joomalungma.com" target="_blank">joomalungma.com</a>.<br />
                </p>
            <h3 align="left">Copyright</h3>

            <p align="left">This software is <a href="http://www.gnu.org/licenses/gpl.html" target="_blank">GPL licensed</a>.<br />
                Modification &copy; 2011 <a href="http://joomalungma.com/" target="_blank">Joomalungma</a>. All Rights Reserved.<br />
            Copyright &copy; 2009 <a href="http://www.angelcoding.com/" target="_blank">Angel Coding</a>. All Rights Reserved.</p>
        <p align="left" style="color:#a52a2a;font-style: italic;">If you like our work please support as on <a href="http://extensions.joomla.org/extensions/e-commerce/paid-downloads/19191" target="_blank">JED</a> with your vote or review!</p>
    </fieldset>
</div>
