<?php
/**
* @version 1.0.0
* @package JooAds!
* @copyright (C) 2011 www.jooads.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
*/
defined('_JEXEC') or die('Restricted access');
$n = count($this->payments);
if($n) {
?>
<p><strong><?php echo JText::_('COM_JEFS_PAYMENT_INTEGRATIONS_MESSAGE'); ?></strong></p>
<form action="<?php echo JRoute::_('index.php?option=com_jooads&view=payments'); ?>" method="post" name="adminForm" id="adminForm">
	<div id="editcell1">
		<table class="adminlist">
			<thead>
			    <tr>
                    <th width="5">№</th>
                    <th><?php echo JText::_('COM_JEFS_PAYMENT_TYPE'); ?></th>
                    <th><?php echo JText::_('COM_JEFS_PAYMENT_DESC'); ?></th>
                    <th width="1"><?php echo JText::_('COM_JEFS_PAYMENT_CONFIGURE'); ?></th>
               </tr>
			</thead>
	<?php
	$k = 0;
	$i = 0;
	foreach ($this->payments as $row)
	{
	?>
		<tr class="row<?php echo $k; ?>">
			<td><?php echo $i+1; ?></td>
			<td><?php echo $row->name; ?></td>
			<td><?php echo $row->limitations; ?></td>
			<td align="center"><a href="<?php echo JRoute::_('index.php?option=com_plugins&task=plugin.edit&extension_id='.$row->cid); ?>"><?php echo JHTML::image('administrator/components/com_jefs/assets/images/config.png', JText::_('COM_JEFS_PAYMENT_CONFIGURE')); ?></a></td>
		</tr>
	<?php
		$i++;
		$k=1-$k;
	}
	?>
		</table>
	</div>

	<?php echo JHTML::_( 'form.token' ); ?>
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="option" value="com_jooads" />
	<input type="hidden" name="view" value="payments" />
	<input type="hidden" name="task" value="" />
</form>
<?php } else { ;?>
    <h2><?php echo JText::_('COM_JEFS_NO_PAYMENTS_WARNING');?></h2>
<?php };?>