<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */



defined('_JEXEC') or die('Restricted access');



jimport('joomla.application.component.view');



class jefsViewPayments extends JView

{

		function display($tpl = null)

	{

		$mainframe = JFactory::getApplication();

        JToolBarHelper::title(JText::_( 'COM_JEFS_PAYMENTS' ) ,'jefs');

		 $plugins = jefshelper::getPlugins();

        if(empty($plugin)){}

        $return = array();

		foreach ($plugins as $paymentplugin => $name)

		{

            $tmp = new stdClass();

			$tmp->name 		  = $name;



			$tmp->limitations = '';



			$className = 'plgjefs'.$paymentplugin;

			if (class_exists($className) && method_exists($className, 'getLimitations'))

			{

				$dispatcher  	  =JDispatcher::getInstance();

				$plugin 	 	  = new $className($dispatcher, array());

				$tmp->limitations = $plugin->getLimitations();

			}

            $db =JFactory::getDBO();

			$db->setQuery("SELECT `extension_id` FROM #__extensions WHERE `folder`='jefs' AND `client_id`='0' AND `element`='".$db->escape($paymentplugin)."' LIMIT 1");

			$tmp->cid = $db->loadResult();



			$return[] = $tmp;

        }

        $this->assignRef('payments', $return);

		parent::display($tpl);

    }

}