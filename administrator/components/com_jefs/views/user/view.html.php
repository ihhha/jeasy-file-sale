<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.view');

class jefsViewUser extends JView
{

	function display($tpl = null)
	{
		$user		=$this->get('Data');
		$isNew		= ($user->id < 1);

		$text = $isNew ? JText::_( 'New User' ) : JText::_( 'Edit' ).' '.$user->name;
		JToolBarHelper::title(  JText::_( 'JEasy File Sale - User Manager' ).': <small><small>[ ' . $text.' ]</small></small>' );
		JToolBarHelper::save();
		JToolBarHelper::apply();
		if ($isNew)  {
			JToolBarHelper::cancel();
		} else {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancel', JText::_('Close') );
		}
		
		// prepare files list
		$db =JFactory::getDBO();
		$query = 'SELECT id AS value, title AS text ' .
				 'FROM #__jefs_files ' .
				 'ORDER BY title ASC ';
		$db->setQuery($query);
		$files = $db->loadObjectList('value');
		
		if(!$files) $files = array();
		// build form control
		$lists['file_access'] = JHTML::_('select.genericlist', $files, 'file_access[]', 'class="inputbox" size="10" style="width:160px" multiple', 'value', 'text', $user->file_access);
		
		// Create array to display donations in a nicer manner i.e. File Title -> Currency -> Amount
		$donations = array();
		foreach($user->donations as $currency => $value) {
			foreach($value as $fileid => $amount) {
				$donations[$files[$fileid]->text][$currency] = $amount;
			}
		}
		ksort($donations);	// sort the array by the key i.e. the file title

		$this->assignRef('user', $user);
		$this->assignRef('lists', $lists);
		$this->assignRef('donations', $donations);

		parent::display($tpl);
    }// function
	
}// class
