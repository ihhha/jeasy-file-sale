<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col100">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'User Details' ); ?></legend>

		<table class="admintable">
		<tr>
			<td width="100" align="right" class="key">
				<label for="id">
					<?php echo JText::_( 'User ID' ); ?>:
				</label>
			</td>
			<td>
				<strong><?php echo $this->user->id;?></strong>
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="name">
					<?php echo JText::_( 'Name' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->user->name;?>
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="username">
					<?php echo JText::_( 'Username' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->user->username;?>
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="email">
					<?php echo JText::_( 'Email' ); ?>:
				</label>
			</td>
			<td>
				<a href="mailto:<?php echo $this->user->email;?>"><?php echo $this->user->email;?></a>
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="file_access">
					<?php echo JText::_( 'File Access' ); ?>:<br />
					(<?php echo JText::_( 'Multiple Select' ); ?>)
				</label>
			</td>
			<td>
				<?php echo $this->lists['file_access']; ?>
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="name">
					<?php echo JText::_( 'Donations' ); ?>:
				</label>
			</td>
			<td>
				<?php 
					if(count($this->donations) > 0) {
						echo '<table><tr align="center" style="background-color:#CCCCCC"><td>'.JText::_( 'File' ).'</td><td>'.JText::_( 'Amount' ).'</td></tr>';
						foreach($this->donations as $file => $value) {
							echo '<tr><td>';
							echo '<strong>'.$file.'</strong>';
							echo '</td><td>';
							foreach($value as $curr => $amount) {
								echo $curr.' '.$amount.'<br />';
							}
							echo '</td></tr>';
						}
						echo '</table>';
					}
					else echo JText::_( 'No Donations Made' );
					
				?>
			</td>
		</tr>
	</table>
	</fieldset>
</div>
<div class="clr"></div>

<input type="hidden" name="option" value="com_jefs" />
<input type="hidden" name="id" value="<?php echo $this->user->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="users" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>