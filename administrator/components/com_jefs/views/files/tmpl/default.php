<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

?>
<form action="index.php" method="post" name="adminForm">
<div id="editcell">
	<table>
		<tr>
			<td align="left" width="100%">
				<?php echo JText::_('Search by Title or Filename'); ?>:
				<input type="text" name="filter_search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="document.adminForm.filter_search.value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
			</td>
			<td nowrap="nowrap">
				<?php echo $this->lists['state']; ?>
				<?php echo $this->lists['link_type']; ?>

			</td>
		</tr>
	</table>

	<table class="adminlist">
	<thead>
		<tr>
			<th width="5">
						<?php echo JText::_( 'Num' ); ?>
			</th>
			<th width="20">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>			
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'Title' ), 'title', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'File Path' ), 'filename', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'Published' ), 'published', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'Downloads' ), 'downloads', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'Purchases' ), 'purchases', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'Link Type' ), 'link_type', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th width="5">
				<?php echo JHTML::_('grid.sort', JText::_( 'ID' ), 'id', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
		</tr>			
	</thead>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->items ); $i < $n; $i++)
	{
		$row = &$this->items[$i];
		$checked 	= JHTML::_('grid.id',   $i, $row->id );
		$link 		= JRoute::_( 'index.php?option=com_jefs&amp;controller=files&amp;task=edit&amp;cid[]='. $row->id.'&amp;'.JUtility::getToken().'=1' );
//		$uri = JFactory::getURI();
//		$root = $uri->root();
//		$root = rtrim($root,'/');
//		$downloadlink = str_replace(JPATH_ROOT,$root,$row->path);
//		$downloadlink = str_replace(DS,'/',$downloadlink);
//		$downloadlink = $downloadlink.'/'.$row->filename;	
//		<small><a href=" echo $downloadlink; " target="_blank">[Download]</a></small>
		$published 	= JHTML::_('grid.published', $row, $i );
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td>
				<?php echo $checked; ?>
			</td>
			<td>
				<a href="<?php echo $link; ?>"><?php echo $row->title; ?></a>
			</td>
			<td>
				<?php echo $row->path.DS; ?><strong><?php echo $row->filename; ?></strong>
			</td>
			<td align="center">
				<?php echo $published;?>
			</td>
			<td>
				<?php echo $row->downloads; ?>
			</td>
			<td>
				<?php echo $row->purchases; ?>
			</td>
			<td>
				<?php echo $row->link_type; ?>
			</td>
			<td>
				<?php echo $row->id; ?>
			</td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	 <tfoot>
    <tr>
      <td colspan="9">
      	<?php echo $this->pagination->getListFooter(); ?>
      </td>
    </tr>
  </tfoot>
	</table>
</div>

<input type="hidden" name="option" value="com_jefs" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="controller" value="files" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
