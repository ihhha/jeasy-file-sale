<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');
?>

<script language="javascript" type="text/javascript">
		function submitbutton(pressbutton) {
			var form = document.adminForm;
			if (pressbutton == 'cancelItem') {
				submitform( pressbutton );
				return;
			}
<?php 
$valuearray = array();
foreach ( $this->items as $item ) {
		$valuearray[] = "!getSelectedValue( 'adminForm', 'files_".$item->id."' )";
}
$selectedValues = implode(' || ', $valuearray);
 ?>
			// do field validation
			if (<?php echo $selectedValues; ?>) {
				alert( "<?php echo JText::_( 'Please select a file from the list/all lists', true ); ?>" );
			} else {
				submitform( pressbutton );
			}
		}
		</script>

<form action="index.php" method="post" name="adminForm">
<?php foreach ( $this->items as $item ) : ?>
	<table class="adminform">
		<tr>
			<td width="3%"></td>
			<td  valign="top" width="30%">
			<strong><?php echo JText::_( 'File Access Rights being copied' ); ?>:</strong>
			<br />
			<ol>
				
				<li><?php echo $item->title; ?> (ID: <?php echo $item->id; ?>)</li>
				
			</ol>
			</td>
			<td  valign="top">
			<strong><?php echo JText::_( 'Copy to File(s)' ); ?>:</strong>
			<br />
			<?php echo JHTML::_('select.genericlist',  $this->files, 'files_'.$item->id.'[]', 'class="inputbox" size="10" style="width:200px;" multiple', 'value', 'text', null ); ?>
			<br /><br />
			</td>
		</tr>
	</table>
	<input type="hidden" name="cid[]" value="<?php echo $item->id; ?>" />
<?php endforeach; ?>

	<input type="hidden" name="option" value="com_jefs" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="controller" value="files" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
