<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');
JHTML::_('behavior.tooltip');

$document =JFactory::getDocument();
$js = "
var assetsBase = '".JURI::base()."components/com_jefs/assets';
var _LOADING_ = '".JText::_('Loading')."';
var _SAVING_ = '".JText::_('Saving')."';
";
$document->addScriptDeclaration( $js );
$document->addScript( JURI::base(true).'/components/com_jefs/assets/ajax.js' );

$typevalue = $this->file->link_type;	//  payment type = purchase (default), donate, register, download, inherit
?>

<form action="index.php" xmlns="http://www.w3.org/1999/html" method="post" name="adminForm" id="adminForm">
<div class="width-60 fltlft">
<?php
jimport( 'joomla.html.html.tabs' );
echo JHtml::_('tabs.start', 'file-edit',array('coockies'=>1));
echo JHtml::_('tabs.panel', JText::_('JEFS_FILE_DETAILS'),'details');
?>
<fieldset>
    <legend><?php echo JText::_( 'JEFS_FILE_ID' ); ?> - <?php if ($this->file->id < 1) echo JText::_( 'JEFS_FILE_NO_ID' );
    else echo $this->file->id; ?></legend>
    <ul class="adminformlist">
        <li>
            <label><?php echo JText::_( 'JEFS_FILE_TITLE' ); ?></label><input class="text_area" type="text" name="title" id="title" size="60" maxlength="250" value="<?php echo $this->file->title;?>" />
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_FILE_TYPE' ); ?></label><select name="link_type" id="link_type" size="1">
                                                                            <option value="purchase" <?php if ($typevalue == 'purchase') echo 'selected="selected"'; ?>><?php echo JText::_( 'JEFS_FILE_PURCHASE' ); ?></option>
                                                                            <option value="donate" <?php if ($typevalue == 'donate') echo 'selected="selected"'; ?>><?php echo JText::_( 'JEFS_FILE_DONATE' ); ?></option>
                                                                            <option value="register" <?php if ($typevalue == 'register') echo 'selected="selected"'; ?> ><?php echo JText::_( 'JEFS_FILE_REGISTER' ); ?></option>
                                                                            <option value="download" <?php if ($typevalue == 'download') echo 'selected="selected"'; ?> ><?php echo JText::_( 'JEFS_FILE_DOWNLOAD' ); ?></option>
                                                                            <option value="inherit" <?php if ($typevalue == 'inherit') echo 'selected="selected"'; ?> ><?php echo JText::_( 'JEFS_FILE_INHERIT' ); ?></option>
                                                                        </select>
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_FILE_BTN' ); ?></label><?php echo $this->purchasebutton;?>
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_FILE_ALIAS' ); ?></label><input class="text_area" type="text" name="alias" id="alias" size="60" maxlength="250" value="<?php echo $this->file->alias;?>" />
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_PUBLISHED' ); ?>:</label><select name="published" class="inputbox" size="1">
                                                                            <option value="1" selected="<?php echo ($this->file->published) ? 'selected="selected"' : '';?>"><?php echo Jtext::_('Published');?></option>
                                                                            <option value="0" <?php echo (!$this->file->published) ? 'selected="selected"' : '';?>"><?php echo Jtext::_('Unpublished');?></option>
                                                                        </select>
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_FILE_NAME' ); ?>:</label><?php echo $this->filehtml;?>
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_FOLDER_PATH' ); ?></label><input class="text_area" type="text" name="path" id="path" size="60" maxlength="250" value="<?php echo $this->file->path;?>" readonly="readonly" />
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_FILE_SIZE' ); ?></label><input class="readonly" value="<?php if(!empty($this->file->filename) && $this->file->filename != '') echo jefshelper::parseSize(filesize($this->file->path.DS.$this->file->filename)); ?>" readonly="readonly"/>
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_CREATED' ); ?>:</label><?php 	$attribs = array('size' => '20');
            $jdate = JFactory::getDate($this->file->created);
            $date = $jdate->toSQL();
            echo JHTML::calendar($date,'created','created','%Y-%m-%d %H:%M:%S',$attribs);
            ?>
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_MODIFIED' ); ?></label><?php
            $this->file->auto_modified == '1' ? $modified = time() : $modified = $this->file->modified;
            $jdate = JFactory::getDate($modified);
            $date = $jdate->toSQL();
            echo JHTML::calendar($date,'modified','modified','%Y-%m-%d %H:%M:%S',$attribs);
            ?>
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_NUMBER_OF_PUR_DON' ); ?></label><input type="text" class="readonly" readonly="readonly" name="purchases" id="purchases" value="<?php echo $this->file->purchases;?>" />
            <input type="button" value="<?php echo JText::_( 'JEFS_RESET' );?>" onClick="document.getElementById('purchases').value = '0'; document.getElementById('purchasesText').style.display = 'block';" />&nbsp;
            <input id="purchasesText" value="<?php echo JText::_( 'You must hit save to store this value' );?>" class="readonly" readonly="readonly" style="display: none;"/>
        </li>
        <li>
            <label><?php echo JText::_( 'Downloads' ); ?></label><input type="text" class="readonly" name="downloads" id="downloads" value="<?php echo $this->file->downloads;?>" readonly="readonly"/>
            <input type="button" value="<?php echo JText::_( 'JEFS_RESET' );?>" onClick="document.getElementById('downloads').value = '0'; document.getElementById('downloadsText').style.display = 'block';" />
            <input id="downloadsText" value="<?php echo JText::_( 'You must hit save to store this value' );?>" class="readonly" readonly="readonly" style="display: none;"/>
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_ADD_TO_DOWNLOAD' ); ?></label><input class="text_area" type="text" name="downloads_add" id="downloads_add" size="10" maxlength="250" value="<?php echo isset($this->file->downloads_add) ? $this->file->downloads_add : '';?>" />&nbsp;<?php echo JHTML::tooltip(JText::_( 'JEFS_ADD_TO_DOWNLOAD_DESC' ), JText::_( 'Inherit Parent' ), 'tooltip.png', '');?>
        </li>
        <li>
            <label><?php echo JText::_( 'JEFS_FILE_PARENT' ); ?></label><?php echo $this->lists['parent']; ?><?php echo JHTML::tooltip(JText::_('JEFS_FILE_INHERIT_DESC'), JText::_( 'JEFS_FILE_INHERIT_PARENT' ), 'tooltip.png', ''); ?>
        </li>
    </ul>
</fieldset>



<?php echo JHtml::_('tabs.panel', JText::_('JEFS_FILE_USER_ACCESS'),'useraccess');?>
<table class="admintable">
    <tr>
        <td width="100" align="right" class="key">
            <label for="applyaccess">
                <?php echo JText::_( 'Apply User Access' ); ?>:
            </label>
        </td>
        <td>
            <select id="applyaccess" name="applyaccess" class="inputbox" size="1">
                <option value="1"><?php echo Jtext::_('Yes');?></option>
                <option value="0"  selected="selected"><?php echo Jtext::_('No');?></option>
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <?php echo JHTML::tooltip(JText::_( "Any changes you make to User Access will only be applied if this option is set to Yes. This feature minimizes the risk of overwriting the access to any users that might have made a purchase while you are editing this file. Ideally, if you want to make user access changes, you should make the changes as soon as possible after opening this page and save/apply straight away." ), JText::_( 'Apply User Access' ), 'tooltip.png', ''); ?>
        </td>
    </tr>
    <tr>
        <td width="100" align="right" class="key">
            <label for="path">
                <?php echo JText::_( 'User Access' ); ?>:
            </label>
        </td>
        <td>
            <table>
                <tr>
                    <td align="center">
                        <?php echo JText::_( 'Users <strong>with</strong> access' );?>:
                        <?php echo $this->accesslist; // &#8250;?><br />

                        <input type="button" value="<?php echo JText::_( 'All' );?> >>" onClick="swapSelectOptions('useraccess','usernonaccess', true)" />
                        <input type="button" value="<?php echo JText::_( 'Selected' );?> >" onClick="swapSelectOptions('useraccess','usernonaccess', false)" />
                    </td>
                    <td align="center">
                        <?php echo JText::_( 'Users <strong>without</strong> access' );?>:
                        <?php echo $this->nonaccesslist; // &#8249;?><br />

                        <input type="button" value="< <?php echo JText::_( 'Selected' );?>" onClick="swapSelectOptions('usernonaccess','useraccess', false)" />
                        <input type="button" value="<< <?php echo JText::_( 'All' );?>" onClick="swapSelectOptions('usernonaccess','useraccess', true)" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php echo JHtml::_('tabs.panel', JText::_('JEFS_FILE_PRICING'),'pricing');?>

<table width="50%">
	<tr>
		<td>
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'File Pricing' ); ?></legend>
		<table class="admintable" width="100%">
		<tr style="background-color:#DDDDDD;" valign="bottom">
			<td width="80" align="center">
				<strong><?php echo JText::_( 'Currency' ); ?></strong>

			</td>
			<td width="10" align="center">
				<strong><?php echo JText::_( 'Code' ); ?></strong>
			</td>
			<td width="10" align="center">
				<strong><?php echo JText::_( 'Symbol' ); ?></strong>
			</td>
			<td align="center">
				<strong><?php echo JText::_( 'Price' ); ?> / <?php echo JText::_( 'Donation Minimum' ); ?></strong>
			</td>
			<td align="center">
				<strong><?php echo JText::_( 'Default' ); ?></strong>
			</td>
			<td align="center">
				<strong><?php echo JText::_( 'Display' ); ?></strong>
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->currency_array ); ?>, 'price_options_display');" />
			</td>
		</tr>


<?php 
	
		if (!isset($this->file->price_options['default'])) {
			$this->file->price_options['default'] = $this->defaultcurrency;	
		}
		$i = 0; // Used to increment the  checkbox id for checking all
		
		foreach ($this->currency_array as $key => $value) : 

		if (!isset($this->file->price_options[$value]['display'])) {
			$this->file->price_options[$value]['display'] = '';
		} 
		if (!isset($this->file->price_options[$value]['price'])) {
			$this->file->price_options[$value]['price'] = '';
		} 
		
 ?>

		<tr>
			<td width="80" align="right" class="key">
				<label for="<?php echo $value;?>">
					<?php echo JText::_( $key ); ?>:
				</label>
			</td>
			<td align="center">
				<?php echo $value; ?>
			</td>
			<td align="center">
				<?php echo $this->symbol_array[$value]; ?>
			</td>
			<td align="center">
				<input class="text_area" type="text" name="price_options[<?php echo $value; ?>][price]" id="price_options_price_<?php echo $value; ?>" size="15" maxlength="250" value="<?php echo $this->file->price_options[$value]['price'];?>" />
			</td>
			<td align="center">
				<input type="radio" name="price_options[default]" id="price_options_default_<?php echo $value; ?>" value="<?php echo $value; ?>" <?php if ($this->file->price_options['default'] == $value) echo 'checked="checked"'; ?> />
			</td>
			<td align="center">
				<input type="checkbox" name="price_options[<?php echo $value; ?>][display]" id="price_options_display<?php echo $i; ?>" size="8" maxlength="250" value="<?php echo $value; ?>" <?php if ($this->file->price_options[$value]['display'] == $value) echo 'checked="checked"'; ?> />
			</td>
		</tr>
		
		<?php 
			$i++;
			endforeach; 
		?>
	</table>
	</fieldset>
	</td>
</tr>
</table>
</table>

<?php echo JHtml::_('tabs.panel', JText::_('JEFS_FILE_DOWNLOAD_PAGE'),'download_page');?>
<table width="100%">
		<tr>
			<td valign="top" width="80%">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Download Page' ); ?></legend>
				<?php echo $this->editor->display( 'description', $this->file->description, '100%', '500', '50', '20'); ?>
			</fieldset>
			</td>
			<td valign="center" align="center">
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Options' ); ?></legend>
				<strong>
				<?php echo JText::_( 'Use download page on redirect?' ); ?><br />
                <select name="return_page" class="inputbox" size="1">
                    <option value="1" selected="<?php echo ($this->file->return_page) ? 'selected="selected"' : '';?>"><?php echo Jtext::_('Yes');?></option>
                    <option value="0" <?php echo (!$this->file->return_page) ? 'selected="selected"' : '';?>"><?php echo Jtext::_('No');?></option>
                </select>

				&nbsp;&nbsp;
				<?php echo JHTML::tooltip(JText::_( "If set to yes, the user will be redirected to this download page upon successful purchase. If not, they will be redirected back to the order page they came from." ), JText::_( 'Redirect to Download Page' ), 'tooltip.png', ''); ?>
				<br /><br />
				<?php echo JText::_( 'Allow download from order page?' ); ?><br />
                <select name="allow_download" class="inputbox" size="1">
                    <option value="1" selected="<?php echo ($this->file->allow_download) ? 'selected="selected"' : '';?>"><?php echo Jtext::_('Yes');?></option>
                    <option value="0" <?php echo (!$this->file->allow_download) ? 'selected="selected"' : '';?>"><?php echo Jtext::_('No');?></option>
                </select>
				&nbsp;&nbsp;
				<?php echo JHTML::tooltip(JText::_( "If set to yes, the purchase button will change to a download button upon successful purchase/donation or if the user already has access to the file. If not, the purchase/donation will not change." ), JText::_( 'Allow Download' ), 'tooltip.png', ''); ?>
				<br /><br />
				<?php echo JText::_( "Show this file's children on user downloads page?" ); ?><br />
                <select name="allow_children" class="inputbox" size="1">
                    <option value="1" selected="<?php echo ($this->file->allow_children) ? 'selected="selected"' : '';?>"><?php echo Jtext::_('Yes');?></option>
                    <option value="0" <?php echo (!$this->file->allow_children) ? 'selected="selected"' : '';?>"><?php echo Jtext::_('No');?></option>
                </select>
				&nbsp;&nbsp;
				<?php echo JHTML::tooltip(JText::_( "This affects the user downloads page, which shows the user which downloads they have access to (not to be confused with the download page opposite). If this option is set to yes, all children of this file will also be displayed if the user has access to it." ), JText::_( 'Show Child Files' ), 'tooltip.png', ''); ?>
				</strong>
			</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add Dynamic Elements' ); ?></legend>
				<input type="button" value="<<< <?php echo JText::_( 'Title' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[title]', 'description');" />
				<br />
				<input type="button" value="<<< <?php echo JText::_( 'File Name' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filename]', 'description');" />
				<br />
				<input type="button" value="<<< <?php echo JText::_( 'File Size' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[filesize]', 'description');" />
				<br />
				<input type="button" value="<<< <?php echo JText::_( 'Created Date' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[created]', 'description');" />
				<br />
				<input type="button" value="<<< <?php echo JText::_( 'Modified Date' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[modified]', 'description');" />
				<br />
				<input type="button" value="<<< <?php echo JText::_( 'Downloads' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[downloads]', 'description');" />
				<br />
                <input type="button" value="<<< <?php echo JText::_( 'Register Button' ); ?>" style="width:160px; text-align:left;" onClick="jInsertEditorText('[registerButton]', 'description');" />
                <br />
			</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Add File Links' ); ?></legend>
				<?php echo JText::_( 'Link Text: ( can use [title] )' ); ?><br />
				<input class="text_area" type="text" name="linktext" id="linktext" size="35" maxlength="250" value="<?php echo JText::_( 'Click here to download' ); ?> [title]" /><br /><br />
				<?php echo JText::_( 'Select a file and add link' ); ?>:
				<?php echo JHTML::tooltip(JText::_( "Only children of this file and 'download only' files will appear here" ), JText::_( 'File List' ), 'tooltip.png', ''); ?>
				<br />
				<?php echo $this->lists['files']; ?>
				<br /><br />
				<input type="button" value="<<< <?php echo JText::_( 'Add File Link' ); ?>" style="width:160px" onClick="var list = document.getElementById('files'); var title = document.getElementById('title'); var listindex = list.selectedIndex; if(list.value=='') var name = '[title]'; else var name = list.options[listindex].text; var linktext = document.getElementById('linktext').value; linktext = linktext.replace('[title]',name); jInsertEditorText('<a href=\'[filelink'+list.value+']\'>'+linktext+'</a>', 'description');" />
			</fieldset>
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Defaults' ); ?></legend>
				<input id="loadDefault" type="button" value="<?php echo JText::_('Load Default'); ?>" style="width:160px" />
				<br />
				<input id="saveDefault" type="button" value="<?php echo JText::_('Save as Default'); ?>" style="width:160px" />
				<!-- This div will contain the response from our ajax call -->
				<h2 id="fieldsContainer"></h2>
			</fieldset>
			</td>
		</tr>
</table>
<?php echo JHtml::_('tabs.end');?>
</div>
<input type="hidden" name="option" value="com_jefs" />
<input type="hidden" name="id" value="<?php echo $this->file->id; ?>" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="controller" value="files" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>