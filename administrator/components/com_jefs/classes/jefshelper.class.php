<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');
jimport( 'joomla.error.log' );

class jefshelper
{
    var $_plugins = array();

    
	function parseSize($bytes, $precision = 2)
	{
		$units = array('B', 'KB', 'MB', 'GB', 'TB');
  
		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);
	  
		$bytes /= pow(1024, $pow);
	  
		return round($bytes, $precision) . ' ' . $units[$pow]; 
	
	}
	
	function inheritParent($file,$parentid)
	{
		if($parentid <= 0) return $file;
		$db =JFactory::getDBO();
		$query = ' SELECT *' .
				 ' FROM #__jefs_files' .
				 ' WHERE id = '.$db->quote($parentid);
		$db->setQuery($query);
		$parent = $db->loadObject();
		if(!$parent) return $file;
		// $file->published = $parent->published;
		$file->purchase_button = $parent->purchase_button;
		$file->price_options = $parent->price_options;
//		$file->auto_conv = $parent->auto_conv;
//		$file->auto_conv_amt = $parent->auto_conv_amt;
		$file->link_type = $parent->link_type;
		$file->return_page = $parent->return_page;
		$file->allow_download = $parent->allow_download;
		
		return $file;		
	}
    
    function getSharedContentPlugins()

	{

		jimport('joomla.plugin.helper');



		static $instances;

		if (!is_array($instances))

		{

			$instances = array();

			$dispatcher =JDispatcher::getInstance();

			// Get plugins

			JPluginHelper::importPlugin('jefs');

			$plugins = JPluginHelper::getPlugin('jefs');



			foreach($plugins as $plugin)

			{

				JPluginHelper::importPlugin('jefs', $plugin->name, false);



				$className = 'plgjefs'.$plugin->name;

				if(class_exists($className))

					$instances[] = new $className($dispatcher, (array)$plugin);

			}

		}

        return $instances;

	}


    static function getPluginsSupportedCurrency()
    {
        jimport('joomla.plugin.helper');

        $dispatcher =JDispatcher::getInstance();

        // Get plugins

        JPluginHelper::importPlugin('jefs');

        $plugins = JPluginHelper::getPlugin('jefs');

        $supportedCurrency = array();
        foreach($plugins as $plugin)

        {

            JPluginHelper::importPlugin('jefs', $plugin->name, false);

            $className = 'plgjefs'.$plugin->name;

            if(class_exists($className) && method_exists($className, 'getCurrencySupported'))
            {
                $dispatcher  =JDispatcher::getInstance();
                $plugin      = new $className($dispatcher, array());
                $supportedCurrency[$plugin->_plugin->name] = call_user_func_array(array($plugin, 'getCurrencySupported'), array());
            }
        }
        return $supportedCurrency;
    }

	static function addPlugin($name, $filename)

	{

		$instance =& jefshelper::getInstance();



		$instance->_plugins[$filename] = $name;

	}



	static function getPlugins()

	{

		$instance =& jefshelper::getInstance();



		return $instance->_plugins;

	}



	static function getPlugin($name)

	{

		$instance =& jefshelper::getInstance();



		if (!empty($instance->_plugins[$name]))

			return $instance->_plugins[$name];

		else

			return false;

	}



	function processPluginResult($array)

	{

		if (is_array($array))

		{

			foreach ($array as $item)

				if ($item !== false)

					return $item;

		}

		else

			return $array;

	}



	static function &getInstance()

	{

		static $instance;



		if (!is_object($instance))

			$instance = new jefshelper();



		return $instance;

	}

    static function addLogger()
    {
        $options = array(
            'text_file'             => 'jefs.log.php',
            'logger'                =>  'formattedtext',
            'text_entry_format'     =>  '{DATE}' . chr(9) . '{TIME}' . chr(9) . '{PRIORITY}' . chr(9) . '{CATEGORY}' . chr(9) . '{MESSAGE}',
        );

        $category = array('component', 'payment','module');
        Jlog::addLogger($options, JLog::ALL, $category);
    }

    static function logEntry($entry, $priority='', $cat='')
    {
        if(!JDEBUG) return;
        if(is_array($entry) || is_object($entry))
        {
            JLog::add('--------------');
            JLog::add('DUMP:');
            foreach ($entry as $k => $v)
            {
                JLog::add($k . ' = ' . $v);
            }
            JLog::add('ENDDUMP:');
            JLog::add('--------------');
        }
        else
        {
            JLog::add($entry, $priority, $cat);
        }

    }

    function fileProcess($fileid,$userid,$amount, $currency,$type, $url,$return, $gatewayName,$nonRegHash='',$redirectTo = '' )
    {
        $db = JFactory::getDbo();
        // Get file details
        $mainframe = JFactory::getApplication();
        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($fileid).' AND published = '.$db->quote(1);
        $db->setQuery( $query );
        $file = $db->loadObject();

        if(!$file) return false;

        jefshelper::logEntry('JEFSHELPER: Get file from DB - OK'  ,JLog::INFO,'component');



        if($file->link_type == 'inherit'){
            $file = jefshelper::inheritParent($file, $file->parent);
            $accessid = $file->parent;
        }
        else $accessid = $file->id;
        jefshelper::logEntry('JEFSHELPER: ACCESSID  = ' . $accessid,JLog::INFO,'component');



        // Make sure that what was returned from PayPal matches up and is correct, otherwise suspect foul play and exit ...
        require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
        $priceHelper = new PriceHelper();
        $price_options = unserialize($file->price_options);
        $pricearray = $priceHelper->getPrices($price_options, $currency);
        var_dump($pricearray);
        unset($priceHelper);
        $fileamount = $pricearray[$currency]['amount'];

        jefshelper::logEntry('JEFSHELPER: Check link type:  ' . $type . ' vs ' . $file->link_type,JLog::INFO,'component');
        if($type != $file->link_type) return false;
        jefshelper::logEntry('JEFSHELPER: Check amount:  ' . $fileamount . ' vs ' . $amount,JLog::INFO,'component');
        if($type == 'purchase' && $fileamount != $amount) return false;
        if($type == 'donate' && $fileamount > $amount) return false;
        unset($pricearray);
        $downloadHash = '';
        // Process registered Users
        jefshelper::logEntry('JEFSHELPER: User ID = ' . $userid ,JLog::INFO,'component');
        jefshelper::logEntry('JEFSHELPER: NonRegHash = ' . $nonRegHash ,JLog::INFO,'component');
        if( $userid != '0' )
        {
            jefshelper::logEntry('JEFSHELPER: Process registered User',JLog::INFO,'processing');
            $query = 'SELECT * FROM #__jefs_users WHERE id = '.$db->quote($userid);
            $db->setQuery( $query );
            $userrow = $db->loadObject();

            // If the user doesn't exist in ACFP db then create a new ACFP user if they're not a guest user
            if(!$userrow && $userid > 0) {
                $newaccessarray = array('files' => '');
                $userobject = new stdClass();
                $userobject->id = $userid;
                $userobject->access = serialize($newaccessarray);
                $userobject->donations = '';
                $db->insertObject('#__jefs_users', $userobject, 'id');
                unset($userobject);
                // Run query again
                $query = 'SELECT * FROM #__jefs_users WHERE id = '.$db->quote($userid);
                $db->setQuery( $query );
                $userrow = $db->loadObject();
            }

            $access = unserialize($userrow->access);
            // make sure the user doesn't already have access (it would be hard for someone to pay more than once, but lets check anyway)
            // if the file data has been inherited then it's the parent that receives access if it hasn't already
            if(!in_array($accessid, $access['files'])) {
                $access['files'][] = $accessid;
                sort($access['files']);
            }

            // Process user donations
            if($userrow->donations == '') $userdonations = array();
            else $userdonations = unserialize($userrow->donations);
            if($type == 'donate') {
                if(isset($userdonations[$currency][$fileid])) $donation = $amount + $userdonations[$currency][$fileid];
                else $donation = $amount;
                $userdonations[$currency][$fileid] = $donation;
            }

            // update the user database to grant access and add any donation info
            $userobject = new stdClass();
            $userobject->id = $userid;
            $userobject->access = serialize($access);
            $userobject->donations = serialize($userdonations);
            $db->updateObject('#__jefs_users', $userobject, 'id');


            // Process file donations
            if($file->donations == '') $filedonations = array();
            else $filedonations = unserialize($file->donations);
            if($type == 'donate') {
                if(isset($filedonations[$currency][$userid])) $donation = $amount + $filedonations[$currency][$userid];
                else $donation = $amount;
                $filedonations[$currency][$userid] = $donation;
            }

            // Update file database with purchase amount and donation info
            $fileobject = new stdClass();
            $fileobject->id = $fileid;
            $fileobject->purchases = $file->purchases + 1;
            $fileobject->donations = serialize($filedonations);
            $db->updateObject('#__jefs_files', $fileobject, 'id');

            $query = 'SELECT * FROM #__users WHERE id = '.$db->quote($userid);
            $db->setQuery( $query );
            $user = $db->loadObject();
            $template = 'email';
            //Process Not registered User
        }
        else
        {
            jefshelper::logEntry('JEFSHELPER: Process Not Registered User',JLog::INFO,'processing');
            // Update file database with purchase amount and donation info
            $downloadHash = md5($nonRegHash . time());
            $db->setQuery("UPDATE #__jefs_nonregistered SET `status` = '1',`downloadHash`='$downloadHash' WHERE `hash` = '$nonRegHash' LIMIT 1");
            if(!$db->query()) JefsHelper::logEntry('JEFSHELPER: DB Error ' . $db->getErrorMsg(),JLog::INFO,'processing');

            $db->setQuery("SELECT `email` FROM #__jefs_nonregistered WHERE `hash` = '$nonRegHash' LIMIT 1");
            $email = $db->loadResult();
            JefsHelper::logEntry('JEFSHELPER: Unregistered user Email = ' . $email,JLog::INFO,'processing');
            $user = new stdClass();
            $user->name = JText::_('JEFS_UNREGISTERED_USER');
            $user->id = 0;
            $user->username = JText::_('JEFS_UNREGISTERED_USER');
            $user->email = $email;
            $url = JURI::base() . JRoute::_('/index.php?option=com_jefs&task=download&downloadHash='.$downloadHash);
            $template = 'emailUnregistered';
            jefshelper::logEntry('JEFSHELPER: URL = ' . $url,JLog::INFO,'processing');
        }
        jefshelper::logEntry('JEFSHELPER: Prepearing for send Emails',JLog::INFO,'processing');
        JEFSHelper::sendNotificationEmails($user, $file, $url,$template);
        JEFSHelper::sendAdminEmails($user, $file,$amount, $currency,$type, $return, $gatewayName );
        // @todo Have to test this redirect for 2checkout
        if(!empty($redirectTo))
        {
            $html = '<DOCTYPE html>
            <html>
            <head><meta http-equiv="refresh" content="5;url=' . $redirectTo . '"></head><body>' . JText::_('JEFS_COM_REDIRECT_BACK') . '</body></html>';
            echo $html;
            exit();
        }
        exit();
    }
    function sendNotificationEmails($user, $file, $url,$template)
    {
        jefshelper::logEntry('JEFSHELPER: *** SEND NOTIFICATION EMAIL ***',JLog::INFO,'processing');

        $params =JComponentHelper::getParams('com_jefs');		// get params
        $valid = $params->def('keepDownloadLinkTime',0);

        $db = JFactory::getDbo();
        $mainframe = JFactory::getApplication();
        // Get user email template details
        $query = 'SELECT * FROM #__jefs_templates WHERE type = '.$db->quote($template);
        $db->setQuery( $query );
        $template = $db->loadObject();
        $tploptions = unserialize($template->options);

        $sitename = $mainframe->getCfg('sitename');
        $config =JFactory::getConfig();
        $mailfrom = array(
            $config->get( 'config.mailfrom' ),
            $config->get( 'config.fromname' ) );

        // Dynamic values: [title] = Item Name, [filename] = Filename, [user] = User's Name, [site] = Site Name
        // add some more replace params for the body text - [website] = Site Website, [email] = Site Email Address, [url] = A link back to the purchased item
        $subject = $tploptions['subject'];
        if($subject == '') $subject = '[site]: Download Access for [title]';

        $body = $template->html;
        if($body == '') {
            $body = $template->default;
        }

        $replacearray = array(	'[title]' => $file->title,
            '[filename]' => $file->filename,
            '[user]' => $user->name,
            '[site]' => $sitename,
            '[website]' => JURI::root(),
            '[email]' => $mailfrom,
            '[url]' => '<a href="'.$url.'" target="_blank">'.$url.'</a>',
            '[valid]' => $valid
        );

        foreach($replacearray as $k => $v) {
            $subject = str_replace($k,$v,$subject);
            $body = str_replace($k,$v,$body);
        }
        $app = JFactory::getApplication();

        JEFSHelper::sendMail($mailfrom, $user->email, $subject, $body, true);
    }
    function sendAdminEmails($user,$file,$amount, $currency,$type,$return, $gatewayName )
    {
        $db = JFactory::getDbo();
        jefshelper::logEntry('JEFSHELPER: *** SEND ADMIN EMAIL ***',JLog::INFO,'processing');
        // Send admin email
        // Get admin email template details
        $config =JFactory::getConfig();
        $mailfrom = array(
            $config->get( 'config.mailfrom' ),
            $config->get( 'config.fromname' ) );

        $query = 'SELECT * FROM #__jefs_templates WHERE type = '.$db->quote('adminemail');
        $db->setQuery( $query );
        $template = $db->loadObject();
        $tploptions = unserialize($template->options);
        $subject = $tploptions['subject'];
        if($subject == '') $subject = $file->title . ' Ordered via ' .$gatewayName;

        $body = $template->html;
        if($body == '') {
            $body = $template->default;
        }

        $alldata = '';
        foreach($return as $k => $v) {
            $alldata .= $k.' = '.$v."<br />";
        }

        $replacearray = array(	'[title]' => $file->title,
            '[filename]' => $file->filename,
            '[user]' => $user->name,
            '[username]' => $user->username,
            '[userid]' => $user->id,
            '[useremail]' => $user->email,
            '[time]' => date('l jS \of F Y h:i:s A'),
            '[type]' => $type,
            '[amount]' => $currency." ".$amount,
            '[alldata]' => $alldata,
        );

        foreach($replacearray as $k => $v) {
            $subject = str_replace($k,$v,$subject);
            $body = str_replace($k,$v,$body);
        }

        $adminemails = $tploptions['emailto'];
        if(empty($adminemails)) {
            $query = 'SELECT * FROM #__users WHERE sendEmail = '.$db->Quote(1).' AND block != '.$db->quote(1);
            $db->setQuery( $query );
            $adminemails = $db->loadObjectList();
        }
        else {
            $admins = implode(',',$adminemails);
            $query = 'SELECT * FROM #__users WHERE id IN('.$admins.') AND block != '.$db->quote(1);
            $db->setQuery( $query );
            $adminemails = $db->loadObjectList();
        }
        foreach($adminemails as $adminemail) {
            JEFSHelper::sendMail($mailfrom, $adminemail->email, $subject, $body, true);
        }
        $otheradminemails = $tploptions['otheremailto'];
        if($otheradminemails != '') {
            $otheradminemails = explode("\n",$otheradminemails);
            foreach($otheradminemails as $otheradminemail) {
                JEFSHelper::sendMail($mailfrom, $otheradminemail, $subject, $body, true);
            }
        }
    }
    static function sendMail($mailfrom, $mailto, $subject, $body, $isHTML)
    {
        jefshelper::logEntry('JEFSHELPER: sendmail Started',JLog::INFO,'component');
        $mailer =JFactory::getMailer();
        jefshelper::logEntry('JEFSHELPER: sendmail mailto = ' .$mailto ,JLog::INFO,'component');
        $mailer->addRecipient($mailto);
        jefshelper::logEntry('JEFSHELPER: sendmail mail from = ' .$mailfrom ,JLog::INFO,'component');
        $mailer->setSender($mailfrom);
        jefshelper::logEntry('JEFSHELPER: sendmail mail subject = ' .$subject ,JLog::INFO,'component');
        $mailer->setSubject($subject);
        jefshelper::logEntry('JEFSHELPER: sendmail mail body = ' .$body ,JLog::INFO,'component');
        $mailer->setBody($body);
        jefshelper::logEntry('JEFSHELPER: sendmail mail isHTML = ' . $isHTML  ,JLog::INFO,'component');
        $mailer->IsHTML($isHTML);
        $send =& $mailer->Send();
        if ( $send !== true ) {
            jefshelper::logEntry('JEFSHELPER: sendmail Error: ' . $send->ErrorInfo ,JLog::WARNING,'component');
        } else {
            jefshelper::logEntry('JEFSHELPER: sendmail SUCCESSFUL!!!'  ,JLog::INFO,'component');
        }
    }
}