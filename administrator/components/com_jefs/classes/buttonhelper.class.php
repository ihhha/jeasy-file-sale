<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */


//--No direct access
defined( '_JEXEC' ) or die( 'No Direct Access' );

class ButtonHelper
{

	var $_templates;
	var $_db;

	function ButtonHelper() {
		// Set up DB
		$this->_db =JFactory::getDBO();

        //load language
        $language = JFactory::getLanguage();
        $langPath = JPATH_SITE . DS . 'components' . DS . 'com_jefs';
        $language->load('com_jefs', $langPath, $language->getTag(), true);

		// Set up templates
		$this->_templates = array();
		$query = 'SELECT * FROM #__jefs_templates ';
		$this->_db->setQuery( $query );
		$this->_templates = $this->_db->loadObjectList('type');
		foreach($this->_templates as $template) {
			if($template->html == '') $template->html = $template->default;
		}
		$uri =JFactory::getURI();
		$currentSession = JSession::getInstance('none',array());
		$currentSession->set("aclastrequest",$uri->toString());	// add this request as the last known
    }
	
	function getButton($file,$source = '',$type = false)
	{
		global $option;

		$user		=JFactory::getUser();
		$userid		= (int) $user->get('id', 0);
		$fileid = $file->id;
		$uri =JFactory::getURI();
		$url = $uri->toString();

        $params = JComponentHelper::getParams( 'com_jefs' );
        $allowNonRegister = $params->def('allowNoRegisterDownload');
		
		if($file->link_type == 'inherit') {
			$file = jefshelper::inheritParent($file, $file->parent);
			$accessid = $file->parent;
		}
		else $accessid = $file->id;
		
		if(!$type) $type = $file->link_type;
		$purchase_button = unserialize($file->purchase_button);
		$price_options = unserialize($file->price_options);
//		$price_options['auto_conv'] = $file->auto_conv;
//		$price_options['auto_conv_amt'] = $file->auto_conv_amt;

        $info ='';
        JPluginHelper::importPlugin( 'jefs' );
        $gateways = jefshelper::getPlugins();
        if(empty($gateways)) $info = base64_decode('PHNwYW4gc3R5bGU9ImZvbnQtc2l6ZTogOXB4O2NvbG9yOiM4MDgwODA7Ij5Qb3dlcmVkIGJ5IDxhIHN0eWxlPSJmb250LXNpemU6IDlweDtjb2xvcjojODA4MDgwOyIgaHJlZj0iaHR0cDovL2pvb21hbHVuZ21hLmNvbSIgdGFyZ2V0PSJfYmxhbmsiPkpvb21hbHVuZ21hPC9hPi48L3NwYW4+');

		$userdonated = false;	// Has the user already donated
		if(($type == 'purchase' || $type == 'donate') && $file->allow_download == '1') {
			// Check to see if user has already paid. If so, change type to download
			if(!empty($userid)) {
					$query = 'SELECT access FROM #__jefs_users WHERE id = '.$userid;
					$this->_db->setQuery( $query );
					$access = unserialize($this->_db->loadResult());
					if(isset($access['files'])) {
						if(in_array($accessid,$access['files'])) {
							if($type == 'purchase') $type = 'download';
							elseif($type == 'donate') $userdonated = true;
						}
					}
			}
		}
        $rnd=rand();
		if($type == 'register' && !empty($userid)) {
			$type = 'download';
		}


		// Generic Hidden items for all but register type
		$hiddenVars  = '<input type="hidden" name="option" value="com_jefs" />';
		$hiddenVars .= '<input type="hidden" name="view" value="download" />';
		$hiddenVars .= '<input type="hidden" name="source" value="'.$source.'" />';
		$hiddenVars .= '<input type="hidden" name="acid" value="'.$fileid.'" />';
		$hiddenVars .= '<input type="hidden" name="url" value="'.urlencode($url).'" />';
		$hiddenVars .= '<input type="hidden" name="type" value="'.$file->link_type.'" />';

		// Get type specific html
		if($type == 'download') {
			$html = '<form name="'.$type.'buttonform_'.$rnd.'" action="index.php" method="post">';
			$tploptions = unserialize($this->_templates['download']->options);
			$downloadbutton = '<input type="submit" name="submitdownload_'.$rnd.'" value="'.$tploptions['button_text'].'" />';
			$template = $this->_templates['download']->html;
			$template = str_replace('[button]', $downloadbutton, $template);
			$template = str_replace('[title]', $file->title, $template);
			$template = str_replace('[filename]', $file->filename, $template);
			$html .= $template;
			$html .= '<input type="hidden" name="task" value="download" />';
			$html .= $hiddenVars;
			$html .= '</form>';
			$html .= $info;
		}

		if($type == 'register') {
			$tploptions = unserialize($this->_templates['register']->options);
			$params =JComponentHelper::getParams('com_jefs');		// get params
			$template = $this->_templates['register']->html;
			// Create Register Button
			$registerbutton = $this->renderRegisterButton($type, $rnd, $source);
			// Create Login Button
			$loginlink = $params->get('loginlink', 'index.php?option=com_users&amp;view=login');
			$splitlink = explode('?',$loginlink);
			$queries = explode('&',$splitlink[1]);
			$vars = array();
			foreach($queries as $query) {
				$var = explode('=',$query);
				$vars[$var[0]] = $var[1];
			}
			$loginbutton = '<form name="'.$type.'buttonform_'.$rnd.'login" action="index.php" method="get">';
			$loginbutton .= '<input type="submit" value="'.$tploptions['login_button_text'].'" />';
			foreach($vars as $name => $value) {
				$loginbutton .= '<input type="hidden" name="'.$name.'" value="'.$value.'" />';
			}
            $loginbutton .= '</form>';
			
			$template = str_replace('[regbutton]', $registerbutton, $template);
			$template = str_replace('[logbutton]', $loginbutton, $template);
			$template = str_replace('[title]', $file->title, $template);
			$template = str_replace('[filename]', $file->filename, $template);
            $html = $template.$info;
			
		}
        if($type == 'purchase'){
            $html = '<form name="'.$type.'buttonform_'.$rnd.'" action="index.php" method="post" onsubmit="return validateForm(' . $rnd . ');">';
			$tploptions = unserialize($this->_templates['purchase']->options);
			$displaycode = ($tploptions['curr_code'] == 0) ? false : true;	// Set whether to display the curr code
			$purchaseimg = $purchase_button['buttonurl'];
            $purchasebutton = '<input type="image" id="p_btn_' . $rnd . '" src="'.$purchaseimg.'" name="image_'.$rnd.'" />';
			$currencylist	= $this->getCurrencyList($price_options,$fileid,$rnd,$source,$displaycode);
			$template = $this->_templates['purchase']->html;
			$template = str_replace('[price]', $currencylist['price'], $template);
			$template = str_replace('[currencylist]', $currencylist['list'], $template);
			$template = str_replace('[paymentgateway]', ButtonHelper::renderPluginRadioButtons($fileid, $rnd), $template);
			$template = str_replace('[button]', $purchasebutton, $template);
			$template = str_replace('[title]', $file->title, $template);
			$template = str_replace('[filename]', $file->filename, $template);

            if($allowNonRegister && !$userid)
            {
                $template = str_replace('[email_input]', "<input type='text' name='email' id='email_$rnd'/>", $template);
                $template = str_replace('[email_input_lbl]', JText::_('COM_JEFS_INPUT_UNREGISTER_EMAIL'), $template);
                $template = str_replace('[email_input_lbl_desc]', JText::_('COM_JEFS_EMAIL_FOR_LINK_AFTER_PURCHASE'), $template);
            }
            else
            {
                $template = str_replace('[email_input]', '', $template);
                $template = str_replace('[email_input_lbl]', '', $template);
                $template = str_replace('[email_input_lbl_desc]', '', $template);
            }
            $html .= $template;
            $html .= $hiddenVars;

			$html .= '<input type="hidden" name="task" value="purchase" />';
			$html .= '<input type="hidden" name="rnd" value="' . $rnd . '" />';
			$html .= '</form>';
		}
		if($type == 'donate') {
			$html = '<form name="'.$type.'buttonform_'.$rnd.'" action="index.php" method="post" onsubmit="return checkMinAmount(this);">';
			$tploptions = unserialize($this->_templates['donate']->options);
			$displaycode = ($tploptions['curr_code'] == 0) ? false : true;	// Set whether to display the curr code
			$purchaseimg = $purchase_button['buttonurl'];
            $rnd = rand(); // added in case few button for one file at the same page
			$purchasebutton = '<input type="image" id="p_btn_' . $fileid . '_' . $rnd . '" src="'.$purchaseimg.'" name="image_'.$rnd.'" /></form>';
			$currencylist	= $this->getCurrencyList($price_options,$fileid,$rnd,$source,$displaycode,$type);
			$amountbox = '<input type="text" name="ACamount_'.$rnd.'" id="ACamount_'.$rnd.'" size="'.$tploptions['boxsize'].'" maxlength="250" value="" />';
			$template = $this->_templates['donate']->html;
			$template = str_replace('[currencylist]', $currencylist['list'], $template);
			$template = str_replace('[amountbox]', $amountbox, $template);
            $template = str_replace('[paymentgateway]', ButtonHelper::renderPluginRadioButtons($fileid, $rnd), $template);
			$template = str_replace('[button]', $purchasebutton, $template);
            if($allowNonRegister && !$userid)
            {
                $template = str_replace('[email_input]', "<input type='text' name='email' id='email_$rnd'/>", $template);
                $template = str_replace('[email_input_lbl]', JText::_('COM_JEFS_INPUT_UNREGISTER_EMAIL'), $template);
                //$template = str_replace('[email_input_lbl]', JText::_('COM_JEFS_INPUT_UNREGISTER_EMAIL'), $template);
                $template = str_replace('[email_input_lbl_desc]', JText::_('COM_JEFS_EMAIL_FOR_LINK_AFTER_DONATE'), $template);
            }
            else
            {
                $template = str_replace('[email_input]', '', $template);
                $template = str_replace('[email_input_lbl]', '', $template);
                $template = str_replace('[email_input_lbl_desc]', '', $template);
            }

			$html .= '<input type="hidden" name="task" value="donate" />';
            $html .= '<input type="hidden" name="rnd" value="' . $rnd . '" />';
			$html .= $hiddenVars;
			$html .= $template;
			//$html .= '</form>';
			// If there's no minimum donation, or the user has already donated, then add download button
			if((int)$currencylist['amount'] == 0 || $userdonated == true) {
				$dlbutton = '<form name="downloadbuttonform_'.$rnd.'" action="index.php" method="post">';
				$dltploptions = unserialize($this->_templates['download']->options);
				$dlbutton .= '<input type="submit" name="submitdownload_'.$rnd.'" value="'.$dltploptions['button_text'].'" />';
				$dlbutton .= '<input type="hidden" name="task" value="download" />';
				$dlbutton .= $hiddenVars;
				$dlbutton .= '</form>';
			}
			else $dlbutton = '';
			if($currencylist['amount'] > 0) $html = str_replace('[mindonation]', $tploptions['button_text'], $html);
			else $html = str_replace('[mindonation]', '', $html);
			$html = str_replace('[dlbutton]', $dlbutton, $html);
			$html = str_replace('[price]', $currencylist['price'], $html);
			$html = str_replace('[title]', $file->title, $html);
			$html = str_replace('[filename]', $file->filename, $html);
		}
		return $html;
			
	}

    static function renderPluginRadioButtons($id=0, $rnd=0)
    {
        JPluginHelper::importPlugin( 'jefs' );
        $gateways = jefshelper::getPlugins();

        if(empty ($gateways)) $html = JText::_('COM_JEFS_NO_PAYMENT_PLUGIN');

        else {
            $html = $onchange = '';

            if(count($gateways)==1) // if only one payment plugin no need show Radio Button
            {
                if ($id) $script = '<script type="text/javascript">var jefsPluginOne = true;</script>';
                foreach ($gateways as $paymentplugin => $plgName)
                {
                    $iconStyle = 'display:inline-block;width:16px;height:16px;background:url("' . JURI::base() . '/plugins/jefs/'. $paymentplugin . '/assets/images/icon.png")';
                    //$html .= "<b>Use " . $plgName . " for payment.</b><input type='hidden' id='paymentPlugin_" . $id . "' name='paymentPlugin' value='$paymentplugin' />";
                    $html .= "<span style='". $iconStyle . "' id='icon_" . $paymentplugin . "_" . $rnd . "'></span>&nbsp;<b>" . JText::sprintf('COM_JEFS_USE_THIS_PLUGIN',$plgName) . "</b><input type='hidden' id='paymentPlugin_" . $rnd . "' name='paymentPlugin_" . $rnd . "' value='$paymentplugin' />";

                }
                $html .= $script;
            }
            else
            {
                //$onchange = 'onchange="checkPluginCurrencySupport(\'' . $rnd . '\', this.value);"';
                foreach ($gateways as $paymentplugin => $plgName)
                {
                    $iconStyle = 'display:inline-block;width:16px;height:16px;background:url("' . JURI::base() . '/plugins/jefs/'. $paymentplugin . '/assets/images/icon.png")';
                    //$html .= "<div style='text-align:left'><input " . $onchange . " type='radio' id='" . $paymentplugin . "_" . $rnd . "' name='paymentPlugin_" . $rnd . "' value='$paymentplugin' />&nbsp;<span style='". $iconStyle . "'></span>&nbsp;$plgName</div>";
                    $html .= "<div style='text-align:left'><input type='radio' id='" . $paymentplugin . "_" . $rnd . "' name='paymentPlugin_" . $rnd . "' value='$paymentplugin' />&nbsp;<span style='". $iconStyle . "' id='icon_" . $paymentplugin . "_" . $rnd . "'></span>&nbsp;$plgName</div>";
                }
            }
        }
        return $html;
    }
	function getCurrencyList($price_options,$fileid,$rnd,$source = '',$displaycode = true,$type = ''){
	
		require_once( JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_jefs'.DS.'classes'.DS.'pricehelper.class.php' );
		$priceHelper = new PriceHelper();				
		$params = JComponentHelper::getParams( 'com_jefs' );
		$default = $priceHelper->getCurrencyCode($params, $price_options);
		$currency_array = $priceHelper->currency_array;
		$symbols = $priceHelper->symbol_array;

        $pluginSupportedCurrency = JEFSHelper::getPluginsSupportedCurrency();

		if($displaycode == true) {
			$codedisplay = $default.' ';
			$jscodedisplay = 'curr + " " + price';
		}
		else {
			$codedisplay = '';
			$jscodedisplay = 'price';
		}
		
		$pricearray = $priceHelper->getPrices($price_options);
		isset($pricearray[$default]['amount']) ? $amount = $pricearray[$default]['amount'] : $amount = '0.00';

        $currencyCount = 0;
        $list = '<select id="ACcurrency_'.$rnd.'" name="ACcurrency_'.$rnd.'" class="inputbox" onchange="changeACprice_'.$rnd.'(\''.$rnd.'\')">';
		foreach($currency_array as $key => $value) {	
			if(isset($price_options[$value]['display']) || $value == $price_options['default']) {
				// Only add currency if we have a price
				if(!isset($pricearray[$value]['amount'])) continue;
				if($pricearray[$value]['amount'] == 0 && $type != 'donate') continue;	// Disallow zero amounts unless using donations (where amount is added by user)
				if($default == $value) $list .= '<option value="'.$value.'" selected>'.$key.'</option>';
						else $list .= '<option value="'.$value.'">'.$key.'</option>';
                $currencyCount++;
			}
		}
		$list .= '</select>';

		if($currencyCount < 2) {
			// if we have no options then don't display currency but replace with hidden value of currency
			$list = '<input type="hidden" id="ACcurrency_'.$rnd.'" name="ACcurrency_'.$rnd.'" value="'.$default.'" />';	
		}
		
		// $symbols = array('AUD' => '$','GBP' => '&pound;','CAD' => '$','CZK' => '&#75;&#269;','DKK'  => '&#107;&#114;', 'EUR' => '&euro;', 'HKD' => '$', 'HUF'  => '&#70;&#116;', 'ILS'  => '&#8362;', 'JPY' => '&yen;', 'MXN' => '$', 'NZD' => '$', 'NOK'  => '&#107;&#114;', 'PLN'  => '&#122;&#322;', 'SGD' => '$','SEK' => '&#107;&#114;', 'CHF'  => '&#67;&#72;&#70;', 'USD' => '$');
		$price = '<span id="ACprice_'.$rnd.'">'.$codedisplay.$symbols[$default].$amount.'</span>';


        //  plugin currency support
        $js = "jefs.msg.choosePaymentSystem = '" . JText::_('COM_JEFS_CHOOSE_PAYMENT_SYSTEM') . "';";
        $js .="window.addEvent('load',function(){checkPluginCurrencySupport($rnd);});";
        $js .= "var pluginSupportedCurrency = {";
        $separator = ',';
        $k = 1;
		foreach($pluginSupportedCurrency as $plgName=>$currencies)
        {
            if($k >= count($pluginSupportedCurrency)) $separator = '';
            $js .="$plgName:['" . implode('\',\'',$currencies) . "']$separator";
            $k++;
        }
        $js .= "};";


        $js .= 'function changeACprice_'.$rnd.'(rnd)
				{
				    if(!checkPluginCurrencySupport(rnd,\'\')) return;
					curr = window.document.getElementById("ACcurrency_'.$rnd.'").value;
					';
		foreach($pricearray as $key => $value) {
			$js .= 'if(curr == "'.$key.'") price = "'.$symbols[$key].$value['amount'].'";';
		}
		$js .= '	window.document.getElementById("ACprice_'.$rnd.'").innerHTML = '.$jscodedisplay.';
				}';




		$doc =JFactory::getDocument();

        $doc->addScript(JURI::base() . '/administrator/components/com_jefs/assets/jefs.js');
		$doc->addScriptDeclaration($js);
		
		if($type == 'donate') {
			$js = 'function checkMinAmount(thisform)
					{';
            if($params->def('allowNoRegisterDownload')){
                $js .="if(!validateForm($rnd))return false;
            ";}
            $js .='            curr = thisform.ACcurrency_'.$rnd.'.value;
						amount = thisform.ACamount_'.$rnd.'.value;
						';
			foreach($pricearray as $key => $value) {
				$js .= 'if(curr == "'.$key.'") {
							pricedisp = "'.$key.' '.$value['amount'].'";
							price = '.$value['amount'].';
							if(amount < price) return confirm("'.JText::_( 'COM_JEF_MIN_DONATION').'" + pricedisp + "\n'.
							JText::_( 'COM_JEF_DONATIONT_WILL_ADJUST' ).'" + pricedisp + "\n'.
							JText::_( 'COM_JEF_OK_OR_CANCEL' ).'");
							else return true;
						}';
			}
			$js .= '  return true;
					}
						';
					
			$doc->addScriptDeclaration($js);
		}
		return array('price' => $price, 'list' => $list, 'amount' => $amount);
	
	}
    function addPluginButton($id)
    {
        JPluginHelper::importPlugin( 'jefs' );
            $plugins = jefshelper::getPlugins();
            foreach ($plugins as $paymentplugin=>$name)
            {

                $data = array('fileId' => $id);
                $className = 'plgjefs'.$paymentplugin;
                    if (class_exists($className) && method_exists($className, 'addButton'))
                    {
                        $dispatcher  =JDispatcher::getInstance();
                        $plugin 	 = new $className($dispatcher, array());
                        $args   	 = array('plugin' => $paymentplugin, 'data' => $data);
                        $html = call_user_func_array(array($plugin, 'addButton'), $args);
                    }
            }
    }
    function renderRegisterButton($type, $rnd,  $source='form1' )
    {
        $tploptions = unserialize($this->_templates['register']->options);
        $params =JComponentHelper::getParams('com_jefs');		// get params
        $template = $this->_templates['register']->html;
        // Create Register Button
        $registerlink = $params->get('registerlink', 'index.php?option=com_users&view=registration');
        $splitlink = explode('?',$registerlink);
        $queries = explode('&',$splitlink[1]);
        $vars = array();
        foreach($queries as $query) {
            $var = explode('=',$query);
            $vars[$var[0]] = $var[1];
        }
        $registerbutton = '<form name="'.$type.'buttonform_'.$rnd.'register" action="index.php" method="get">';
        $registerbutton .= '<input type="submit" value="'.$tploptions['register_button_text'].'" />';
        foreach($vars as $name => $value) {
            $registerbutton .= '<input type="hidden" name="'.$name.'" value="'.$value.'" />';
        }
        $registerbutton .= '</form>';
        return $registerbutton;
    }
}