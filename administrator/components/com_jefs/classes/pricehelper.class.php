<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined( '_JEXEC' ) or die( 'No Direct Access' );

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

class PriceHelper
{
	var $currency_array = array();
	var $symbol_array = array();
	var $_currencies = array();


	function PriceHelper() {
		// get list of accepted PayPal currencies from DB
		// see ISO 4217 and list of PayPal accepted currencies
		$db =JFactory::getDBO();
		$query = ' SELECT * FROM #__jefs_currencies';
		$db->setQuery($query);
		$this->_currencies = $db->loadObjectList('code');
		
		$this->currency_array = array();
		$this->symbol_array = array();
		
		foreach($this->_currencies as $currency) {
			$this->currency_array[$currency->name] = $currency->code;
			$this->symbol_array[$currency->code] = $currency->symbol;
		}
    }
	
	function getCurrencyCode($params, $options) {
	
		if(isset($options['default'])) {
			$currency_code = $options['default'];
		}
		else {
			$currency_code = $params->get('defaultcurrency');
		}
		
		return $currency_code;
	}

	function getPrices($price_options, $currency = false) {
		if($currency) $currency_array = array('currency' => $currency);
		else $currency_array = $this->currency_array;
		$pricearray = array();
		
		foreach($currency_array as $key => $value) {	
			if(isset($price_options[$value]['display']) || $value == $price_options['default']) {
//				if($price_options[$value]['price'] == '') continue;
//				$amount = $price_options[$value]['price'];
				$amount = empty($price_options[$value]['price']) ? 0.00 : $price_options[$value]['price'];
				
				if($value == 'HUF' || $value == 'JPY') $roundby = 0;	// These currencies require to be round to nearest whole number
				else $roundby = 2;
				$pricearray[$value]['amount'] = number_format($amount, $roundby, '.', '');	// could add a thousands separator value here?
			}
		}
		
		return $pricearray;
	}
	
}

?>