window.addEvent("domready", function() {
    $("saveDefault").addEvent("click", function() {
        $("fieldsContainer").empty().addClass("ajax-loading").setHTML("<img src='"+assetsBase+"/images/ajax-loader.gif' border='0'> "+ _SAVING_ );
		var newdescription = tinyMCE.getInstanceById('description').getBody().innerHTML;
        // var url="index.php?option=com_jefs&controller=files&task=savedefault&format=raw";
		var url="index.php";
        var a=new Ajax(url,{
            method:"post",
			data: {option:'com_jefs',controller:'files',task:'savedefault',format:'raw',description:newdescription},
            onComplete: function(response) {
                var resp=Json.evaluate(response);
                $("fieldsContainer").removeClass("ajax-loading").setHTML(resp.msg);
            }
        }).request();
    });
});

window.addEvent("domready", function() {
    $("loadDefault").addEvent("click", function() {
        $("fieldsContainer").empty().addClass("ajax-loading").setHTML("<img src='"+assetsBase+"/images/ajax-loader.gif' border='0'> "+ _LOADING_ );
        var url="index.php?option=com_jefs&controller=files&task=loaddefault&format=raw";
        var a=new Ajax(url,{
            method:"get",
            onComplete: function(response) {
                var resp=Json.evaluate(response);
                $("fieldsContainer").removeClass("ajax-loading").setHTML(resp.msg);
				tinyMCE.getInstanceById('description').getBody().innerHTML = resp.html;
            }
        }).request();
    });
});
