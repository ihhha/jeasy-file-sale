/**
 * Created with JetBrains PhpStorm.
 * User: root
 * Date: 11.07.12
 * Time: 23:34
 * To change this template use File | Settings | File Templates.
 */
jefs = {
    msg:{
        invalidEmail: 'Invalid email address.',
        choosePaymentSystem: 'You have to choose payment system.',
        currencyNotSupported: 'Payment system doesn\'t support this currency.'
    }
}

function validateForm(rnd){
    if(!checkPluginCurrencySupport(rnd))
    {
        radioObj = document.forms['purchasebuttonform_' + rnd].elements['paymentPlugin_'+rnd];
        setCheckedValue(radioObj,'');
        return false;
    }
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var email = window.document.getElementById('email_'+rnd);
    if(email != null)
    {
        var address = email.value;
        if(reg.test(address) == false) {
            alert(jefs.msg.invalidEmail);
            return false;
        }
    }
    if(typeof jefsPluginOne == 'undefined') // plugin is not only one
    {
        radioObj = document.forms['purchasebuttonform_' + rnd].elements['paymentPlugin_'+rnd];
        console.log(getCheckedValue(radioObj));
        if('' == getCheckedValue(radioObj))
        {
            alert(jefs.msg.choosePaymentSystem);
            return false;
        }
    }
    return true;
}

function setCheckedValue(radioObj, newValue){
    if(!radioObj)
        return;
    var radioLength = radioObj.length;
    if(radioLength == undefined) {
        radioObj.checked = (radioObj.value == newValue.toString());
        return;
    }
    for(var i = 0; i < radioLength; i++) {
        radioObj[i].checked = false;
        if(radioObj[i].value == newValue.toString()) {
            radioObj[i].checked = true;
        }
    }
}
function getCheckedValue(radioObj) {
    if(!radioObj)
        return "";
    var radioLength = radioObj.length;
    if(radioLength == undefined)
        if(radioObj.checked)
            return radioObj.value;
        else
            return "";
    for(var i = 0; i < radioLength; i++){
        if(radioObj[i].checked) {
            return radioObj[i].value;
        }
    }
    return "";
}

function checkPluginCurrencySupport(rnd)
{
    if($('ACcurrency_'+rnd).type == 'hidden') return true;
    selectedCurr = $('ACcurrency_'+rnd).value;

    radioObj = document.forms['purchasebuttonform_' + rnd].elements['paymentPlugin_'+rnd];
    if(typeof jefsPluginOne != 'undefined')  // plugin is only one
    {
        plgName = document.getElementById('paymentPlugin_' + rnd).value;
        if(in_array(selectedCurr,pluginSupportedCurrency[plgName])) return true;
        else
        {
            alert(jefs.msg.currencyNotSupported);
            return false;
        }
    }
    else
    {
        for (var plgName in pluginSupportedCurrency)
        {
            if(pluginSupportedCurrency[plgName].contains(selectedCurr))
            {
                document.getElementById(plgName + "_" + rnd).disabled = false;
                document.getElementById("icon_" + plgName + "_" + rnd).style.backgroundPosition = "center top";
            }
            else
            {
                document.getElementById(plgName + "_" + rnd).checked = false;
                document.getElementById(plgName + "_" + rnd).disabled = true;
                document.getElementById("icon_" + plgName + "_" + rnd).style.backgroundPosition = "center bottom";
            }
        }
        radioObj = document.forms['purchasebuttonform_' + rnd].elements['paymentPlugin_'+rnd];
    }
    return true;
}

function in_array(needle, haystack, strict) {	// Checks if a value exists in an array
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

    var found = false, key, strict = !!strict;

    for (key in haystack) {
        if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
            found = true;
            break;
        }
    }

    return found;
}
