<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.form.formfield');

// The class name must always be the same as the filename (in camel case)
class JFormFieldModal_Jefsfile extends JFormField
{

	//The field class must know its own type through the variable $type.
	protected $type = 'Modal_Jefsfile';

    public function getInput(){
        // Load the javascript
        JHtml::_('behavior.framework');
        JHtml::_('behavior.modal', 'a.modal');

        $db			=JFactory::getDBO();

        $script = array();
        $script[] = '	function jSelectChart_'.$this->id.'(id, name, object) {';
        $script[] = '		document.id("'.$this->id.'_id").value = id;';
        $script[] = '		document.id("'.$this->id.'_name").value = name;';
        $script[] = '		SqueezeBox.close();';
        $script[] = '	}';

        // Add the script to the document head.
        JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

        // Get the title of the linked chart
        $db = JFactory::getDBO();
        $db->setQuery(
            'SELECT title' .
            ' FROM #__jefs_files' .
            ' WHERE id = '.(int) $this->value
        );
        $title = $db->loadResult();

        if ($error = $db->getErrorMsg()) {
            JError::raiseWarning(500, $error);
        }

        if (empty($title)) {
            $title = JText::_('COM_JEFS_SELECT_FILE');
        }

        $link = 'index.php?option=com_jefs&amp;view=files&amp;tmpl=component&amp;object='.$this->id;


        $html = "\n".'<div class="fltlft"><input type="text" id="'.$this->id.'_name" value="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" disabled="disabled" /></div>';
        $html .= '<div class="button2-left"><div class="blank"><a class="modal" title="'.JText::_('COM_JEFS_CHANGE_FILE_BUTTON').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('COM_JEFS_CHANGE_FILE_BUTTON').'</a></div></div>'."\n";
        // The active file id field.
        if (0 == (int)$this->value) {
            $value = '';
        } else {
            $value = (int)$this->value;
        }

        // class='required' for client side validation
        $class = '';
        if ($this->required) {
            $class = ' class="required modal-value"';
        }

        $html .= '<input type="hidden" id="'.$this->id.'_id"'.$class.' name="'.$this->name.'" value="'.$value.'" />';
        return $html;
    }
}