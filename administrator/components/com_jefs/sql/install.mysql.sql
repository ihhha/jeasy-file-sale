CREATE TABLE IF NOT EXISTS `#__jefs_currencies`
(
  `code` varchar(3) NOT NULL,
  `name` varchar(255) NOT NULL,
  `symbol` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `last_check` date NOT NULL,
  PRIMARY KEY  (`code`)
)
ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jefs_files` (
					  `id` int(11) NOT NULL auto_increment,
					  `filename` varchar(255) NOT NULL,
					  `title` varchar(255) NOT NULL,
					  `alias` varchar(255) NOT NULL,
					  `parent` int(11) NOT NULL,
					  `created` datetime NOT NULL,
					  `modified` datetime NOT NULL,
					  `auto_modified` tinyint(1) NOT NULL,
					  `downloads` int(11) NOT NULL,
					  `downloads_add` int(11) NOT NULL,
					  `purchases` int(11) NOT NULL,
					  `published` tinyint(1) NOT NULL,
					  `path` text NOT NULL,
					  `description` text NOT NULL,
					  `purchase_button` text NOT NULL,
					  `price_options` text NOT NULL,
					  `link_type` varchar(255) NOT NULL,
					  `return_page` tinyint(1) NOT NULL COMMENT '0=order page, 1=download page',
					  `allow_download` tinyint(1) NOT NULL,
					  `allow_children` tinyint(1) NOT NULL,
					  `donations` text NOT NULL COMMENT 'array currency=user=amount',
					  PRIMARY KEY  (`id`),
					  UNIQUE KEY `alias` (`alias`)
					) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `#__jefs_templates` (
					  `type` varchar(255) NOT NULL,
					  `default` text NOT NULL,
					  `html` text NOT NULL,
					  `options` text NOT NULL,
					  PRIMARY KEY  (`type`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jefs_users` (
					  `id` int(11) NOT NULL,
					  `access` text NOT NULL,
					  `donations` text NOT NULL,
					  PRIMARY KEY  (`id`)
					) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jefs_nonregistered`(
                    `id` INT(11) UNSIGNED NOT NULL auto_increment,
                    `email` VARCHAR(256) NOT NULL,
                    `created` DATETIME NOT NULL,
                    `fileid` INT(11) NOT NULL,
                    `donation` TEXT DEFAULT NULL,
                    `validUntil` DATETIME NOT NULL,
                    `hash` CHAR(32) NOT NULL,
                    `downloadHash` CHAR(32) NOT NULL,
                    `status` INT(11) NOT NULL DEFAULT 0,
                    PRIMARY KEY (`id`)
                  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;


INSERT IGNORE INTO `#__jefs_currencies` (`code`, `name`, `symbol`, `rate`, `last_check`) VALUES
					('AUD', 'Australian Dollar', '$', '', '0000-00-00'),
					('GBP', 'British Pound', '&pound;', '', '0000-00-00'),
					('CAD', 'Canadian Dollar', '$', '', '0000-00-00'),
					('CZK', 'Czech Koruna', '&#75;&#269;', '', '0000-00-00'),
					('DKK', 'Danish Krone', '&#107;&#114;', '', '0000-00-00'),
					('EUR', 'Euro', '&euro;', '', '0000-00-00'),
					('HKD', 'Hong Kong Dollar', '$', '', '0000-00-00'),
					('HUF', 'Hungarian Forint', '&#70;&#116;', '', '0000-00-00'),
					('ILS', 'Israeli New Shekel', '&#8362;', '', '0000-00-00'),
					('JPY', 'Japanese Yen', '&yen;', '', '0000-00-00'),
					('MXN', 'Mexican Peso', '$', '', '0000-00-00'),
					('NZD', 'New Zealand Dollar', '$', '', '0000-00-00'),
					('NOK', 'Norwegian Krone', '&#107;&#114;', '', '0000-00-00'),
					('PLN', 'Polish Zloty', '&#122;&#322;', '', '0000-00-00'),
					('SGD', 'Singapore Dollar', '$', '', '0000-00-00'),
					('SEK', 'Swedish Krona', '&#107;&#114;', '', '0000-00-00'),
					('CHF', 'Swiss Franc', '&#67;&#72;&#70;', '', '0000-00-00'),
					('USD', 'U.S. Dollar', '$', '', '0000-00-00'),
					('RUB', 'Russian Ruble', 'р.', '', '0000-00-00');

INSERT IGNORE INTO `#__jefs_templates` (`type`, `default`, `html`, `options`) VALUES
('download', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'a:1:{s:11:\"button_text\";s:8:\"Download\";}'),
('purchase', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>Price: <span style=\"color: #ff0000;\">[price]</span></strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[currencylist]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">\r\n[email_input_lbl] [email_input]<br>[email_input_lbl_desc]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[paymentgateway]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>Price: <span style=\"color: #ff0000;\">[price]</span></strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[currencylist]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">\r\n[email_input_lbl] [email_input]<br>[email_input_lbl_desc]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[paymentgateway]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'a:1:{s:9:\"curr_code\";i:1;}'),
('donate', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>[mindonation]</strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">Amount: [amountbox] [currencylist]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">\r\n[email_input_lbl] [email_input]<br>[email_input_lbl_desc]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[paymentgateway]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[dlbutton]</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>[mindonation]</strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">Amount: [amountbox] [currencylist]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">\r\n[email_input_lbl] [email_input]<br>[email_input_lbl_desc]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[paymentgateway]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[dlbutton]</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'a:3:{s:11:\"button_text\";s:57:\"Minimum Donation: <span style=\"color:red;\">[price]</span>\";s:7:\"boxsize\";s:1:\"5\";s:9:\"curr_code\";i:1;}'),
('register', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>You must be registered <br />to download this file</strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[regbutton]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[logbutton]</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>You must be registered <br />to download this file</strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[regbutton]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[logbutton]</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'a:2:{s:20:\"register_button_text\";s:22:\"Click here to register\";s:17:\"login_button_text\";s:19:\"Click here to login\";}'),
('email', '<p>Hi [user] <br /><br /> Thanks for choosing [title] <br /> You now have access to download the file ([filename]) from here: (please make sure you''re still logged in)<br /><br /> [url] <br /><br /> You may also receive a separate Transaction ID from Payment System.<br /><br /></p>\r\n<hr />\r\n<p>Please contact us if you have any questions/problems:<br /><br /> [site] <br /> [website] <br /> [email]</p>\r\n<hr />', '<p>Hi [user] <br /><br /> Thanks for choosing [title] <br /> You now have access to download the file ([filename]) from here: (please make sure you''re still logged in)<br /><br /> [url] <br /><br /> You may also receive a separate Transaction ID from Payment System.<br /><br /></p>\r\n<hr />\r\n<p>Please contact us if you have any questions/problems:<br /><br /> [site] <br /> [website] <br /> [email]</p>\r\n<hr />', 'a:1:{s:7:\"subject\";s:35:\"[site]: Download Access for [title]\";}'),
('emailUnregistered', '<p>Hello, <br /><br /> Thanks for choosing [title] <br /> You now may download the file ([filename]) from here: (link valid [valid] hours)<br /><br /> [url] <br /><br /> You may also receive a separate Transaction ID from Payment System.<br /><br /></p>\r\n<hr />\r\n<p>Please contact us if you have any questions/problems:<br /><br /> [site] <br /> [website] <br /> [email]</p>\r\n<hr />', '<p>Hello, <br /><br /> Thanks for choosing [title] <br /> You now may download the file ([filename]) from here: (link valid [valid] hours)<br /><br /> [url] <br /><br /> You may also receive a separate Transaction ID from Payment System.<br /><br /></p>\r\n<hr />\r\n<p>Please contact us if you have any questions/problems:<br /><br /> [site] <br /> [website] <br /> [email]</p>\r\n<hr />', 'a:1:{s:7:\"subject\";s:35:\"[site]: Download Access for [title]\";}'),
('downloadpage', '<p style=\"text-align: center;\"><span class=\"componentheading\">[title] Download Page<br /></span></p>\r\n<p style=\"text-align: center;\"><span class=\"componentheading\"><br /></span></p>\r\n<table style=\"width: 100%;\" align=\"center\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td rowspan=\"8\" valign=\"top\" width=\"20%\"><br /></td>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Name:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[title]</strong></td>\r\n<td rowspan=\"8\" valign=\"top\" width=\"20%\"><br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>File Name:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[filename]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>File Size:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[filesize]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Created:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[created]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Modified:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[modified]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Total Downloads:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[downloads]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Download Link:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><span style=\"font-size: 14pt;\"><strong><a href=\"[filelink]\">Click here to download [title]</a></strong></span></td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" align=\"left\" valign=\"top\">\r\n<hr />\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<p style=\"text-align: center;\"><span class=\"componentheading\">[title] Download Page<br /></span></p>\r\n<p style=\"text-align: center;\"><span class=\"componentheading\"><br /></span></p>\r\n<table style=\"width: 100%;\" align=\"center\" border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td rowspan=\"8\" valign=\"top\" width=\"20%\"><br /></td>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Name:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[title]</strong></td>\r\n<td rowspan=\"8\" valign=\"top\" width=\"20%\"><br /></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>File Name:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[filename]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>File Size:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[filesize]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Created:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[created]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Modified:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[modified]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Total Downloads:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><strong>[downloads]</strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color: #d7d8d4;\" align=\"right\" height=\"10\" valign=\"top\"><strong>Download Link:</strong></td>\r\n<td align=\"left\" height=\"10\" valign=\"top\"><span style=\"font-size: 14pt;\"><strong><a href=\"[filelink]\">Click here to download [title]</a></strong></span></td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" align=\"left\" valign=\"top\">\r\n<hr />\r\n<p> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'a:0:{}'),
('redirect', '<h3 class=\"componentheading\" style=\"text-align: center;\">Your order is being processed and you will soon be redirected to the payment system website.</h3>\r\n<p style=\"text-align: center;\"><br /><br />If you are not automatically redirected to payment website within a few seconds...<br />[button]</p>\r\n<p style=\"text-align: center;\"> </p>', '<h3 class=\"componentheading\" style=\"text-align: center;\">Your order is being processed and you will soon be redirected to the payment system website.</h3>\r\n<p style=\"text-align: center;\"><br /><br />If you are not automatically redirected within a few seconds...<br />[button]</p>\r\n<p style=\"text-align: center;\"> </p>', 'a:1:{s:11:\"button_text\";s:10:\"Click here\";}'),
('adminemail', '<p>An instant payment notification was successfully received by payment system!</p>\r\n<p>For: [title]<br />Filename: [filename]<br />User: [user]<br />Username: [username]<br />User ID: [userid]<br />User Email: [useremail]<br />Payment Email: [paymentemail]<br />Transaction ID: [transactionid]<br />Time and Date: [time]<br />Payment Type: [type]<br />Amount Paid: [amount]<br />Special Instructions From Buyer:</p>\r\n<p>[instructions]</p>\r\n<hr />\r\n<p>All Returned Payment System Data:</p>\r\n<p>[alldata]</p>', '<p>An instant payment notification was successfully received by Payment System!</p>\r\n<p>For: [title]<br />Filename: [filename]<br />User: [user]<br />Username: [username]<br />User ID: [userid]<br />User Email: [useremail]<br />Payment Email: [paymentemail]<br />Transaction ID: [transactionid]<br />Time and Date: [time]<br />Payment Type: [type]<br />Amount Paid: [amount]<br />Special Instructions From Buyer:</p>\r\n<p>[instructions]</p>\r\n<hr />\r\n<p>All Returned Payment System Data:</p>\r\n<p>[alldata]</p>', 'a:3:{s:7:\"subject\";s:26:\"[title] Ordered via Payment System\";s:7:\"emailto\";a:0:{}s:12:\"otheremailto\";s:0:\"\";}');