<?php

defined('_JEXEC') or die('Restricted access');


class com_jefsInstallerScript
{
    protected $_jefs_extension = 'com_jefs';
	function install($parent) 
	{
        // Install modules and plugins -- BEGIN

        // -- General settings
        jimport('joomla.installer.installer');
        $db = JFactory::getDBO();
        $status = new JObject();
        $status->modules = array();
        $status->plugins = array();
        $src = dirname(__FILE__);

        // -- Module mod_jefs --
        $installer = new JInstaller;
        $result = $installer->install($src.DS.'mod_jefs');
        $status->modules[] = array('name'=>'mod_jefs','client'=>'site', 'result'=>$result);

        // plugins
        $installer = new JInstaller;
        $result = $installer->install($src.DS.'plg_jefslink');
        $status->plugins[] = array('name'=>'plg_jefslink','group'=>'editors-xtd', 'result'=>$result);

        $installer = new JInstaller;
        $result = $installer->install($src.DS.'plg_jefs');
        $status->plugins[] = array('name'=>'plg_jefs_lazy','group'=>'content', 'result'=>$result);

        $rows = 0;
        ?>
        <table class="adminlist">
            <thead>
                <tr>
                    <th class="title" colspan="2"><?php echo JText::_('Extension'); ?></th>
                    <th width="30%"><?php echo JText::_('Status'); ?></th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="3"></td>
                </tr>
            </tfoot>
            <tbody>
                <tr class="row0">
                    <td class="key" colspan="2"><?php echo 'J!Easy File Sale '.JText::_('Component'); ?></td>
                    <td><strong><?php echo JText::_('Installed'); ?></strong></td>
                </tr>
                <?php if (count($status->modules)) : ?>
                <tr>
                    <th><?php echo JText::_('Module'); ?></th>
                    <th><?php echo JText::_('Client'); ?></th>
                    <th></th>
                </tr>
                <?php foreach ($status->modules as $module) : ?>
                <tr class="row<?php echo (++ $rows % 2); ?>">
                    <td class="key"><?php echo $module['name']; ?></td>
                    <td class="key"><?php echo ucfirst($module['client']); ?></td>
                    <td><strong><?php echo ($module['result'])?JText::_('Installed'):JText::_('Not installed'); ?></strong></td>
                </tr>
                <?php endforeach;?>
                <?php endif;?>
                <?php if (count($status->plugins)) : ?>
                <tr>
                    <th><?php echo JText::_('Plugin'); ?></th>
                    <th><?php echo JText::_('Group'); ?></th>
                    <th></th>
                </tr>
                <?php foreach ($status->plugins as $plugin) : ?>
                <tr class="row<?php echo (++ $rows % 2); ?>">
                    <td class="key"><?php echo ucfirst($plugin['name']); ?></td>
                    <td class="key"><?php echo ucfirst($plugin['group']); ?></td>
                    <td><strong><?php echo ($plugin['result'])?JText::_('Installed'):JText::_('Not installed'); ?></strong></td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <?php

        /* v1.1.1 Copy images to the Images Folder from administrator */
        jimport( 'joomla.filesystem.folder' );
        $target = JPATH_ROOT . DS .'images'. DS .'jefs'. DS . 'buttonimages';
        $source = dirname(__FILE__) . DS . 'admin' . DS . 'assets' . DS . 'buttonimages';
        if(!JFolder::exists($target))
        {
            JFolder::copy($source,$target);
        }
    }
 
	/**
	 * method to uninstall the component
	 *
	 * @return void
	 */
	function uninstall($parent) 
	{
        jimport('joomla.installer.installer');
        $db = JFactory::getDBO();
        $status = new JObject();
        $status->modules = array();
        $status->plugins = array();
        $src = $src = dirname(__FILE__);

        // -- Module mod_jefs --
        $db->setQuery('SELECT `extension_id` FROM #__extensions WHERE `element` = "mod_jefs" AND `type` = "module"');
        $id = $db->loadResult();
        if($id)
        {
        	$installer = new JInstaller;
        	$result = $installer->uninstall('module',$id,1);
        	$status->modules[] = array('name'=>'JEasy File Sale','client'=>'site', 'result'=>$result);
        }

        // PLugin plg_jefslink
        $db->setQuery('SELECT `extension_id` FROM #__extensions WHERE `type` = "plugin" AND `element` = "jefslink" AND `folder` = "editors-xtd"');
        $id = $db->loadResult();
        if($id)
        {
        	$installer = new JInstaller;
        	$result = $installer->uninstall('plugin',$id,1);
        	$status->plugins[] = array('name'=>'Button - JEasy File Sale Link','group'=>'editors-xtd', 'result'=>$result);
        }

        //Plugin plg_jefs
        // PLugin plg_jefslink
        $db->setQuery('SELECT `extension_id` FROM #__extensions WHERE `type` = "plugin" AND `element` = "jefs" AND `folder` = "content"');
        $id = $db->loadResult();
        if($id)
        {
            $installer = new JInstaller;
            $result = $installer->uninstall('plugin',$id,1);
            $status->plugins[] = array('name'=>'Content - JEasy File Sale','group'=>'content', 'result'=>$result);
        }
        ?>
    <?php $rows = 0;?>
    <h2><?php echo JText::_('J!Easy File Sale Uninstallation Status'); ?></h2>
    <table class="adminlist">
    	<thead>
    		<tr>
    			<th class="title" colspan="2"><?php echo JText::_('Extension'); ?></th>
    			<th width="30%"><?php echo JText::_('Status'); ?></th>
    		</tr>
    	</thead>
    	<tfoot>
    		<tr>
    			<td colspan="3"></td>
    		</tr>
    	</tfoot>
    	<tbody>
    		<tr class="row0">
    			<td class="key" colspan="2"><?php echo 'J!Easy File Sale '.JText::_('Component'); ?></td>
    			<td><strong><?php echo JText::_('Removed'); ?></strong></td>
    		</tr>
    		<?php if (count($status->modules)) : ?>
    		<tr>
    			<th><?php echo JText::_('Module'); ?></th>
    			<th><?php echo JText::_('Client'); ?></th>
    			<th></th>
    		</tr>
    		<?php foreach ($status->modules as $module) : ?>
    		<tr class="row<?php echo (++ $rows % 2); ?>">
    			<td class="key"><?php echo $module['name']; ?></td>
    			<td class="key"><?php echo ucfirst($module['client']); ?></td>
    			<td><strong><?php echo ($module['result'])?JText::_('Removed'):JText::_('Not removed'); ?></strong></td>
    		</tr>
    		<?php endforeach;?>
    		<?php endif;?>
    		<?php if (count($status->plugins)) : ?>
    		<tr>
    			<th><?php echo JText::_('Plugin'); ?></th>
    			<th><?php echo JText::_('Group'); ?></th>
    			<th></th>
    		</tr>
    		<?php foreach ($status->plugins as $plugin) : ?>
    		<tr class="row<?php echo (++ $rows % 2); ?>">
    			<td class="key"><?php echo ucfirst($plugin['name']); ?></td>
    			<td class="key"><?php echo ucfirst($plugin['group']); ?></td>
    			<td><strong><?php echo ($plugin['result'])?JText::_('Removed'):JText::_('Not removed'); ?></strong></td>
    		</tr>
    		<?php endforeach; ?>
    		<?php endif; ?>
    	</tbody>
    </table>

        <?php
    }
 
	/**
	 * method to update the component
	 *
	 * @return void
	 */
	function update($parent) 
	{
        // Install modules and plugins -- BEGIN

        // -- General settings
        jimport('joomla.installer.installer');

        /* v1.1.0 Support Unregistered  */
        $db = JFactory::getDBO();
        $q = "CREATE TABLE IF NOT EXISTS `#__jefs_nonregistered`(
                    `id` INT(11) UNSIGNED NOT NULL auto_increment,
                    `email` VARCHAR(256) NOT NULL,
                    `created` DATETIME NOT NULL,
                    `fileid` INT(11) NOT NULL,
                    `donation` TEXT DEFAULT NULL,
                    `validUntil` DATETIME NOT NULL,
                    `hash` CHAR(32) NOT NULL,
                    `downloadHash` CHAR(32) NOT NULL,
                    `status` INT(11) NOT NULL DEFAULT 0,
                    PRIMARY KEY (`id`)
                  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        $db->setQuery($q);
        if(!$db->query()) JFactory::getApplication()->enqueueMessage('Failed to create NonRegister Table','warning');

        $q = "INSERT IGNORE INTO `#__jefs_templates` (`type`, `default`, `html`, `options`) VALUES
('emailUnregistered', '<p>Hello, <br /><br /> Thanks for choosing [title] <br /> You now may download the file ([filename]) from here: (link valid [valid] hours)<br /><br /> [url] <br /><br /> You may also receive a separate Transaction ID from Payment System.<br /><br /></p>\r\n<hr />\r\n<p>Please contact us if you have any questions/problems:<br /><br /> [site] <br /> [website] <br /> [email]</p>\r\n<hr />', '<p>Hello, <br /><br /> Thanks for choosing [title] <br /> You now may download the file ([filename]) from here: (link valid [valid] hours)<br /><br /> [url] <br /><br /> You may also receive a separate Transaction ID from Payment System.<br /><br /></p>\r\n<hr />\r\n<p>Please contact us if you have any questions/problems:<br /><br /> [site] <br /> [website] <br /> [email]</p>\r\n<hr />', 'a:1:{s:7:\"subject\";s:35:\"[site]: Download Access for [title]\";}'),
('purchase', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>Price: <span style=\"color: #ff0000;\">[price]</span></strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[currencylist]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">\r\n[email_input_lbl] [email_input]<br>[email_input_lbl_desc]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[paymentgateway]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>Price: <span style=\"color: #ff0000;\">[price]</span></strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[currencylist]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">\r\n[email_input_lbl] [email_input]<br>[email_input_lbl_desc]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[paymentgateway]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'a:1:{s:9:\"curr_code\";i:1;}'),
('donate', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>[mindonation]</strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">Amount: [amountbox] [currencylist]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">\r\n[email_input_lbl] [email_input]<br>[email_input_lbl_desc]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[paymentgateway]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[dlbutton]</td>\r\n</tr>\r\n</tbody>\r\n</table>', '<table border=\"0\">\r\n<tbody>\r\n<tr>\r\n<td align=\"center\">[title] ([filename])</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\"><strong>[mindonation]</strong></td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">Amount: [amountbox] [currencylist]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">\r\n[email_input_lbl] [email_input]<br>[email_input_lbl_desc]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[paymentgateway]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[button]</td>\r\n</tr>\r\n<tr>\r\n<td align=\"center\">[dlbutton]</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'a:3:{s:11:\"button_text\";s:57:\"Minimum Donation: <span style=\"color:red;\">[price]</span>\";s:7:\"boxsize\";s:1:\"5\";s:9:\"curr_code\";i:1;}');";
        if(!$db->query()) JFactory::getApplication()->enqueueMessage('Failed to create NonRegister Table','warning');

        /* v1.1.1 Copy images to the Images Folder from administrator */
        jimport( 'joomla.filesystem.folder' );
        $target = JPATH_ROOT . DS .'images'. DS .'jefs'. DS . 'buttonimages';
        $source = dirname(__FILE__) . DS . 'admin' . DS . 'assets' . DS . 'buttonimages';
        if(!JFolder::exists($target))
        {
            JFolder::copy($source,$target);
        }


        /**
         * v1.1.2 Added plugin Currency support control
         *  added plugin Icon
         */
    }
	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent) 
	{
        if(in_array($type, array('install','discover_install'))) {
            $this->_bugfixDBFunctionReturnedNoError();
        } else {
            $this->_bugfixCantBuildAdminMenus();
        }
    }
 
	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent) 
	{

	}
    private function _bugfixCantBuildAdminMenus()
    	{
    		$db = JFactory::getDbo();
    		
    		// If there are multiple #__extensions record, keep one of them
    		$query = $db->getQuery(true);
    		$query->select('extension_id')
    			->from('#__extensions')
    			->where($db->nameQuote('element').' = '.$db->Quote($this->_jefs_extension));
    		$db->setQuery($query);
    		$ids = $db->loadResultArray();
    		if(count($ids) > 1) {
    			asort($ids);
    			$extension_id = array_shift($ids); // Keep the oldest id
    			
    			foreach($ids as $id) {
    				$query = $db->getQuery(true);
    				$query->delete('#__extensions')
    					->where($db->nameQuote('extension_id').' = '.$db->Quote($id));
    				$db->setQuery($query);
    				$db->query();
    			}
    		}
    		
    		// If there are multiple assets records, delete all except the oldest one
    		$query = $db->getQuery(true);
    		$query->select('id')
    			->from('#__assets')
    			->where($db->nameQuote('name').' = '.$db->Quote($this->_jefs_extension));
    		$db->setQuery($query);
    		$ids = $db->loadObjectList();
    		if(count($ids) > 1) {
    			asort($ids);
    			$asset_id = array_shift($ids); // Keep the oldest id
    			
    			foreach($ids as $id) {
    				$query = $db->getQuery(true);
    				$query->delete('#__assets')
    					->where($db->nameQuote('id').' = '.$db->Quote($id));
    				$db->setQuery($query);
    				$db->query();
    			}
    		}
    
    		// Remove #__menu records for good measure!
    		$query = $db->getQuery(true);
    		$query->select('id')
    			->from('#__menu')
    			->where($db->nameQuote('type').' = '.$db->Quote('component'))
    			->where($db->nameQuote('menutype').' = '.$db->Quote('main'))
    			->where($db->nameQuote('link').' LIKE '.$db->Quote('index.php?option='.$this->_jefs_extension));
    		$db->setQuery($query);
    		$ids1 = $db->loadResultArray();
    		if(empty($ids1)) $ids1 = array();
    		$query = $db->getQuery(true);
    		$query->select('id')
    			->from('#__menu')
    			->where($db->nameQuote('type').' = '.$db->Quote('component'))
    			->where($db->nameQuote('menutype').' = '.$db->Quote('main'))
    			->where($db->nameQuote('link').' LIKE '.$db->Quote('index.php?option='.$this->_jefs_extension.'&%'));
    		$db->setQuery($query);
    		$ids2 = $db->loadResultArray();
    		if(empty($ids2)) $ids2 = array();
    		$ids = array_merge($ids1, $ids2);
    		if(!empty($ids)) foreach($ids as $id) {
    			$query = $db->getQuery(true);
    			$query->delete('#__menu')
    				->where($db->nameQuote('id').' = '.$db->Quote($id));
    			$db->setQuery($query);
    			$db->query();
    		}
    	}
    private function _bugfixDBFunctionReturnedNoError()
    	{
    		$db = JFactory::getDbo();
    			
    		// Fix broken #__assets records
    		$query = $db->getQuery(true);
    		$query->select('id')
    			->from('#__assets')
    			->where($db->nameQuote('name').' = '.$db->Quote($this->_jefs_extension));
    		$db->setQuery($query);
    		$ids = $db->loadResultArray();
    		if(!empty($ids)) foreach($ids as $id) {
    			$query = $db->getQuery(true);
    			$query->delete('#__assets')
    				->where($db->nameQuote('id').' = '.$db->Quote($id));
    			$db->setQuery($query);
    			$db->query();
    		}
    
    		// Fix broken #__extensions records
    		$query = $db->getQuery(true);
    		$query->select('extension_id')
    			->from('#__extensions')
    			->where($db->nameQuote('element').' = '.$db->Quote($this->_jefs_extension));
    		$db->setQuery($query);
    		$ids = $db->loadResultArray();
    		if(!empty($ids)) foreach($ids as $id) {
    			$query = $db->getQuery(true);
    			$query->delete('#__extensions')
    				->where($db->nameQuote('extension_id').' = '.$db->Quote($id));
    			$db->setQuery($query);
    			$db->query();
    		}
    
    		// Fix broken #__menu records
    		$query = $db->getQuery(true);
    		$query->select('id')
    			->from('#__menu')
    			->where($db->nameQuote('type').' = '.$db->Quote('component'))
    			->where($db->nameQuote('menutype').' = '.$db->Quote('main'))
    			->where($db->nameQuote('link').' LIKE '.$db->Quote('index.php?option='.$this->_jefs_extension));
    		$db->setQuery($query);
    		$ids = $db->loadResultArray();
    		if(!empty($ids)) foreach($ids as $id) {
    			$query = $db->getQuery(true);
    			$query->delete('#__menu')
    				->where($db->nameQuote('id').' = '.$db->Quote($id));
    			$db->setQuery($query);
    			$db->query();
    		}
    	}
}

;?>