<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */


// todo check for delete
defined('_JEXEC') or die( 'Restricted access' );

class JElementJefs extends JElement
{
	
	var	$_name = 'acfile';

	function fetchElement($name, $value, &$node, $control_name)
	{
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jefs'.DS.'tables');

		
		$db			=JFactory::getDBO();
		$doc 		=JFactory::getDocument();
		$fieldName	= $control_name.'['.$name.']';
		$file =& JTable::getInstance('Files', 'Table');
		
		if ($value) {
			$file->load($value);
		} else {
			$file->title = JText::_('*Select File*');
		}
		
		$js = "
		function selectJefs(id, title, object) {
		  document.getElementById(object + '_id').value = id;
		  document.getElementById(object + '_name').value = title;
		  document.getElementById('sbox-window').close();
		}";
		$doc->addScriptDeclaration($js);

		$link = 'index.php?option=com_jefs&amp;view=files&amp;tmpl=component&amp;object='.$name;

		JHTML::_('behavior.modal', 'a.modal');
		$html = "\n".'<div style="float: left;"><input style="background: #ffffff;" type="text" size="40" id="'.$name.'_name" value="'.htmlspecialchars($file->title, ENT_QUOTES, 'UTF-8').'" disabled="disabled" /></div>';
		$html .= '<div class="button2-left"><div class="blank"><a class="modal" title="'.JText::_('*Select File*').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 650, y: 375}}">'.JText::_('Select').'</a></div></div>'."\n";
		$html .= "\n".'<input type="hidden" id="'.$name.'_id" name="params['.$name.']" value="'.$value.'" />';
		
		return $html;
	  
	}
}
