<?php
/**
 * CHANGELOG
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access to this changelog...
defined('_JEXEC') or die('No Direct Access');

//--For phpDocumentor documentation we need to construct a function
/**
 * CHANGELOG
 * {@source}
 */
function CHANGELOG()
{
/*
_______________________________________________
_______________________________________________

This is the changelog for jefs

Please be patient =;)
_______________________________________________
_______________________________________________

Legend:

* -> Security Fix
# -> Bug Fix
+ -> Addition
^ -> Change
- -> Removed
! -> Note
______________________________________________

--------------- 1.0.1 beta -- [22-Dec-2009] ---------------------

22-Dec-2009 Angel Coding
 # Fixed content plugin bug that caused all payment buttons (displayed using the content plugin tag), which use a dollar currency as default (e.g. USD, CAD, AUD), to remove the first digit of the payment amount.
 ^ Changed the install and uninstall methods so that upgrading is easier.
 +^ Moved the ID column in all Admin tables to the right and added a result number column on the left to keep in line with other Joomla extensions.



--------------- 1.0.0 beta -- [13-Sep-2009] ---------------------

13-Sep-2009 Angel Coding
 ! Startup

*/
}//--This is the END