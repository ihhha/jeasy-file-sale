<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

class TableFiles extends JTable
{

	var $id = null;
	var $filename = null;
	var $title = null;
	var $alias = null;
	var $parent = null;
	var $created = null;
	var $modified = null;
	var $auto_modified = null;
	var $downloads = null;
	var $downloads_add = null;
	var $purchases = null;
	var $published = null;
	var $path = null;
	var $description = null;
	var $purchase_button = null;
	var $price_options = null;
	var $link_type = null;
	var $return_page = null;
	var $allow_download = null;
	var $allow_children = null;
	var $donations = null;

	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function TableFiles(& $db) {
		parent::__construct('#__jefs_files', 'id', $db);
	}
}
?>
