<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

class TableTemplates extends JTable
{

	var $type = null;
	var $default = null;
	var $html = null;
	var $options = null;

	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function TableTemplates(& $db) {
		parent::__construct('#__jefs_templates', 'type', $db);
	}
}
?>
