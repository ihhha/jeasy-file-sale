<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */



// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );



jimport( 'joomla.application.component.controller' );

/**

 * @package		Joomla

 * @subpackage	Banners

 */

class jefsControllerPayments extends JController

{

    function __construct( $config = array() )

	{

		parent::__construct( $config );

	}

    function display()

    {

        JRequest::setVar('view', 'payments');

        parent::display();

	}

}