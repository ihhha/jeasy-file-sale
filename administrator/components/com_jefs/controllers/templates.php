<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport('joomla.application.component.controller');

class jefsControllerTemplates extends jefsController
{

	function __construct()
	{
		JRequest::setVar( 'view', 'Templates' );
		parent::__construct();

	}

	function save()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model = $this->getModel('Templates');
		
		if ($model->store()) {
			$msg = JText::_( 'Templates Saved!' );
		} else {
			$msg = JText::_( 'Error Saving Templates' );
		}

		$link = 'index.php?option=com_jefs&controller=templates';

		$this->setRedirect($link, $msg);
	}
	
}