<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport('joomla.application.component.controller');

class jefsControllerConfig extends jefsController
{
	function __construct()
	{
		$view = JRequest::getVar( 'view', 'Config' );
		JRequest::setVar( 'view', $view );
		parent::__construct();

	}

}