<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport('joomla.application.component.controller');

class jefsControllerFiles extends jefsController
{

	function __construct()
	{
		JRequest::setVar( 'view', 'Files' );
		parent::__construct();

		$this->registerTask( 'add' , 'edit' );
		$this->registerTask( 'apply', 'save' );
		$this->registerTask( 'save2new', 'save' );
		$this->registerTask( 'unpublish', 'publish' );
	}

	function edit()
	{
		// Check for request forgeries
		JRequest::checkToken() or JRequest::checkToken('get') or jexit( 'Invalid Token' );
		JRequest::setVar( 'view', 'File' );
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);

		parent::display();
	}

	function save()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model = $this->getModel('File');
		
		$itemid = $model->store();
		
		if ($itemid) {
			$msg = JText::_( 'File Item Saved' );
		} else {
			$msg = JText::_( 'Error Saving File Item' );
		}
		$task = JRequest::getCmd( 'task' );
		switch ($task)
		{
			case 'apply':
				$link = 'index.php?option=com_jefs&controller=files&task=edit&cid[]='. $itemid.'&'.JUtility::getToken().'=1' ;
				break;
            case 'save2new':
                $link = 'index.php?option=com_jefs&controller=files&task=edit&cid[]=0&'.JUtility::getToken().'=1' ;
                break;
            case 'save':
			default:
				$link = 'index.php?option=com_jefs&controller=files';
				break;
		}

		$this->setRedirect($link, $msg);
	}

	function remove()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model = $this->getModel('File');
		if(!$model->delete()) {
			$msg = JText::_( 'Error: One or More File Items Could not be Deleted' );
		} else {
			$msg = JText::_( 'File Item(s) Deleted' );
		}

		$this->setRedirect( 'index.php?option=com_jefs&controller=files', $msg );
	}

	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_jefs&controller=files', $msg );
	}
	
	function publish()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model = $this->getModel('Files');
		$task	= $this->getTask();
		$link = $model->publish($task);
		$task == 'publish' ? $msg = JText::_( 'Item Published' ) : $msg = JText::_( 'Item Unpublished' );
		$this->setRedirect( $link, $msg );
	}
	
	function copyAccess()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model	=$this->getModel( 'File' );
		$view =$this->getView( 'File', 'html' );
		$view->setModel( $model, true );
		$view->copyForm();
	}
	
	function doCopy()
	{
		$mainframe = JFactory::getApplication();

		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model	=$this->getModel( 'File' );
		// Get some variables from the request
		$menu	 	= JRequest::getString( 'menu', '', 'post');
		$cid		= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		foreach($cid as $fromid) {
			$files		= JRequest::getVar( 'files_'.$fromid, array(), 'post', 'array' );
			if (empty($files))
			{
				$msg = JText::_('Error: Please make sure that all list boxes have at least one file selected');
				$mainframe->enqueueMessage($msg, 'message');
				return $this->execute('copyAccess');
			}
			
			if (!$model->copyAccess($fromid, $files)) {
				$msg = $model->getError();
				$this->setRedirect( 'index.php?option=com_jefs&controller=files', $msg );
				return;
			}
		}
		$msg = JText::_('File Item(s) User Acces information Copied Successfully');
		$this->setRedirect( 'index.php?option=com_jefs&controller=files', $msg );
	}
	
	function copyItem()
	{
		$mainframe = JFactory::getApplication();

		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model	=$this->getModel( 'File' );
		$cids		= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		foreach($cids as $cid) {
			if (!$model->copyItem($cid)) {
				$msg = JText::_('Error: One or More File Items Could not be Copied');
				$this->setRedirect( 'index.php?option=com_jefs&controller=files', $msg );
				return;
			}
		}
		$msg = JText::_('File Item(s) Copied Successfully');
		$this->setRedirect( 'index.php?option=com_jefs&controller=files', $msg );
	}
	
	function savedefault()
	{
		$model	=$this->getModel( 'File' );
		$response = $model->saveDefault();
		
		$view =$this->getView( 'File', 'html' );
		$view->setModel( $model, true );
		$view->jsonResponse($response);
	}
	
	function loaddefault()
	{
		$model	=$this->getModel( 'File' );
		$response = $model->loadDefault();
		
		$view =$this->getView( 'File', 'html' );
		$view->setModel( $model, true );
		$view->jsonResponse($response);
	}
	
}// class