<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport('joomla.application.component.controller');

class jefsControllerUsers extends jefsController
{

	function __construct()
	{
		JRequest::setVar( 'view', 'Users' );
		parent::__construct();

		$this->registerTask( 'add' , 'edit' );
		$this->registerTask( 'apply', 'save' );
	}

	function edit()
	{
		JRequest::checkToken() or JRequest::checkToken('get') or jexit( 'Invalid Token' );
		JRequest::setVar( 'view', 'User' );
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);

		parent::display();
	}

	function save()
	{
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$model = $this->getModel('User');
		
		$itemid = $model->store();
		
		if ($itemid) {
			$msg = JText::_( 'User Saved!' );
		} else {
			$msg = JText::_( 'Error Saving User' );
		}
		$task = JRequest::getCmd( 'task' );
		switch ($task)
		{
			case 'apply':
				$link = 'index.php?option=com_jefs&controller=users&task=edit&cid[]='. $itemid.'&'.JUtility::getToken().'=1' ;
				break;

			case 'save':
			default:
				$link = 'index.php?option=com_jefs&controller=users';
				break;
		}

		$this->setRedirect($link, $msg);
	}

	function cancel()
	{
		$msg = JText::_( 'Operation Cancelled' );
		$this->setRedirect( 'index.php?option=com_jefs&controller=users', $msg );
	}

}