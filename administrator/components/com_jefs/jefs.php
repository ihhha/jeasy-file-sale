<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

require_once( JPATH_COMPONENT.DS.'controller.php' );
$controller = JRequest::getWord( 'controller', 'config' );


JPluginHelper::importPlugin( 'jefs' );


// Setup submenus
if($controller == 'files') {
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_FILES'), 'index.php?option=com_jefs&controller=files', true );
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_USERS'), 'index.php?option=com_jefs&controller=users');
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_TEMPLATES'), 'index.php?option=com_jefs&controller=templates');
    JSubMenuHelper::addEntry(JText::_('COM_JEFS_PAYMENTS'), 'index.php?option=com_jefs&controller=payments' );
    JSubMenuHelper::addEntry(JText::_('COM_JEFS_CONFIG'), 'index.php?option=com_jefs');
}
else if($controller == 'users') {
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_FILES'), 'index.php?option=com_jefs&controller=files');
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_USERS'), 'index.php?option=com_jefs&controller=users', true );
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_TEMPLATES'), 'index.php?option=com_jefs&controller=templates');
    JSubMenuHelper::addEntry(JText::_('COM_JEFS_PAYMENTS'), 'index.php?option=com_jefs&controller=payments' );
    JSubMenuHelper::addEntry(JText::_('COM_JEFS_CONFIG'), 'index.php?option=com_jefs');
}
else if($controller == 'templates') {
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_FILES'), 'index.php?option=com_jefs&controller=files');
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_USERS'), 'index.php?option=com_jefs&controller=users');
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_TEMPLATES'), 'index.php?option=com_jefs&controller=templates', true );
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_PAYMENTS'), 'index.php?option=com_jefs&controller=payments' );
    JSubMenuHelper::addEntry(JText::_('COM_JEFS_CONFIG'), 'index.php?option=com_jefs');
}
else if($controller == 'payments') {
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_FILES'), 'index.php?option=com_jefs&controller=files');
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_USERS'), 'index.php?option=com_jefs&controller=users');
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_TEMPLATES'), 'index.php?option=com_jefs&controller=templates' );
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_PAYMENTS'), 'index.php?option=com_jefs&controller=payments', true );
    JSubMenuHelper::addEntry(JText::_('COM_JEFS_CONFIG'), 'index.php?option=com_jefs');
}
else {
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_FILES'), 'index.php?option=com_jefs&controller=files' );
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_USERS'), 'index.php?option=com_jefs&controller=users');
	JSubMenuHelper::addEntry(JText::_('COM_JEFS_TEMPLATES'), 'index.php?option=com_jefs&controller=templates');
    JSubMenuHelper::addEntry(JText::_('COM_JEFS_PAYMENTS'), 'index.php?option=com_jefs&controller=payments' );
    JSubMenuHelper::addEntry(JText::_('COM_JEFS_CONFIG'), 'index.php?option=com_jefs', true );
}

$path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
if( file_exists($path))
{
	require_once $path;
} 

require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'jefshelper.class.php' );

$classname    = 'jefsController'.$controller;
$controller   = new $classname( );
$controller->execute( JRequest::getVar( 'task' ) );
$controller->redirect();
