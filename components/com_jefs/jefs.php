<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

// Require the base controller
require_once( JPATH_COMPONENT.DS.'controller.php' );

// Require specific controller if requested
if( $controller = JRequest::getWord('controller'))
{
   $path = JPATH_COMPONENT.DS.'controllers'.DS.$controller.'.php';
   if( file_exists($path))
	{
       require_once $path;
   } else
   {
       $controller = '';
   }
}

require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'jefshelper.class.php' );
JPluginHelper::importPlugin( 'jefs' );

// Create the controller
$classname    = 'jefsController'.$controller;
$controller   = new $classname( );
$task = JRequest::getVar( 'task' );
jefshelper::logEntry('JEFS: Controller name - ' . $classname,JLog::INFO,'component');
jefshelper::logEntry('JEFS: Task name - ' . $task ,JLog::INFO,'component');

// Perform the Request task
$controller->execute( $task );

// Redirect if set by the controller
$controller->redirect();