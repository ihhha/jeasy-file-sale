<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

function jefsBuildRoute( &$query )
{
	$segments = array();

	if (isset($query['view'])) {
		$segments[] = $query['view'];
		unset( $query['view'] );
	}
	if (isset($query['acid'])) {
		// $acid = explode( ':', $query['acid'] );
		// $segments[] = $acid[1];
		$segments[] = $query['acid'];
		unset( $query['acid'] );
	}
	if (isset($query['task'])) {
		$segments[] = $query['task'];
		unset( $query['task'] );
	}
	if (isset($query['url'])) {
		// $segments[] = $query['url'];
		unset( $query['url'] );
	}
	if (isset($query['type'])) {
		// $segments[] = $query['url'];
        $segments[] = $query['type'];
		unset( $query['type'] );
	}
    return $segments;
}


function jefsParseRoute( $segments )
{
	$vars = array();
//	$menu =& JMenu::getInstance();
//    $item =& $menu->getActive();
	$count = count( $segments );

	switch($segments[0])
       {
	   		case 'success':
				$vars['task'] = 'success';
				$vars['type'] = $segments[1];
				break;
	   		case 'download':
				$vars['view'] = 'download';
				// $acid = explode( ':', $segments[1] );
                // $vars['acid'] = (int) $acid[0];
				$vars['acid'] = $segments[1];
				if($count >= 3) $vars['task'] = $segments[2];
				break;
			case 'downloads':
				$vars['view'] = 'downloads';
				break;
			case 'ipn':
				$vars['task'] = 'ipn';
				break;
			case 'cancel':
				$vars['task'] = 'cancel';
				break;
            case 'validate':
                $vars['task'] = 'validate';
                break;
       }

	
	// if(!$item) $vars['task'] = 'success';

	return $vars;
}