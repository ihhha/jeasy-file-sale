<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.model' );

class jefsModelUser extends JModel
{
	var $_userdata = null;

	function __construct()
	{
		parent::__construct();

		$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);
	}//function

	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}//function

	function &getData()
	{
			$query = ' SELECT * FROM #__users '.
					'  WHERE id = '.$this->_id;
			$this->_db->setQuery( $query );
			$this->_userdata = $this->_db->loadObject();
			
			$query = ' SELECT access, donations FROM #__jefs_users '.
					'  WHERE id = '.$this->_id;
			$this->_db->setQuery( $query );
			$acuser = $this->_db->loadObject();
			
			if (!empty($acuser->access)) {
				$acuseraccess = unserialize($acuser->access);
				$this->_userdata->file_access = $acuseraccess['files'];
			}
			else {
				$this->_userdata->file_access = array();
			}
			if (!empty($acuser->donations)) {
				$acuserdonations = unserialize($acuser->donations);
				$this->_userdata->donations = $acuserdonations;
			}
			else {
				$this->_userdata->donations = array();
			}
	
		return $this->_userdata;
	}//function

	function store()
	{
		$data = JRequest::get( 'post' );
	
		$accessarray = array('files' => $data['file_access']);
		$data['access'] = serialize($accessarray);
		// check if user exists in AC user list
		$query = ' SELECT access FROM #__jefs_users WHERE id = '.$data['id'];
		$this->_db->setQuery( $query );
		$acusercheck = $this->_db->loadResult();
		// If it doesn't then we must create an entry, so we might as well process here and return
		if (!$acusercheck) {
			$tmpobject = new stdClass();
			$tmpobject->id = $data['id'];
			$tmpobject->access = $data['access'];
			$this->_db->insertObject('#__jefs_users', $tmpobject, 'id');
			
			return $data['id'];
		}
		// If user does exist then we can carry on and use the table
		$row =$this->getTable('users');

		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if (!$row->store()) {
			$this->setError( $this->_db->getErrorMsg() );
			return false;
		}

		return $data['id'];
	}//function

	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		$row =$this->getTable();

		if (count( $cids ))
		{
			foreach($cids as $cid) {
				if (!$row->delete( $cid )) {
					$this->setError( $row->getErrorMsg() );
					return false;
				}
			}//foreach
		}
		return true;
	}//function
	
	function getImages()
	{
		$images = array();
		$imagepath = JPATH_COMPONENT_ADMINISTRATOR.DS.'assets'.DS.'buttonimages';
		$foldertree = JFolder::listFolderTree($imagepath, '.');
		
		foreach($foldertree as $folder) {
			$filelist 	= JFolder::files($folder['fullname']);
			sort($filelist);
			$images[$folder['name']] = $filelist;
		}
		
		return $images;
	}

}// class