<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.model' );

class jefsModelTemplates extends JModel
{

	function __construct()
	{
		parent::__construct();

	}//function

	function &getData()
	{
		$query = ' SELECT * FROM #__jefs_templates ';
		$this->_db->setQuery( $query );
		$result = $this->_db->loadObjectList('type');
		return $result;

	}//function

	function store()
	{
		$row =$this->getTable();
		$types = array('download','purchase','donate','register','email','adminemail', 'redirect','downloadpage');

		foreach($types as $type) {
			$html = JRequest::getVar( $type,'','post','string',JREQUEST_ALLOWRAW );

			$options = array();
			if($type == 'download' || $type == 'redirect' || $type == 'donate') {
				$button_text = JRequest::getVar( $type.'text', '','post','string',JREQUEST_ALLOWRAW );
				$options['button_text'] = $button_text;
			}
			if($type == 'register') {
				$register_button_text = JRequest::getVar( $type.'regtext', '' );
				$login_button_text = JRequest::getVar( $type.'logtext', '' );
				$options['register_button_text'] = $register_button_text;
				$options['login_button_text'] = $login_button_text;
			}
			if($type == 'donate') {
				$boxsize = JRequest::getVar( $type.'size', '5' );
				$options['boxsize'] = $boxsize;
			}
			if($type == 'email' || $type == 'adminemail') {
				$emailsubject = JRequest::getVar( $type.'subject', '' );
				$options['subject'] = $emailsubject;
			}
			if($type == 'donate' || $type == 'purchase') {
				$curr_code = JRequest::getInt( $type.'curr_code', '0' );
				$options['curr_code'] = $curr_code;
			}
			if($type == 'adminemail') {
				$emailto = JRequest::getVar( 'adminemailto', array() );
				$otheremailto = JRequest::getVar( 'otheradminemailto', '','post','string',JREQUEST_ALLOWRAW );
				$options['emailto'] = $emailto;
				$options['otheremailto'] = $otheremailto;
			}
			
			$tmpobject = new stdClass();
			$tmpobject->type = $type;
			$tmpobject->html = $html;
			$tmpobject->options = serialize($options);
			if(!$this->_db->updateObject('#__jefs_templates', $tmpobject, 'type'))
            {
                echo $this->_db->getErrorMsg();
                jexit();
            };

		}

		return true;
	}//function

}// class