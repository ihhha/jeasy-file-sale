<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.model' );

class jefsModelUsers extends JModel
{
	var $_data;
	var $_userdata;
	var $_total = null;
	var $_pagination = null;
	
	function __construct()
	{
	 	parent::__construct();
	
		$mainframe = JFactory::getApplication();$option = JRequest::getCmd('option');
	
		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest($option.'.limitstart', 'limitstart', 0, 'int');
	
		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
	
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}//function

	function _buildQuery()
	{
		$query =  ' SELECT * '
				. ' FROM #__users '
				. $this->_buildQueryWhere()
				. $this->_buildQueryOrderBy();

		return $query;
	}
	
	function _buildQueryWhere()
	{
		$mainframe = JFactory::getApplication();$option = JRequest::getCmd('option');
		$controller = JRequest::getWord( 'controller', 'config' );
	
		// get the filter published state value
		$filter_files = $mainframe->getUserStateFromRequest($option.$controller.'filter_files', 'filter_files');
		$filter_search = $mainframe->getUserStateFromRequest($option.$controller.'filter_search', 'filter_search');
		// prepare the WHERE clause
		$where = array();
		$db =$this->_db;
		// Determine link type
		
		if (!empty($filter_files))
		{
			
			$filter_files = JString::strtolower($filter_files);
			$filter_files = $db->getEscaped($filter_files);
			$query = ' SELECT id FROM #__jefs_users WHERE LOWER(access) LIKE '.$db->quote('%"'.$filter_files.'"%');
			$this->_db->setQuery( $query );
			$userlist = $this->_db->loadObjectList();
			
			if($userlist) {
				$userids = array();
				foreach($userlist as $user) {
					$userids[] = $user->id;
				}
				$userlist = implode(',', $userids);
			}
			else $userlist = '0';
			$where[] = 'id IN('.$userlist.')';
		}
		
		// Determine search terms
		if ($filter_search = trim($filter_search))
		{
			$filter_search = JString::strtolower($filter_search);
			$filter_search = $db->getEscaped($filter_search);
			$where[] = '(LOWER(name) LIKE "%'.$filter_search.'%" OR LOWER(username) LIKE "%'.$filter_search.'%" OR LOWER(email) LIKE "%'.$filter_search.'%")';
		}
	
		// return the WHERE clause
    	return (count($where)) ? ' WHERE '.implode(' AND ', $where) : '';

	}
	
	function _buildQueryOrderBy()
	{
		$mainframe = JFactory::getApplication();$option = JRequest::getCmd('option');
		$controller = JRequest::getWord( 'controller', 'config' );
	
		// Array of allowable order fields
		$orders = array('name', 'username', 'email', 'id');
	
		// get the order field and direction
		$filter_order = $mainframe->getUserStateFromRequest($option.$controller.'filter_order', 'filter_order', 'name');
		$filter_order_Dir = strtoupper($mainframe->getUserStateFromRequest($option.$controller.'filter_order_Dir', 'filter_order_Dir', 'ASC'));
	
		// validate the order direction, must be ASC or DESC
		if ($filter_order_Dir != 'ASC' && $filter_order_Dir != 'DESC')
		{
			$filter_order_Dir = 'ASC';
		}
	
		// if order column is unknown use the default
		if (!in_array($filter_order, $orders))
		{
			$filter_order = 'title';
		}
	
		// return the ORDER BY clause
		return ' ORDER BY '.$filter_order.' '.$filter_order_Dir;
	}

	function getData()
	{
		// Lets load the data if it doesn't already exist
		if (empty( $this->_data ))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList( $query, $this->getState('limitstart'), $this->getState('limit') );
			
		}
		return $this->_data;
	}//function
	
	function getACUsers()
	{
		$query = ' SELECT * FROM #__jefs_users ';
		$this->_db->setQuery( $query );
		return $this->_db->loadAssocList('id');	// key by id for easy access later
	}
	
	function getFiles()
	{
		$query = ' SELECT id,title FROM #__jefs_files ';
		$this->_db->setQuery( $query );
		return $this->_db->loadAssocList('id');	// key by id for easy access later
	}
	
	function getTotal()
	{
		// Load the content if it doesn't already exist
		if (empty($this->_total))
		{
			$query = $this->_buildQuery();
			$this->_total = $this->_getListCount($query);	
		}
		return $this->_total;
	}//function
  
	function getPagination()
	{
		// Load the content if it doesn't already exist
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}
		return $this->_pagination;
	}//function
  
	function publish($task)
	{
		$mainframe = JFactory::getApplication();
		// Initialize some variables
		$db 	=JFactory::getDBO();
		$cid 	= JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		$publish	= ($task == 'publish');
		if (empty( $cid )) {
			return JError::raiseWarning( 500, 'No items selected' );
		}
		$cids = implode( ',', $cid );
		$query = 'UPDATE #__jefs_files'
		. ' SET published = ' . intval( $publish )
		. ' WHERE id IN ( '.$cids.' )'
		;
		$db->setQuery( $query );
		if (!$db->query()) {
			return JError::raiseWarning( 500, $db->getErrorMsg() );
		}

		return 'index.php?option=com_jefs&controller=files';
	}
	  

}//class