<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.model' );

class jefsModelDownloads extends JModel
{
	var $_user = null;

	function getFiles()
	{
		$this->_user	=JFactory::getUser();
		$userid			= (int) $this->_user->get('id', 0);
		
		if($userid > 0) {
			$db =JFactory::getDBO();
			$query = 'SELECT access FROM #__jefs_users WHERE id = '.$db->quote($userid);
			$db->setQuery( $query );
			$useraccess = $db->loadResult();
			
			if($useraccess) {
				$useraccess = unserialize($useraccess);
				if(!empty($useraccess['files'])) $accesslist = implode(',',$useraccess['files']);
				else $accesslist = '0';
			}
			else $accesslist = '0';
			
			$query = ' SELECT * FROM #__jefs_files'.
					 ' WHERE id IN('.$accesslist.')'.
					 ' AND published = '.$db->quote(1).
					 ' OR link_type = '.$db->quote('download').
					 ' AND published = '.$db->quote(1).
					 ' OR link_type = '.$db->quote('register').
					 ' AND published = '.$db->quote(1).
					 ' ORDER BY title ASC';
			$db->setQuery( $query );
			$files = $db->loadObjectList('id');
		}
		else $files = false;
		
		if(!$files) $files = array();
		return $files;
	}// function
	
	function getChildFiles()
	{
		$this->_user	=JFactory::getUser();
		$userid			= (int) $this->_user->get('id', 0);
		
		if($userid > 0) {
			$db =JFactory::getDBO();
			$query = 'SELECT access FROM #__jefs_users WHERE id = '.$db->quote($userid);
			$db->setQuery( $query );
			$useraccess = $db->loadResult();
			
			if($useraccess) {
				$useraccess = unserialize($useraccess);
				if(!empty($useraccess['files'])) $accesslist = implode(',',$useraccess['files']);
				else $accesslist = '0';
				$query = ' SELECT * FROM #__jefs_files'.
						 ' WHERE link_type = '.$db->quote('inherit').
						 ' AND parent IN('.$accesslist.')'.
						 ' AND published = '.$db->quote(1).
						 ' ORDER BY title ASC';
				$db->setQuery( $query );
				$files = $db->loadObjectList('id');
				if(!$files) return false;
			}
			else return false;
		}
		else return false;
		
		$parents = array();
		foreach($files as $file) {
			$parents[$file->parent][] = $file;
		}
		return $parents;
		
	}// function
	
	function &getUser()
	{
		if(empty($this->_user)) $this->_user =JFactory::getUser();
		return $this->_user;
	}
	
}// class
