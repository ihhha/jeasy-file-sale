<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.model' );

class jefsModelDownload extends JModel
{

	function getFile($fileid, $checkinherit = true)
	{
        JEFShelper::logEntry('Looking for File by File ID= '. $fileid, JLog::INFO, 'download model');
		$mainframe = JFactory::getApplication();
		$db =JFactory::getDBO();

		if(is_numeric($fileid)) $field = 'id';
		else $field = 'alias';
		$fileid = str_replace(':','-',$fileid);	// Replace the colon from the request with a dash
		
		$query = 'SELECT * FROM #__jefs_files WHERE '.$db->nameQuote($field).' = '.$db->quote($fileid).' AND published = '.$db->quote(1);
		$db->setQuery( $query );
		$file = $db->loadObject();
		
		if(!$file) {
			$mainframe->enqueueMessage(JText::_( "File item does not exist" ), 'error');
			return false;
		}
		
		if($file->link_type == 'inherit' && $checkinherit) $file = jefshelper::inheritParent($file, $file->parent);
		$file->purchase_button = unserialize($file->purchase_button);
		$file->price_options = unserialize($file->price_options);
		return $file;

	}// function

    function getFileByDownloadHash($downloadHash)
    {
        JEFSHelper::logEntry('Looking for File ID by downloadHash = '. $downloadHash, JLog::INFO, 'download model');
        $mainframe = JFactory::getApplication();
        $db =JFactory::getDBO();
        $db->setQuery("SELECT `fileid` FROM #__jefs_nonregistered WHERE `downloadHash`='$downloadHash' AND `validUntil` > NOW() AND `status`='1' LIMIT 1");
        $fileid = $db->loadResult();
        if(!$fileid) JEFShelper::logEntry('DB Error: '. $db->getErrorMsg(), JLog::INFO, 'download model');
        else return $this->getFile($fileid, false);
    }
}// class
