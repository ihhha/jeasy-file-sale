<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.model' );

class jefsModelFile extends JModel
{
	var $_users = null;
	var $_fileaccess = null;

	function __construct()
	{
		parent::__construct();

		$array = JRequest::getVar('cid',  0, '', 'array');
		$this->setId((int)$array[0]);
	}

	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	function &getData()
	{
		// Load the data
		if (empty( $this->_data )) {
			$query = ' SELECT * FROM #__jefs_files '.
					'  WHERE id = '.$this->_id;
			$this->_db->setQuery( $query );
			$this->_data = $this->_db->loadObject();
		}
		if (!$this->_data) {
			$query = 'SELECT * FROM #__jefs_templates WHERE type = '.$this->_db->quote('downloadpage');
			$this->_db->setQuery( $query );
			$template = $this->_db->loadObject();
			
			$this->_data = new stdClass();
			$this->_data->id = 0;
			$this->_data->title = null;
			$this->_data->alias = null;
			$this->_data->parent = 0;
			$this->_data->filename = null;
			$this->_data->created = time();
			$this->_data->modified= time();
			$this->_data->auto_modified= 1;
			$this->_data->downloads = 0;
			$this->_data->downloads_add = null;
			$this->_data->purchases = 0;
			$this->_data->published = 1;
			$this->_data->path = null;
			$this->_data->purchase_button = null;
			$this->_data->price_options = null;
			$this->_data->link_type = 'purchase';
			$this->_data->description = $template->default;
			$this->_data->return_page = 1;
			$this->_data->allow_download = 1;
			$this->_data->allow_children = 1;
		}
		
		// the following is a fairly slow process - consider speeding up - might need a DB change though :-(
		$query = ' SELECT * FROM #__jefs_users ';
		$this->_db->setQuery( $query );
		$acusers = $this->_db->loadObjectList('id');
		
		$query = ' SELECT * FROM #__users ';
		$this->_db->setQuery( $query );
		$jusers = $this->_db->loadObjectList();
		
		$useraccessarray = array();
		$usernonaccessarray = array();
		// search through all Joomla users
		foreach ($jusers as $juser) {
			// if the user is in the access db then get the access field, otherwise it goes in the non-access array
			if (isset($acusers[$juser->id])) {
				$accessarray = unserialize($acusers[$juser->id]->access);
				// if the file id has been granted access for this user then add to access array, otherwise it goes in the non-access array
				if (in_array($this->_id, $accessarray['files'])) {
					$useraccessarray[$juser->id] = $juser->name;
				}
				else {
					$usernonaccessarray[$juser->id] = $juser->name;
				}
			}
			else {
				$usernonaccessarray[$juser->id] = $juser->name;
			}
		}
		
		$this->_data->useraccess = $useraccessarray;
		$this->_data->usernonaccess = $usernonaccessarray;
		
		
		return $this->_data;
	}

	function store()
	{
	
		jimport( 'joomla.filter.filteroutput' );
		$row =$this->getTable('files');

		$data = JRequest::get( 'post' );
		$data['price_options'] = serialize($data['price_options']);
		
		// Process Purchase Button
		$purchase_button = array();
		$purchase_button['buttontype'] 	= $data['buttontype'];
		$tempname = $data['buttontype'].'img';
		$purchase_button['buttonimage'] = $data[$tempname];
		$purchase_button['buttonurl'] 	= JRoute::_(JURI::root().'images/jefs/buttonimages/'.
													$purchase_button['buttontype'].'/'.
													$purchase_button['buttonimage']);
		$data['purchase_button'] = serialize($purchase_button);
		
		// Process Description
		$description = JRequest::getVar( 'description','','post','string',JREQUEST_ALLOWRAW );
		$data['description'] = $description;
		
		// Process alias
		$cleantitle = JFilterOutput::stringURLSafe($data['title']);
		$data['alias'] = JFilterOutput::stringURLSafe($data['alias']);
		if($data['alias'] == '') $data['alias'] = $cleantitle;
		
		$query = ' SELECT id FROM #__jefs_files '.
				'  WHERE alias = '.$this->_db->quote($data['alias']).
				' AND id != '.$this->_db->quote($data['id']);
		$this->_db->setQuery( $query );
		$aliasexists = $this->_db->loadResult();
		
		if($aliasexists) {
			$mainframe->enqueueMessage(JText::_('Alias already exists - the name has been adjusted to make it unique'), 'error');
			$data['alias'] = $data['id'].'-'.$cleantitle;
		}
		
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if (!$row->check()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if (!$row->store()) {
			$this->setError( $this->_db->getErrorMsg() );
			return false;
		}
		
		// Load the row to get id value
		if (!$row->load()) {
			$this->setError( $this->_db->getErrorMsg() );
			return false;
		}
		
		$itemid = $row->id;
		
		// Update User access if allowed - this is done last because we need the item id if a new item
		if($data['applyaccess'] == '1') {

			$query = ' SELECT * FROM #__jefs_users';
			$this->_db->setQuery( $query );
			$acusers = $this->_db->loadObjectList();
			$accessarray = array();
			foreach ($acusers as $acuser) {
				$accessarray[$acuser->id] = unserialize($acuser->access);
			}
			foreach ($data['users'] as $userid => $useraccess) {
				// if this user has access to the file ...
				if ($useraccess == 'useraccess') {
					// ... and if user has a db entry ...
					if(isset($accessarray[$userid])) {
						// update accessarray for updating the db later by adding file to access list if not in list already
						if(!in_array($itemid,$accessarray[$userid]['files'])) {
							$accessarray[$userid]['files'][] = $itemid;
							sort($accessarray[$userid]['files']);
						}
					}
					// ... else we create a db entry
					else {
						$tmparray = array('files' => array($itemid));
						$tmpobject = new stdClass();
						$tmpobject->id = $userid;
						$tmpobject->access = serialize($tmparray);
						$this->_db->insertObject('#__jefs_users', $tmpobject, 'id');
					}
				}
				// ... else if the user doesn't have access
				elseif ($useraccess == 'usernonaccess') {
					// only proceed if the user has a db entry (if not then no processing is needed)
					if(isset($accessarray[$userid])) {
						// update accessarray for updating the db later by deleting (unset) file to access list if not in list already
						if(in_array($itemid,$accessarray[$userid]['files'])) {
							$key = array_search($itemid, $accessarray[$userid]['files']); // find which key the value is at
							unset($accessarray[$userid]['files'][$key]);
							sort($accessarray[$userid]['files']);
						}
					}
				
				}
			}
			// update db values for all users in db
			foreach ($acusers as $acuser) {
				$tmpobject = new stdClass();
				$tmpobject->id = $acuser->id;
				$tmpobject->access = serialize($accessarray[$acuser->id]);
				$this->_db->updateObject('#__jefs_users', $tmpobject, 'id');
			}
			
		}

		return $itemid;
	}

	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		$row =$this->getTable('files');

		if (count( $cids ))
		{
			foreach($cids as $cid) {
				if (!$row->delete( $cid )) {
					$this->setError( $row->getErrorMsg() );
					return false;
				}
			}
		}
		return true;
	}
	
	function getImages()
	{
		$images = array();
		$imagepath = JPATH_COMPONENT_ADMINISTRATOR.DS.'assets'.DS.'buttonimages';
		$foldertree = JFolder::listFolderTree($imagepath, '.');
		
		foreach($foldertree as $folder) {
			$filelist 	= JFolder::files($folder['fullname']);
			sort($filelist);
			$images[$folder['name']] = $filelist;
		}
		
		return $images;
	}
	
	function getFileList()
	{
		$query = ' SELECT id AS value, title AS text FROM #__jefs_files ORDER BY title ASC';
		$this->_db->setQuery( $query );
		return $this->_db->loadObjectList();
	}
	
	function getItemsFromRequest()
	{
		$cid = JRequest::getVar( 'cid', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		if (count($cid) < 1) {
			$this->setError(JText::_( 'Select an item to copy'));
			return false;
		}

		// Query to list the selected menu items
		$db =$this->getDBO();
		$cids = implode( ',', $cid );
		$query = 'SELECT `id`, `title`' .
				' FROM `#__jefs_files`' .
				' WHERE `id` IN ( '.$cids.' )';

		$db->setQuery( $query );
		$items = $db->loadObjectList();

		return $items;
	}
	
	function copyAccess( $fromid, $tofiles )
	{
		// If only a single 'tofiles' then put in array for processing later
		if(!is_array($tofiles)) $tofiles = array($tofiles);
		
		if(empty($this->_users)) {
			$query = 'SELECT * FROM #__jefs_users';
			$this->_db->setQuery( $query );
			$this->_users = $this->_db->loadObjectList();

			// convert users showing file access to files showing user access
			$files = array();	// holder for holding which users have access to which files
			$users = array();	// holder for user access
			foreach($this->_users as $user) {
				$filearray = unserialize($user->access);
				$users[$user->id] = $filearray['files'];
				foreach($filearray['files'] as $fileid) {
					$files[$fileid][] = $user->id;	// file id is key, then array of user ids	
				}
			}
			$this->_users = $users;
			$this->_fileaccess = $files;
		}
		// check user access and change if necessary
		$newuseraccess = $this->_users;	// copy array to hold new values
		foreach($tofiles as $toid) {
			foreach($this->_users as $key => $value) {	// key is user id, value is array of file ids that user has access to
				$userid = $key;
				if(in_array($userid, $this->_fileaccess[$fromid])) {	// If user has access in 'from file' ...
					if(!in_array($toid, $newuseraccess[$key])) $newuseraccess[$key][] = $toid;	// give it access to the 'to file' if it hasn't already
				}
				elseif(in_array($toid, $newuseraccess[$key])) {
					$keytounset = array_search($toid, $newuseraccess[$key]);
					unset($newuseraccess[$key][$keytounset]);	// else the user doesn't have access so we check if current fileid has access and remove if it does
				}
				else {	// else there is no change so lets unset the user value
					unset($newuseraccess[$key]);
				}
				if(isset($newuseraccess[$key])) sort($newuseraccess[$key]);	// sort the array if it still exists
			}

		}
		// update the db
		foreach($newuseraccess as $key => $value) {
			$tmparray = array('files' => $value);
			$tmpobject = new stdClass();
			$tmpobject->id = $key;
			$tmpobject->access = serialize($tmparray);
			$this->_db->updateObject('#__jefs_users', $tmpobject, 'id');
		}
		
		// error checking???
		
		return true;
	}
	
	function copyItem($id)
	{
		$row =$this->getTable('files');
		$jdate = JFactory::getDate(time());
		$date = $jdate->toMySQL();
		// load the row from the db table
		$row->load( (int) $id );
		$row->title 		= JText::sprintf( 'Copy of', $row->title );
		$row->id 			= 0;
		$row->created 		= $date;
		$row->modified	 	= $date;
		$row->downloads 	= 0;
		$row->purchases 	= 0;
		$row->published 	= 0;
		$row->donations 	= null;
		// check and store new row
		if (!$row->check()) {
			$this->setError( $this->_db->getErrorMsg() );
			return false;
		}
		if (!$row->store()) {
			$this->setError( $this->_db->getErrorMsg() );
			return false;
		}
		
		return true;
	
	}
	
	function saveDefault()
	{
		jimport('joomla.filter.output');
		$description = JRequest::getVar( 'description','','post','string',JREQUEST_ALLOWRAW );
		// $description = JFilterOutput::objectHTMLSafe( $description );	// make it HTML safe
		
		$tmpobject = new stdClass();
		$tmpobject->type = 'downloadpage';
		$tmpobject->html = $description;
		$this->_db->updateObject('#__jefs_templates', $tmpobject, 'type');
		
		return array('msg' => JText::_( 'Saved' ));
	}
	
	function loadDefault()
	{
				
		$query = 'SELECT * FROM #__jefs_templates WHERE type = '.$this->_db->quote('downloadpage');
		$this->_db->setQuery( $query );
		$template = $this->_db->loadObject();
		if(!$template) {
			$default = '';
			$msg = JText::_( 'Error Loading' );
		}
		else {
			$default = $template->html;
			$msg = JText::_( 'Load Complete' );
		}
		
		return array('msg' => $msg, 'html' => $default);
	}

}