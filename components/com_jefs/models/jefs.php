<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.model' );

class jefsModeljefs extends JModel
{
	function purchase($type,$acid,$url,$source = '')
	{
		$mainframe = JFactory::getApplication();
	
		$params =JComponentHelper::getParams('com_jefs');		// get params
		// check if user is logged in, if not they will be sent to login/registration
		$user		=JFactory::getUser();
		$userid		= (int) $user->get('id', 0);
		$loginlink = $params->get('loginlink', 'index.php?option=com_users&view=login');
        $allowNoRegister = $params->def('allowNoRegisterDownload',0);
		
		// Get siteroot used for links
		$uri =JFactory::getURI();
		$uriroot = $uri->root();
		$siteroot = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));
		
		$db =JFactory::getDBO();
		$query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($acid).' AND published = '.$db->quote(1);
		$db->setQuery( $query );
		$file = $db->loadObject();
		
		if(!$file) {
			$mainframe->enqueueMessage(JText::_( "File item does not exist" ), 'error');
			return false;
		}
		
		if($file->link_type == 'inherit') {
			$file = jefshelper::inheritParent($file, $file->parent);
			$type = $file->link_type;
		}

		if(empty($userid) && $file->link_type == 'purchase' && !$allowNoRegister) {
			$mainframe->redirect($uriroot.$loginlink, JText::_( 'Not Logged In - You must be registered and logged in to purchase files' ), 'error');
			return false;
		}
		
		$price_options = unserialize($file->price_options);
		$currency_code = $price_options['default'];
		if($currency_code == 'HUF' || $currency_code == 'JPY') $roundby = 0;	// These currencies require to be round to nearest whole number
		else $roundby = 2;
		$amount = number_format($price_options[$currency_code]['price'], $roundby, '.', '');	// make sure amount is a number
		
		if(empty($userid) && $file->link_type == 'donate' && $amount > 0 && !$allowNoRegister) {
			$mainframe->redirect($uriroot.$loginlink, JText::_( 'Not Logged In - You must be registered and logged in to make a donation to this file' ), 'error');
			return false;
		}
	
		$msg = '';
		$link = '';
	

		$price_options = unserialize($file->price_options);

		
		require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
		$priceHelper = new PriceHelper();
		$currencycode = JRequest::getWord('ACcurrency_'.$source.$file->id, $priceHelper->getCurrencyCode($params, $price_options), 'post');
		
		// Get final price taking into account all options using priceHelper class
		$pricearray = $priceHelper->getPrices($price_options, $currencycode);
		$amount = $pricearray[$currencycode]['amount'];
		// Check donation minimum price and adjust if necessary
		if($file->link_type == 'donate') {
			$donateamount = JRequest::getVar('ACamount_'.$source.$file->id, $amount);
			if($donateamount > $amount || $amount == '') {
				if($currencycode == 'HUF' || $currencycode == 'JPY') $roundby = 0;
				else $roundby = 2;
				$amount = number_format($donateamount, $roundby, '.', '');
			}
		}
		
		//$cancelurl = $siteroot.'index.php?option=com_jefs&task=cancel&requesturl='.urlencode($url);
		$cancelurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=cancel&requesturl='.urlencode($url));
		
		//if($file->return_page == '1') $url = $sitehost.JRoute::_( 'index.php?option=com_jefs&view=download&acid='.$file->id.':'.$file->alias );
		if($file->return_page == '1') $url = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&view=download&acid='. $file->id );
		
		switch($type)
		{
			case 'donate':
				$cmd = '_donations';
				break;
			case 'purchase':
			default:
				$cmd = '_xclick';
				break;
		}

		//$returnurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=success&requesturl='.urlencode($url).'&type='.$type);
        //$notifyurl = rtrim($siteroot,'/').JRoute::_( 'index.php?option=com_jefs&task=validate&payment=jefspaypal' );

		return;
    }
	
	
	
	function validateIPN(){
		$mainframe = JFactory::getApplication();
		
		require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'paypal.class.php' );
		$p = new PayPal;
		$params =JComponentHelper::getParams('com_jefs');		// get params
		$testing = $params->get('sandbox', '0');
		if($testing == '1') $p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';	// if testing use sandbox details
		else $p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';
		
		$sitename = $mainframe->getCfg('sitename');
		$mailfrom = $mainframe->getCfg('mailfrom');
	
		if ($p->validate_ipn()) {

			// get file id that was just purchased
			$return = $p->ipn_data;
			$data = explode('&',$return['custom']);

			$item = explode('=',$data[0]);
			$fileid = $item[1];	// the id
			
			$user = explode('=',$data[1]);
			$userid = $user[1];
			
			$urlparam = explode('=',$data[2]);
			$url = urldecode($urlparam[1]);
			
			$type = explode('=',$data[3]);
			$type = $type[1];
			
			$amount = $return['mc_gross'];
			$currency = $return['mc_currency'];
			if($currency == 'HUF' || $currency == 'JPY') $roundby = 0;
			else $roundby = 2;
			$amount = number_format($amount, $roundby, '.', '');
				
			// Get file details
			$query = 'SELECT * FROM #__jefs_files WHERE id = '.$this->_db->quote($fileid).' AND published = '.$this->_db->quote(1);
			$this->_db->setQuery( $query );
			$file = $this->_db->loadObject();
			
			if(!$file) return false;
			
			if($file->link_type == 'inherit') {
				$file = jefshelper::inheritParent($file, $file->parent);
				$accessid = $file->parent;
			}
			else $accessid = $file->id;
			
			// Make sure that what was returned from PayPal matches up and is correct, otherwise suspect foul play and exit ...
			require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
			$priceHelper = new PriceHelper();
			$price_options = unserialize($file->price_options);
			$pricearray = $priceHelper->getPrices($price_options, $currency);
			unset($priceHelper);
			$fileamount = $pricearray[$currency]['amount'];

			if($type != $file->link_type) return false;
			if($type == 'purchase' && $fileamount != $amount) return false;
			if($type == 'donate' && $fileamount > $amount) return false;
			unset($pricearray);
			
			$query = 'SELECT * FROM #__jefs_users WHERE id = '.$this->_db->quote($userid);
			$this->_db->setQuery( $query );
			$userrow = $this->_db->loadObject();
			
			// If the user doesn't exist in ACFP db then create a new ACFP user if they're not a guest user
			if(!$userrow && $userid > 0) {
				$newaccessarray = array('files' => '');
				$userobject = new stdClass();
				$userobject->id = $userid;
				$userobject->access = serialize($newaccessarray);
				$userobject->donations = '';
				$this->_db->insertObject('#__jefs_users', $userobject, 'id');
				unset($userobject);
				// Run query again
				$query = 'SELECT * FROM #__jefs_users WHERE id = '.$this->_db->quote($userid);
				$this->_db->setQuery( $query );
				$userrow = $this->_db->loadObject();
			}
			
			$access = unserialize($userrow->access);
			// make sure the user doesn't already have access (it would be hard for someone to pay more than once, but lets check anyway)
			// if the file data has been inherited then it's the parent that receives access if it hasn't already
			if(!in_array($accessid, $access['files'])) {
				$access['files'][] = $accessid;
				sort($access['files']);
			}
			
			// Process user donations
			if($userrow->donations == '') $userdonations = array();
			else $userdonations = unserialize($userrow->donations);
			if($type == 'donate') {
				if(isset($userdonations[$currency][$fileid])) $donation = $amount + $userdonations[$currency][$fileid];
				else $donation = $amount;
				$userdonations[$currency][$fileid] = $donation;
			}
			
			// update the user database to grant access and add any donation info
			$userobject = new stdClass();
			$userobject->id = $userid;
			$userobject->access = serialize($access);
			$userobject->donations = serialize($userdonations);
			$this->_db->updateObject('#__jefs_users', $userobject, 'id');
			
			
			// Process file donations
			if($file->donations == '') $filedonations = array();
			else $filedonations = unserialize($file->donations);
			if($type == 'donate') {
				if(isset($filedonations[$currency][$userid])) $donation = $amount + $filedonations[$currency][$userid];
				else $donation = $amount;
				$filedonations[$currency][$userid] = $donation;
			}
			
			// Update file database with purchase amount and donation info
			$fileobject = new stdClass();
			$fileobject->id = $fileid;
			$fileobject->purchases = $file->purchases + 1;
			$fileobject->donations = serialize($filedonations);
			$this->_db->updateObject('#__jefs_files', $fileobject, 'id');
			
			// send user email
				// get user details
			$query = 'SELECT * FROM #__users WHERE id = '.$this->_db->quote($userid);
			$this->_db->setQuery( $query );
			$user = $this->_db->loadObject();
			// Get user email template details
			$query = 'SELECT * FROM #__jefs_templates WHERE type = '.$this->_db->quote('email');
			$this->_db->setQuery( $query );
			$template = $this->_db->loadObject();
			$tploptions = unserialize($template->options);
			
			$sitename = $mainframe->getCfg('sitename');
			$mailfrom = $mainframe->getCfg('mailfrom');
			
			// Dynamic values: [title] = Item Name, [filename] = Filename, [user] = User's Name, [site] = Site Name
			// add some more replace params for the body text - [website] = Site Website, [email] = Site Email Address, [url] = A link back to the purchased item
			$subject = $tploptions['subject'];
			if($subject == '') $subject = '[site]: Download Access for [title]';
			
			$body = $template->html;
			if($body == '') {
				$body = $template->default;
			}
			
			$replacearray = array(	'[title]' => $file->title,
									'[filename]' => $file->filename,
									'[user]' => $user->name,
									'[site]' => $sitename,
									'[website]' => JURI::root(),
									'[email]' => $mailfrom,
									'[url]' => '<a href="'.$url.'" target="_blank">'.$url.'</a>'
									);
									
			foreach($replacearray as $k => $v) {
				$subject = str_replace($k,$v,$subject);
				$body = str_replace($k,$v,$body);
			}
			
			$this->sendMail($mailfrom, $user->email, $subject, $body, true);

			// Send admin email
			// Get admin email template details
			$query = 'SELECT * FROM #__jefs_templates WHERE type = '.$this->_db->quote('adminemail');
			$this->_db->setQuery( $query );
			$template = $this->_db->loadObject();
			$tploptions = unserialize($template->options);
			$subject = $tploptions['subject'];
			if($subject == '') $subject = $return['item_name'].' Ordered via PayPal';
			
			$body = $template->html;
			if($body == '') {
				$body = $template->default;
			}
			
			$alldata = '';
			foreach($return as $k => $v) {
				$alldata .= $k.' = '.$v."<br />";
			}
			
			$replacearray = array(	'[title]' => $file->title,
									'[filename]' => $file->filename,
									'[user]' => $user->name,
									'[username]' => $user->username,
									'[userid]' => $user->id,
									'[useremail]' => $user->email,
									'[paymentemail]' => $return['payer_email'],
									'[transactionid]' => $return['txn_id'],
									'[time]' => date('l jS \of F Y h:i:s A'),
									'[type]' => $type,
									'[amount]' => $currency." ".$amount,
									'[instructions]' => $return['memo'],
									'[alldata]' => $alldata,
									);
									
			foreach($replacearray as $k => $v) {
				$subject = str_replace($k,$v,$subject);
				$body = str_replace($k,$v,$body);
			}
			
			$adminemails = $tploptions['emailto'];
			if(empty($adminemails)) {
				$query = 'SELECT * FROM #__users WHERE sendEmail = '.$this->_db->Quote(1).' AND block != '.$this->_db->quote(1);
				$this->_db->setQuery( $query );
				$adminemails = $this->_db->loadObjectList();
			}
			else {
				$admins = implode(',',$adminemails);
				$query = 'SELECT * FROM #__users WHERE id IN('.$admins.') AND block != '.$this->_db->quote(1);
				$this->_db->setQuery( $query );
				$adminemails = $this->_db->loadObjectList();
			}
			foreach($adminemails as $adminemail) {
				$this->sendMail($mailfrom, $adminemail->email, $subject, $body, true);
			}
			$otheradminemails = $tploptions['otheremailto'];
			if($otheradminemails != '') {
				$otheradminemails = explode("\n",$otheradminemails);
				foreach($otheradminemails as $otheradminemail) {			
					$this->sendMail($mailfrom, $otheradminemail, $subject, $body, true);
				}
			}

		}
	
	}
	
	function sendMail($mailfrom, $to, $subject, $body, $html) {
			$mainframe = JFactory::getApplication();
					
			$mailtype = $mainframe->getCfg('mailer');	// either mail, sendmail, or smtp
			
			if($mailtype == 'mail') {
				if($html) {
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.$mailfrom.' ' . "\r\n";
				}
				else {
					$headers = 'From: '.$mailfrom.' ' . "\r\n";
				}
				mail($to, $subject, $body, $headers);
			}
			else {
			
			$mail = new JMail(); 
			$mail->setsender($mailfrom);
			$mail->addRecipient($to);
			$mail->setSubject($subject);
			$mail->setbody($body);
			if($mailtype == 'sendmail') $mail->useSendmail();
			else if($mailtype == 'smtp') $mail->useSMTP();
			
			$mail->send();
			
			}
	}
	
	function addDownload($fileid)
	{
			$query = 'SELECT downloads FROM #__jefs_files WHERE id = '.$this->_db->quote($fileid);
			$this->_db->setQuery( $query );
			$downloads = $this->_db->loadResult();
	
			$object = new stdClass();
			$object->id = $fileid;
			$object->downloads = $downloads + 1;

			$this->_db->updateObject('#__jefs_files', $object, 'id');
	
	
	}
	
	function addPurchase($fileid)
	{
			// not used anymore
			$query = 'SELECT purchases FROM #__jefs_files WHERE id = '.$this->_db->quote($fileid);
			$this->_db->setQuery( $query );
			$purchases = $this->_db->loadResult();
	
			$object = new stdClass();
			$object->id = $fileid;
			$object->purchases = $purchases + 1;

			$this->_db->updateObject('#__jefs_files', $object, 'id');
	
	
	}
    function createDownloadLink($fileid)
    {
        $params =JComponentHelper::getParams('com_jefs');
        $validInterval = $params->def('keepDownloadLinkTime') * 3600;
        $input = new JInput();
        $email = $this->_db->escape($input->get('email','','string'));
        $created = date('Y-m-d H:i:s',time());
        $validUntil = date('Y-m-d H:i:s',time() + $validInterval);
        $hash = md5($fileid . $email . rand());
        $q = "INSERT INTO #__jefs_nonregistered (`email`,`fileid`, `hash`, `created`, `validUntil`) VALUES ('$email','$fileid','$hash','$created','$validUntil')";
        $this->_db->setQuery($q)->query();
        return $hash;
    }
}// class
