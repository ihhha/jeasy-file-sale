<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');
jimport('joomla.filesystem.folder');
jimport('joomla.filter.filteroutput');

class jefsViewFtp extends JView
{
	function display()
	{
		$mainframe = JFactory::getApplication();
	
		$db			= JFactory::getDBO();
		$document	= JFactory::getDocument();
		$document->setTitle('AC FTP Browser');
		
		$path = JRequest::getVar( 'path', '' );
		$filename = JRequest::getVar( 'filename', '' );
        if($path == '') $path = JPATH_ROOT;
		$path = urldecode($path);
        //$path_js = str_replace('\\','\\\\',$path);
        $path_js = addslashes($path);
		$filename = urldecode($filename);
		$submitfolder = JRequest::getVar( 'submitfolder', false );
		if($submitfolder) {
			$newfolder = JRequest::getVar( 'newfolder', '' );
			if($newfolder != '') {
				$newfolder = JFilterOutput::stringURLSafe($newfolder);
				$path = $path.DS.$newfolder;
				$filename = '';	// filename not used if creating new folder
				$createfolder = JFolder::create($path);
			}
		}
		
		$submitfile = JRequest::getVar( 'submitfile', false );
		if($submitfile) {
				$filename = $this->fileUpload();
		}

		JHTML::_('behavior.modal');

		$template = $mainframe->getTemplate();
		$document->addStyleSheet("templates/$template/css/general.css");
		
		$breadcrumbs = explode(DS,$path);
		$folders = JFolder::folders($path,null,null,true);
		$files = JFolder::files($path,null,null,true);
		
		$currfolder = array_pop($breadcrumbs);
		$uplink = 'index.php?option=com_jefs&amp;view=ftp&amp;tmpl=component&amp;path='.urlencode(implode(DS,$breadcrumbs));
		$breadcrumbs[] = $currfolder;	// put last folder back in breadcrumbs array
		
		$filename == '' ? $filesize = '' : $filesize = jefshelper::parseSize(filesize($path.DS.$filename));

		// File Upload
		$config =JComponentHelper::getParams('com_media');
		
		?>
		<form action="index.php?option=com_jefs&amp;view=ftp&amp;tmpl=component" method="post" name="adminForm">

			<table width="100%">
				<tr>
					<td nowrap="nowrap">
					<strong>
						<?php echo JText::_( 'Folder Path' ); ?>: 
					</strong>
					</td>
					<td nowrap="nowrap">
						
						<input class="text_area" type="text" name="pathdisplay" id="pathdisplay" size="70" maxlength="250" value="<?php echo $path;?>" readonly="readonly" /> 
						<input type="hidden" name="path" value="<?php echo urlencode($path);?>" />
						 / 
						<input class="text_area" type="text" name="newfolder" id="newfolder" size="20" maxlength="250" value="" />
						<input type="submit" name="submitfolder" value="<?php echo JText::_( 'Create Folder' ); ?>" />
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">
					<strong>
						<?php echo JText::_( 'File Name' ); ?>: 
					</strong>
					</td>
					<td nowrap="nowrap">
						<input class="text_area" type="text" name="filename" id="filename" size="70" maxlength="250" value="<?php echo $filename;?>" readonly="readonly" /> 
						<button onclick="window.parent.jSelectFile('<?php echo $filename; ?>', '<?php echo $path_js; ?>', '<?php echo $filesize; ?>');window.close();"><?php echo JText::_( 'OK' ); ?></button>
						<button onclick="window.parent.SqueezeBox.close();"><?php echo JText::_( 'Cancel' ); ?></button>
                    </form>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">
					<strong>
						<?php echo JText::_( 'Upload File' ); ?>:
					</strong>
					</td>
					<td nowrap="nowrap">
						<form action="<?php echo JURI::base(); ?>index.php?option=com_jefs&amp;view=ftp&amp;tmpl=component&amp;path=<?php echo urlencode($path); ?>&amp;<?php echo JUtility::getToken();?>=1" id="uploadForm" method="post" enctype="multipart/form-data">
							<input type="file" id="file-upload" name="Filedata" size="67" />
							<input type="submit" name="submitfile" id="file-upload-submit" value="<?php echo JText::_('Start Upload'); ?>"/> 
							[ <?php echo JText::_( 'Max' ); ?>&nbsp;<?php echo ($config->get('upload_maxsize') / 1000000); ?>M ]
							<input type="hidden" name="folder" value="<?php echo urlencode($path); ?>" />
							<input type="hidden" name="return-url" value="<?php echo base64_encode('index.php?option=com_jefs&view=ftp&tmpl=component'); ?>" />
						</form>
					</td>
				</tr>
			</table>
			<hr />
			<table width="100%">
				<tr>
					<td width="80">&nbsp;</td>
					<td align="left" valign="top">
						<strong>
						<?php 
							$breadarray = array();
							foreach($breadcrumbs as $breadcrumb) {
								$breadarray[] = $breadcrumb;
								echo '/'.'<a href="index.php?option=com_jefs&amp;view=ftp&amp;tmpl=component&amp;path='.urlencode(implode(DS,$breadarray)).'">'
										. $breadcrumb.'</a>';
							}
						?>
						</strong>
					</td>
					<td nowrap="nowrap" align="right" valign="top">
						<strong>
						<?php echo '<a href="index.php?option=com_jefs&amp;view=ftp&amp;tmpl=component&amp;path='.urlencode(JPATH_ROOT)
									.'"><<< '.JText::_( 'Back to Site Root' ).'</a>';
						?>
						</strong>
					</td>
					<td width="80">&nbsp;</td>
				</tr>
			</table>
			
			
			<div class="manager">
			<table width="100%" cellspacing="0">
			<thead>
				<tr>
					<th width="80">&nbsp;</th>
					<th width="20">&nbsp;</th>
					<th width="150" align="left"><?php echo JText::_( 'Filename' ); ?></th>
					<th width="50"><?php echo JText::_( 'Size' ); ?></th>
					<th>&nbsp;</th>
					<th width="80">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td width="80">&nbsp;</td>
					<td class="imgTotal">
						<a href="<?php echo $uplink; ?>">
							<img src="../media/media/images/folderup_16.png" width="16" height="16" border="0" alt="<?php echo JText::_( 'Up One Level' ); ?>" /></a>
					</td>
					<td class="description">
						<a href="<?php echo $uplink; ?>">-- [<?php echo JText::_( 'Up One Level' ); ?>] --</a>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td width="80">&nbsp;</td>
				</tr>

		
				<?php foreach ($folders as $folder) : 
						$folderarray = explode(DS,$folder);
						$folderlink = 'index.php?option=com_jefs&amp;view=ftp&amp;tmpl=component&amp;path='.urlencode($folder);
						$foldername = end($folderarray);
				?>
				<tr>
					<td width="80">&nbsp;</td>
					<td class="imgTotal">
						<a href="<?php echo $folderlink; ?>">
						<img src="../media/media/images/folder_sm.png" width="16" height="16" border="0" alt="<?php echo $foldername; ?>" />
						</a>
					</td>
					<td class="description">
						<a href="<?php echo $folderlink; ?>"><?php echo $foldername; ?></a>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td width="80">&nbsp;</td>
				</tr>
				<?php endforeach; ?>
		
				<?php foreach ($files as $file) : 
						$filearray = explode(DS,$file);
						$filename = array_pop($filearray);
						$filefolder = implode(DS,$filearray);
						$filelink = 'index.php?option=com_jefs&amp;view=ftp&amp;tmpl=component&amp;path='.urlencode($filefolder).'&amp;filename='.urlencode($filename);
				?>
				<tr>
					<td width="80">&nbsp;</td>
					<td>&nbsp;</td>
					<td class="description">
									<strong><a href="<?php echo $filelink; ?>"><?php echo $filename; ?></a></strong>
					</td>
					<td><?php echo jefshelper::parseSize(filesize($file)); ?></td>
					<td>&nbsp;</td>
					<td width="80">&nbsp;</td>
				</tr>
				<?php endforeach; ?>
				<tr>
					<td width="80">&nbsp;</td>
					<td colspan="4"><hr /></td>
					<td width="80">&nbsp;</td>
				</tr>
			</tbody>
			</table>
			</div>

		
		<?php
	}
	
	function fileUpload()
	{
		$mainframe = JFactory::getApplication();

		// Check for request forgeries
		JRequest::checkToken( 'request' ) or jexit( 'Invalid Token' );

		$file 		= JRequest::getVar( 'Filedata', '', 'files', 'array' );
		$folder		= JRequest::getVar( 'folder', '' );
		$format		= JRequest::getVar( 'format', 'html', '', 'cmd');
		$return		= JRequest::getVar( 'return-url', null, 'post', 'base64' );
		$err		= null;
		
		$folder = urldecode($folder);
		$return = false;


		// Set FTP credentials, if given
		jimport('joomla.client.helper');
		JClientHelper::setCredentialsFromRequest('ftp');

		// Make the filename safe
		jimport('joomla.filesystem.file');
		$file['name']	= JFile::makeSafe($file['name']);

		if (isset($file['name'])) {
			$filepath = JPath::clean($folder.DS.strtolower($file['name']));
			
			if (JFile::exists($filepath)) {
				if ($format == 'json') {
					jimport('joomla.error.log');
					$log = &JLog::getInstance('upload.error.php');
					$log->addEntry(array('comment' => 'File already exists: '.$filepath));
					header('HTTP/1.0 409 Conflict');
					jexit('Error. File already exists');
				} else {
					JError::raiseNotice(100, JText::_('Error. File already exists'));
					// REDIRECT
					if ($return) {
						$mainframe->redirect(base64_decode($return).'&path='.$folder);
					}
					return '';
				}
			}

			if (!JFile::upload($file['tmp_name'], $filepath)) {
				if ($format == 'json') {
					jimport('joomla.error.log');
					$log = &JLog::getInstance('upload.error.php');
					$log->addEntry(array('comment' => 'Cannot upload: '.$filepath));
					header('HTTP/1.0 400 Bad Request');
					jexit('Error. Unable to upload file');
				} else {
					JError::raiseWarning(100, JText::_('Error. Unable to upload file'));
					// REDIRECT
					if ($return) {
						$mainframe->redirect(base64_decode($return).'&path='.$folder);
					}
					return '';
				}
			} else {
				if ($format == 'json') {
					jimport('joomla.error.log');
					$log = &JLog::getInstance();
					$log->addEntry(array('comment' => $folder));
					jexit('Upload complete');
				} else {
					$mainframe->enqueueMessage(JText::_('Upload complete'));
					// REDIRECT
					if ($return) {
						$mainframe->redirect(base64_decode($return).'&path='.$folder);
					}
					return JPath::clean(strtolower($file['name']));
				}
			}
			
		} else {
			$mainframe->redirect('index.php', 'Invalid Request', 'error');
		}
	}
	

}
