<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');
jimport( 'joomla.filter.output' );


if(!$this->files) $title = JText::_( 'Sorry, you do not have access to any downloads' );
else $title = JText::_( 'You have access to the following downloads' );
$uri =JFactory::getURI();
$url = $uri->toString();
$Itemid = JRequest::getInt('Itemid','');
?>
<h1 class="contentheading"><?php echo JText::_( 'Hello' ).' '.$this->user->name; ?>!</h1><h2><?php echo $title; ?>:</h2>
	<ul>
	<?php foreach( $this->files as $file ) :
	// $link = JRoute::_( 'index.php?option=com_jefs&view=download&acid='. $file->id.':'.JFilterOutput::stringURLSafe($file->title) );
	$link = JRoute::_( 'index.php?option=com_jefs&view=download&acid='. $file->id );
	?>
	
	<li><a href="<?php echo $link; ?>" class="category"><?php echo $file->title; ?></a>&nbsp;
			<span class="small"> ( <?php echo $file->filename; ?> ) </span>
			<?php 	if(!empty($file->allow_children) && isset($this->childfiles[$file->id])) : ?>
					<ul>
				<?php 	foreach($this->childfiles[$file->id] as $childfile) : 
						//$link = JRoute::_( 'index.php?option=com_jefs&view=download&acid='. $childfile->id.':'.JFilterOutput::stringURLSafe($childfile->title) );
						$link = JRoute::_( 'index.php?option=com_jefs&view=download&acid='. $childfile->id );
				?>
							<li><a href="<?php echo $link; ?>" class="category"><?php echo $childfile->title; ?></a>&nbsp;
							<span class="small"> ( <?php echo $childfile->filename; ?> ) </span>
							</li>
				<?php 	endforeach; ?>
					</ul>
			<?php 	endif; ?>
	</li>
	
	<?php endforeach; ?>
	</ul>