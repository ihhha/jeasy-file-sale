<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.view');

class jefsViewDownloads extends JView
{

	function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();
		
		$files = $this->get( 'Files' );
		$childfiles = $this->get( 'ChildFiles' );
		$user =$this->get( 'User' );
		$userid		= (int) $user->get('id', 0);
		
		if($userid <= 0) {
			$params =JComponentHelper::getParams('com_jefs');
			$loginlink = $params->get('loginlink', 'index.php?option=com_users&view=login');
			$mainframe->redirect(JRoute::_($loginlink), JText::_( 'You do not have access to any downloads - Please make sure you are logged in.' ), 'error');
			return false;
		}
		
		$this->assignRef( 'files',	$files );
		$this->assignRef( 'user',	$user );
		$this->assignRef( 'childfiles',	$childfiles );

		parent::display($tpl);
	}
}// class
