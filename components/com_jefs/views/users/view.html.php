<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.view');

class jefsViewUsers extends JView
{

	function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();$option = JRequest::getCmd('option');
		$controller = JRequest::getWord( 'controller', 'config' );
	
		JToolBarHelper::title(   JText::_( 'JEasy File Sale - User Manager' ), 'generic.png' );
		JToolBarHelper::editListX();
		
		// Get data from the model
	 	$items =$this->get('Data');
		$acusers =$this->get('ACUsers');
		$files =$this->get('Files');
	 	$pagination =$this->get('Pagination');
		
		// prepare list array
		$lists = array();
		// get the user state of the order and direction
		$filter_order = $mainframe->getUserStateFromRequest($option.$controller.'filter_order', 'filter_order', 'name');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($option.$controller.'filter_order_Dir', 'filter_order_Dir', 'ASC');
		// set the table order values
		$lists['order_Dir'] = $filter_order_Dir;
		$lists['order'] = $filter_order;

		// get the user-state of the published filter
		$filter_files = $mainframe->getUserStateFromRequest($option.$controller.'filter_files', 'filter_files');
		$filter_search = $mainframe->getUserStateFromRequest($option.$controller.'filter_search', 'filter_search');
		
		$js = "onchange=\"if (this.options[selectedIndex].value!='')
       { document.adminForm.submit(); }\"";
		
		// prepare database
		$db =JFactory::getDBO();
		$query = ' SELECT id AS value, title AS text' .
				 ' FROM #__jefs_files' .
				 ' WHERE title != '.$db->quote('') .
				 ' ORDER BY title ASC';
		$db->setQuery($query);
		// add first 'select' option
		$options = array();
		$options[] = JHTML::_('select.option', '0', '- '.JText::_('Select File Access').' -');
		// append database results
		$options = array_merge($options, $db->loadObjectList());
		// build form control
		$lists['files'] = JHTML::_('select.genericlist', $options, 'filter_files', 'class="inputbox" size="1" '.$js, 'value', 'text', $filter_files);
		$lists['search'] = $filter_search;

		// Get file access names
		if($items) {
			foreach($items as $item) {
				$accessarray = array();
				if(isset($acusers[$item->id])) {
					$access = unserialize($acusers[$item->id]['access']);
					$file_access = $access['files'];
					if(!empty($file_access)) {
						foreach($file_access as $key => $value){
							$accessarray[] = $files[$value]['title'];
						}
					}
				}
				$item->file_access = implode(',',$accessarray);
			}
		}
		
		$this->assignRef('lists', $lists);
		$this->assignRef('items', $items);
		$this->assignRef('acusers', $acusers);
		$this->assignRef('pagination', $pagination);

		parent::display($tpl);
	}//function

}//class