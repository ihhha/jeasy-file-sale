<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.view');

class jefsViewDownload extends JView
{

	function display($tpl = null)
	{
		$task = JRequest::getWord( 'task' );
		$fileid = JRequest::getVar( 'acid', '0' );
		
		// Get active menu params if available and change fileid
		$menu = &JSite::getMenu();
		$active = $menu->getActive();
		$headerText	= false;
		$footerText = false;
		if (is_object( $active )) {
            //$fileid = $active->params->get( 'fileid', $fileid );
            // @todo get Fileid using correct way

            $fileid = JRequest::getVar( 'fileid', $fileid );
            $headerText	= trim( $active->params->get( 'header_text', false ) );
			$footerText	= trim( $active->params->get( 'footer_text', false ) );
		}

		$this->assignRef( 'headerText',	$headerText );
		$this->assignRef( 'footerText',	$footerText );
			
		if($task == 'purchase') {
            $this->setLayout('redirect');
		}
		else {
			$model =$this->getModel( 'Download' );
			$file = $model->getFile( $fileid );
			
			$uri = JFactory::getURI();
			$url = $uri->toString();
			
			if($file) {		
				// Change links to correct download links
				$regex = '/\[filelink(.*?)\]/i';
				preg_match_all( $regex, $file->description, $matches );
				if(isset($matches[1])) {
					foreach($matches[1] as $key => $acid) {
						if($acid == '') {
							$acid = $file->id;
							$alias = $file->alias;
						}
						else {
							$alias = $this->getFileAlias($acid);
						}

						$link = JRoute::_( 'index.php?option=com_jefs&view=download&acid='.$acid.':'.$alias.'&task=download&url='.urlencode($url) );
						$file->description = str_replace($matches[0][$key],$link,$file->description);
					}
				}
                // replace anything else that might need replacing
				$filesize = jefshelper::parseSize(filesize($file->path.DS.$file->filename));
				$file->description = str_replace('[title]',$file->title,$file->description);
				$file->description = str_replace('[filename]',$file->filename,$file->description);
				$file->description = str_replace('[filesize]',$filesize,$file->description);
				$file->description = str_replace('[created]',$file->created,$file->description);
				$file->description = str_replace('[modified]',$file->modified,$file->description);
				$downloads = $file->downloads + $file->downloads_add;
				$file->description = str_replace('[downloads]',$downloads,$file->description);
                $registerButton = '';
                if (file_exists(JPATH_COMPONENT_ADMINISTRATOR . DS . 'classes' . DS . 'buttonhelper.class.php')){
                    require_once (JPATH_COMPONENT_ADMINISTRATOR . DS . 'classes' . DS . 'buttonhelper.class.php');
                    $buttonHelper = new ButtonHelper();
                    $registerButton = $buttonHelper->renderRegisterButton('register',$fileid);
                };
                $file->description = str_replace('[registerButton]',$registerButton,$file->description);
            }
			
			$this->assignRef( 'file',	$file );
		}

		parent::display($tpl);
	}
	
	function getFileAlias($id)
	{
		$db =JFactory::getDBO();
		$query = ' SELECT alias FROM #__jefs_files WHERE id = '.$db->quote($id);
		$db->setQuery( $query );
		return $db->loadResult();
	
	}
	
	function purchaseRedirect($tpl = null)
	{
        jefshelper::logEntry('Download view: PurchaseRedirect started',JLog::INFO,'view');
        $mainframe = JFactory::getApplication();
        $input = new JInput();

        $type = JRequest::getWord('type', '');
        $acid = JRequest::getInt('acid', '0');
        $url = urldecode(JRequest::getVar('url', ''));
        $source = JRequest::getWord('source', '');

        $params =JComponentHelper::getParams('com_jefs');		// get params
        // check if user is logged in, if not they will be sent to login/registration
        $user		=JFactory::getUser();
        $userid		= (int) $user->get('id', 0);
        $loginlink = $params->def('loginlink', 'index.php?option=com_users&view=login');
        $allowNoRegister = $params->def('allowNoRegisterDownload',0);


        // Get siteroot used for links
        $uri =JFactory::getURI();
        $uriroot = $uri->root();
        $siteroot = $uri->toString(array('scheme', 'user', 'pass', 'host', 'port'));

        $db =JFactory::getDBO();
        $query = 'SELECT * FROM #__jefs_files WHERE id = '.$db->quote($acid).' AND published = '.$db->quote(1);
        $db->setQuery( $query );
        $file = $db->loadObject();

        if(!$file){
            $mainframe->enqueueMessage(JText::_( "JEFS_FILE_DOESNT_EXIST" ), 'error');
            return false;
        }

        if($file->link_type == 'inherit') {
            $file = jefsHelper::inheritParent($file, $file->parent);
            $type = $file->link_type;
        }
        $nonRegHash = '';
        if(!$userid && $allowNoRegister)
        {
            $model = $this->getModel('jefs');
            $nonRegHash = $model->createDownloadLink($file->id);
        }

        if(empty($userid) && $file->link_type == 'purchase' && !$allowNoRegister) {
            $mainframe->redirect($uriroot.$loginlink, JText::_( 'JEFS_NOT_LOGIN_TO_PURCHASE' ), 'error');
            return false;
        }

        $price_options = unserialize($file->price_options);
        $currency_code = $price_options['default'];
        if($currency_code == 'HUF' || $currency_code == 'JPY') $roundby = 0;	// These currencies require to be round to nearest whole number
        else $roundby = 2;
        $amount = number_format($price_options[$currency_code]['price'], $roundby, '.', '');	// make sure amount is a number

        if(empty($userid) && $file->link_type == 'donate' && $amount > 0 && !$allowNoRegister) {
            $mainframe->redirect($uriroot.$loginlink, JText::_( 'Not Logged In - You must be registered and logged in to make a donation to this file' ), 'error');
            return false;
        }

        $data = array(
            'type'      =>  $type,
            'acid'      =>  $acid,
            'url'       =>  $url,
            'source'    =>  $source,
        );
        if(!empty($nonRegHash))
        {
            $data['nonRegHash'] =   $nonRegHash;
            $data['url']        =   JURI::base();
        }
        $jinput = JFactory::getApplication()->input;
        $rnd = $jinput->getInt('rnd', '');
		$paymentplugin = $jinput->getString('paymentPlugin_' . $rnd, '');
        jefshelper::logEntry('Paymetn plugin is ' . $paymentplugin, JLog::INFO,'Download view');
        if($paymentplugin !='')
        {
            JPluginHelper::importPlugin( 'jefs' );
            $className = 'plgjefs'.$paymentplugin;
            if (class_exists($className) && method_exists($className, 'onPurchase'))
            {
                $dispatcher  =JDispatcher::getInstance();
                $plugin      = new $className($dispatcher, array());
                //$args        = array('plugin' => $paymentplugin, 'data' => $data);
                $purchase = call_user_func_array(array($plugin, 'onPurchase'), $data);
            }
        }

		$this->assignRef( 'purchase',	$purchase );

		// Get redirect template
		$db =JFactory::getDBO();
		$query = ' SELECT * FROM #__jefs_templates WHERE type = '.$db->quote('redirect');
		$db->setQuery( $query );
		$template = $db->loadObject();
		$template->options = unserialize($template->options);
		$this->assignRef( 'template',	$template );
		$this->assignRef( 'allowNoRegister',	$allowNoRegister);

		$this->setLayout('redirect');
		parent::display($tpl);
	}
}// class
