<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');
if($this->purchase) {
	JHTML::_('behavior.mootools');
	$document =JFactory::getDocument();
	$js = '
	  window.addEvent(\'domready\', function() {
		document.forms[\'jefspayment\'].submit();
	  });
	';
	$document->addScriptDeclaration($js);
	$document->setTitle('Processing Request ...');

	$button = '<input type="submit" value="'.$this->template->options['button_text'].'" />';
	$text = str_replace('[button]', $button, $this->template->html);

    // purchase['html']  for future use when plugin have to pass not only form data
    if(empty($this->purchase['html']))
    {
        echo "<form method=\"post\" name=\"jefspayment\" action=\"".$this->purchase['paymenturl']."\">\n";
        foreach ($this->purchase['vars'] as $name => $value){
         echo "<input type=\"hidden\" name=\"$name\" value=\"$value\"/>\n";
        }
        echo $text;
        echo "</form>\n";
    }
    else {
        echo $this->purchase['html'];
        echo $text;
        echo "</form>\n";
    }
}

?>