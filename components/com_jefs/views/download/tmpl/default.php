<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');
if($this->headerText) 
{
	echo $this->headerText;
}
echo '<br />';
if($this->file) 
{
	echo $this->file->description;
}
echo '<br />';
if($this->footerText) 
{
	echo $this->footerText;
}
?>
