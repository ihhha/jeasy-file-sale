<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.view');

class jefsViewFile extends JView
{

	function display($tpl = null)
	{
		$file		=$this->get('Data');
		$isNew		= ($file->id < 1);

		$text = $isNew ? JText::_( 'New File Item' ) : JText::_( 'Edit' ).' '.$file->title;
		JToolBarHelper::title(  JText::_( 'JEasy File Sale - File Manager').': <small><small>[ ' . $text.' ]</small></small>' );
		JToolBarHelper::save();
        JToolBarHelper::save2new();
		JToolBarHelper::apply();
		if ($isNew)  {
			JToolBarHelper::cancel();
		} else {
			// for existing items the button is renamed `close`
			JToolBarHelper::cancel( 'cancel', JText::_('Close') );
		}
		
		// prepare child files list
		$db =JFactory::getDBO();
		if($file->id > 0 ) {
			$query = ' SELECT id AS value, title AS text' .
					 ' FROM #__jefs_files' .
					 ' WHERE id != '.$db->quote($file->id).
					 ' AND parent = '.$db->quote($file->id).
					 ' OR link_type = '.$db->quote('download').
					 ' ORDER BY title ASC ';
			$db->setQuery($query);
			$rows = $db->loadObjectList();
		}
		else $rows = array();

		if(!$rows) $rows = array();
		// add first 'select' option
		$options = array();
		$options[] = JHTML::_('select.option', '', '-- ['.JText::_('This File').'] --');
		// append database results
		$options = array_merge($options, $rows);
		// build form control
		$isNew ? $defaultfile = '' : $defaultfile = $file->id;
		$lists['files'] = JHTML::_('select.genericlist', $options, 'files', 'class="inputbox" size="5" style="width:160px"', 'value', 'text', $defaultfile);
		if(empty($rows)) $lists['files'] .= '<br /><strong>&nbsp;&nbsp;&nbsp;&nbsp;** '.JText::_('No Child Files Found').' **</strong>';
		// Build parent list
		$query = ' SELECT id AS value, title AS text' .
				 ' FROM #__jefs_files' .
				 ' WHERE id != '.$db->quote($file->id).
				 ' AND link_type != '.$db->quote('inherit').
				 ' ORDER BY title ASC ';
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		if(!$rows) $rows = array();
		$options = array();
		$options[] = JHTML::_('select.option', '0', '-- '.JText::_('No Parent').' --');
		$options = array_merge($options, $rows);
		$lists['parent'] = JHTML::_('select.genericlist', $options, 'parent', 'class="inputbox" size="6" style="width:160px"', 'value', 'text', $file->parent);
		
		$editor =JFactory::getEditor();
		
		$file->price_options = unserialize($file->price_options);
		
		$images = $this->get('Images');
		
		$accesslist = $this->getUserAccessHtml($file->useraccess, 'useraccess');
		$nonaccesslist = $this->getUserAccessHtml($file->usernonaccess, 'usernonaccess');
		$purchasebutton = $this->getButtonTypeHtml($images,unserialize($file->purchase_button));
		
		$params = JComponentHelper::getParams( 'com_jefs' );
		$defaultcurrency = $params->get( 'defaultcurrency' );
		
		// Get currency and symbol list from pricehelper class
		require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'pricehelper.class.php' );
		$priceHelper = new PriceHelper();
		$currency_array = $priceHelper->currency_array;
		$symbol_array = $priceHelper->symbol_array;

		// Get File Browse button
		$doc 		=JFactory::getDocument();
		$js = "
		
		function jSelectFile(filename,path,size) {
		  if(filename == '') path = '';	// If no filename then remove path
		  document.getElementById('filename').value = filename;
		  document.getElementById('filedisplay').value = filename;
		  document.getElementById('path').value = path;
		  document.getElementById('filesize').innerHTML = size;
		  window.parent.SqueezeBox.close();
		}";
		$doc->addScriptDeclaration($js);

		$link = 'index.php?option=com_jefs&amp;view=ftp&amp;tmpl=component&amp;path='.$file->path.'&amp;filename='.$file->filename;

		JHTML::_('behavior.modal', 'a.modal');
		$filehtml = "\n".'<div style="float: left;"><input class="text_area" type="text" size="40" name="filedisplay" id="filedisplay" value="'.$file->filename.'" readonly="readonly" /></div>';
		$filehtml .= '<div class="button2-left"><div class="blank"><a class="modal" title="'.JText::_('Browse Files').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 700, y: 500}}" >'.JText::_('Browse Files').'</a></div></div>'."\n";
		$filehtml .= "\n".'<input type="hidden" id="filename" name="filename" value="'.$file->filename.'" />';
		
		$this->assignRef('file', $file);
		$this->assignRef('accesslist', $accesslist);
		$this->assignRef('nonaccesslist', $nonaccesslist);
		$this->assignRef('purchasebutton', $purchasebutton);
		$this->assignRef('defaultcurrency', $defaultcurrency);
		$this->assignRef('editor', $editor);
		$this->assignRef('lists', $lists);
		$this->assignRef('filehtml', $filehtml);
		$this->assignRef('currency_array', $currency_array);
		$this->assignRef('symbol_array', $symbol_array);


		parent::display($tpl);
    }// function
	
	function getUserAccessHtml($array,$name)
	{
		if($name == 'useraccess') {
			// Only add the javascript function once
			$document =JFactory::getDocument();
			$js = '
			function swapSelectOptions(selectFrom,selectTo,swapAll) {
				if (typeof(selectFrom) == "string") {
					availableitems = document.getElementById(selectFrom);
				}
				if (typeof(selectTo) == "string") {
					selecteditems= document.getElementById(selectTo);
				}
				for (var i = 0; i < availableitems.length; i++) {
					if (availableitems.options[i].selected || swapAll) {
						selecteditems.options[selecteditems.options.length] = new Option(availableitems.options[i].text);
						selecteditems.options[selecteditems.options.length-1].value = availableitems.options[i].value;
						availableitems.options[i].selected = false;
						availableitems.options[i] = null;
						
						userid = selecteditems.options[selecteditems.options.length-1].value;
						hiddenfield = document.getElementById(\'users_\' + userid);
						hiddenfield.value = selectTo;
						i--;
					}
				}
			}';
			
			$document->addScriptDeclaration($js);
		}
	
		$html = '<select name="'.$name.'[]" id="'.$name.'" multiple="multiple" size="20" style="width:200px" >';
		
		foreach ($array as $k => $v) {
			$html .= '<option value="'.$k.'">'.$v.' (ID: '.$k.')</option>';
		
		}
		
		$html .= '</select>';
		
		// write hidden inputs for each user to hold the data for processing
		foreach ($array as $k => $v) {
			$html .= '<input type="hidden" name="users['.$k.']" id="users_'.$k.'" value="'.$name.'" />';
		
		}
		
		return $html;
	
	}
	
	function getButtonTypeHtml($images,$options)
	{
		$buttonTypes = array('buynow' => 'Buy Now','paynow' => 'Pay Now','donate' => 'Donate');
		$selected = array();
		$display = array();
		$uri = JFactory::getURI();
		$urlbase = $uri->base();
		$imagefolder = $urlbase.'components/com_jefs/assets/buttonimages/';
		// Set type from settings or use buynow as default if not set
		$type = isset($options['buttontype']) ? $options['buttontype'] : 'buynow';
		// set initial values
		foreach($buttonTypes as $buttonType => $buttonName) {
			if($buttonType == $type) {
				$selected[$buttonType] = 'selected';
				$display[$buttonType] = 'display: block;';
			}
			else {
				$selected[$buttonType] = '';
				$display[$buttonType] = 'display: none;';
			}
		}
	
		$document =JFactory::getDocument();
		$js = 'function ShowMenu(value)
					{
							var buttontype = document.getElementById(\'buttontype\');
							for (var i=0; i<buttontype.options.length; i++){
								if (buttontype.options[i].value==value){
									document.getElementById(\'div\' + buttontype.options[i].value).style.display = \'block\';
								}
								else {
									document.getElementById(\'div\' + buttontype.options[i].value).style.display = \'none\';
								}
							
							}
							
							var folder = value;
							var imgvalue = document.getElementById(value + \'img\').value;
							var file = "'.$imagefolder.'" + folder + "/" + imgvalue;
							document.getElementById(\'buttonimg\').src=file;
					}
							
				function ChangeImage(type,imgvalue,folder)
					{
						var file = "'.$imagefolder.'" + folder + "/" + imgvalue;

						if (imgvalue!=\'\') {
							document.getElementById(type).src=file;
						} else {
							document.getElementById(type).src=\'../images/blank.png\';
						}
					
					
					}
					';
					
		$document->addScriptDeclaration($js);

		
		$html = '<table width="100%">';
		$html .= '<tr>';
		$html .= '<td width="30">Type:<br /><br />Style:</td>';
		
		$html .= '<td width="150">';
		$html .= '<select id="buttontype" name="buttontype" onChange="javascript: ShowMenu(document.getElementById(\'buttontype\').value)" style="width:150px">';
		// Add Type Options
		foreach($buttonTypes as $buttonType => $buttonName) {
			$html .= '<option '.$selected[$buttonType].' value="'.$buttonType.'" >'.$buttonName.'</option>';
		}
		$html .= '</select>';
		$html .= '<br />';
		$html .= '<br />';
		// Add Style Options
		foreach($buttonTypes as $buttonType => $buttonName) {
			$html .='<div id="div'.$buttonType.'" style="'.$display[$buttonType].'">';
			$html .='<select id="'.$buttonType.'img" name="'.$buttonType.'img" onchange="javascript: ChangeImage(\'buttonimg\',document.getElementById(\''.$buttonType.'img\').value,\''.$buttonType.'\')" style="width:150px">';
			foreach($images[$buttonType] as $image) {
				if($image == $options['buttonimage']) $html .= '<option selected value="'.$image.'">'.$image.'</option>';
				else $html .= '<option value="'.$image.'">'.$image.'</option>';
			}
			$html .= '</select>';
			$html .= '</div>';
		}
		$html .= '</td>';
		// Image Preview
		$html .= '<td align="center">';
		$html .= '<img src="" id="buttonimg" name="buttonimg" border="2" alt="Preview" />';
		$html .= '<script language="javascript" type="text/javascript">
					var selectedIndex = document.getElementById(\'buttontype\').selectedIndex;
					var folder = document.getElementById(\'buttontype\').options[selectedIndex].value;
							
					var selectedIndex2 = document.getElementById(folder + \'img\' ).selectedIndex;
					var imgvalue = document.getElementById(folder + \'img\' ).options[selectedIndex2].value;
					var file = "'.$imagefolder.'" + folder + "/" + imgvalue;
					document.getElementById(\'buttonimg\').src=file;
				</script>';
		$html .= '</td></tr></table>';
		
		
		return $html;
	}
	
	function copyForm($tpl=null)
	{
		$mainframe = JFactory::getApplication();

		$this->_layout = 'copyaccess';

		JToolBarHelper::title( JText::_( 'JEasy File Sale - File Manager' ) . ': <small><small>[ '. JText::_( 'Copy User Access' ) .' ]</small></small>' );
		JToolBarHelper::custom( 'doCopy', 'copy.png', 'copy_f2.png', JText::_( 'Copy' ), false );
		JToolBarHelper::cancel();

		$document = JFactory::getDocument();
		$document->setTitle(JText::_('Copy User Access'));

		// Build the File select list
		$files = $this->get('FileList');
		$items = &$this->get('ItemsFromRequest');

		$this->assignRef('items', $items);
		$this->assignRef('files', $files);

		parent::display($tpl);
	}
	
	function jsonResponse($response)
	{
		if( function_exists('json_encode') )
		{
		   echo ( json_encode( $response ) );
		}
		else
		{
		   //--seems we are in PHP < 5.2... or json_encode() is disabled
		   require_once( JPATH_COMPONENT_ADMINISTRATOR.DS.'classes'.DS.'json.php' );
		   $json = new Services_JSON();
		   echo $json->encode( $response );
		}
	}
}// class
