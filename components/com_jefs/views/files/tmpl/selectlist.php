<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');
?>
<form action="index.php?option=com_jefs&amp;view=files&amp;tmpl=component&amp;object=<?php echo $this->object; ?>" method="post" name="adminForm">
<div id="editcell">
	<fieldset>
		<div style="float: left">
			<strong><?php echo JText::_('Click on the title or filename to insert a file item'); ?></strong>
		</div>
		<div style="float: right">
			<button type="button" onclick="window.parent.SqueezeBox.close();;"><?php echo JText::_('Cancel') ?></button>
		</div>
	</fieldset>
	
	<table>
		<tr>
			<td align="left" width="100%">
				<?php echo JText::_('Filter'); ?>:
				<input type="text" name="filter_search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
				<button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
				<button onclick="document.adminForm.filter_search.value='';this.form.submit();"><?php echo JText::_('Reset'); ?></button>
			</td>
			<td nowrap="nowrap">
				<?php echo $this->lists['state']; ?>
				<?php echo $this->lists['link_type']; ?>

			</td>
		</tr>
	</table>

	<table class="adminlist">
	<thead>
		<tr>	
			<th width="5">
						<?php echo JText::_( 'Num' ); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'Title' ), 'title', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'File Name' ), 'filename', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'Published' ), 'published', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th>
				<?php echo JHTML::_('grid.sort', JText::_( 'Link Type' ), 'link_type', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>
			<th width="5">
				<?php echo JHTML::_('grid.sort', JText::_( 'ID' ), 'id', $this->lists['order_Dir'], $this->lists['order']); ?>
			</th>	
		</tr>			
	</thead>
	<?php
	$k = 0;
	for ($i=0, $n=count( $this->items ); $i < $n; $i++)
	{
		$row = &$this->items[$i];
		$checked 	= JHTML::_('grid.id',   $i, $row->id );
		$published 	= JHTML::_('grid.published', $row, $i );
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td>
				<?php echo $this->pagination->getRowOffset( $i ); ?>
			</td>
			<td>
				<strong>
				<a style="cursor: pointer;" onclick="window.parent.jSelectChart_jform_request_fileid('<?php echo $row->id; ?>', '<?php echo str_replace(array("'", "\""), array("\\'", ""),$row->title); ?>', '<?php echo $this->object; ?>'); window.parent.SqueezeBox.close();;">
				<?php echo $row->title; ?></a></strong>
			</td>
			<td>
				<a style="cursor: pointer;" onclick="window.parent.jSelectChart_jform_request_fileid('<?php echo $row->id; ?>', '<?php echo str_replace(array("'", "\""), array("\\'", ""),$row->title); ?>', '<?php echo $this->object; ?>'); window.parent.SqueezeBox.close();;">
				<?php echo $row->filename; ?></a>
			</td>
			<td align="center">
				<?php echo $published;?>
			</td>
			<td>
				<?php echo $row->link_type; ?>
			</td>
			<td>
				<?php echo $row->id; ?>
			</td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
	 <tfoot>
    <tr>
      <td colspan="6">
      	<?php echo $this->pagination->getListFooter(); ?>
      </td>
    </tr>
  </tfoot>
	</table>
</div>

<input type="hidden" name="option" value="com_jefs" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="controller" value="files" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
