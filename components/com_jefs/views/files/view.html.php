<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport( 'joomla.application.component.view');

class jefsViewFiles extends JView
{

	function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();
        $option = JRequest::getCmd('option');
		$controller = JRequest::getWord( 'controller', 'config' );
	
		JToolBarHelper::title(   JText::_( 'JEasy File Sale - File Manager' ), 'generic.png' );
		JToolBarHelper::customX( 'copyItem', 'copy.png', 'copy_f2.png', JText::_( 'Copy File Item' ), true );
		JToolBarHelper::customX( 'copyAccess', 'copy.png', 'copy_f2.png', JText::_( 'Copy User Access' ), true );
		JToolBarHelper::deleteList();
		JToolBarHelper::editListX();
		JToolBarHelper::addNewX();
		
		// prepare list array
		$lists = array();
		// get the user state of the order and direction
		$filter_order = $mainframe->getUserStateFromRequest($option.$controller.'filter_order', 'filter_order', 'title');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($option.$controller.'filter_order_Dir', 'filter_order_Dir', 'ASC');
		// set the table order values
		$lists['order_Dir'] = $filter_order_Dir;
		$lists['order'] = $filter_order;

		// get the user-state of the published filter
		$filter_state = $mainframe->getUserStateFromRequest($option.$controller.'filter_state', 'filter_state');
		$filter_link_type = $mainframe->getUserStateFromRequest($option.$controller.'filter_link_type', 'filter_link_type');
		$filter_search = $mainframe->getUserStateFromRequest($option.$controller.'filter_search', 'filter_search');

		// set the table filter values
		$lists['state'] = JHTML::_('grid.state', $filter_state);
		
		$js = "onchange=\"if (this.options[selectedIndex].value!='')
       { document.adminForm.submit(); }\"";
		
		// prepare database
		$db =JFactory::getDBO();
		$query = ' SELECT DISTINCT(link_type) AS value, link_type AS text' .
				 ' FROM #__jefs_files' .
				 ' WHERE link_type != '.$db->quote('') .
				 ' ORDER BY link_type ASC';
		$db->setQuery($query);
		// add first 'select' option
		$options = array();
		$options[] = JHTML::_('select.option', '0', '- '.JText::_('Select Link Type').' -');
		// append database results
		$options = array_merge($options, (array)$db->loadObjectList());
		// build form control
		$lists['link_type'] = JHTML::_('select.genericlist', $options, 'filter_link_type', 'class="inputbox" size="1" '.$js, 'value', 'text', $filter_link_type);
		
		$lists['search'] = $filter_search;
		
		// add the lists array to the object
		$this->assignRef('lists', $lists);
		
		// Get data from the model
	 	$items =$this->get('Data');
	 	$pagination =$this->get('Pagination');
	
		// push data into the template
		$this->assignRef('items', $items);
		$this->assignRef('pagination', $pagination);
		
		// Request from editor button
		$ename = JRequest::getVar( 'e_name', false );
		if($ename) {
			$this->setLayout('list');
			$this->assignRef('ename', $ename);
		}
		
		// Request from module params
		$object = JRequest::getVar( 'object', false );
		if($object) {
			$this->setLayout('selectlist');
			$this->assignRef('object', $object);
		}

		parent::display($tpl);
	}//function

}//class