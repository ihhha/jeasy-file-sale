<?php
/**
 * @version		JEasy File Sale v1.0
 * @package		com_jefs
 * @copyright	Copyright (C) 2011 Joomalungma
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @author      Nikita Zonov {@link http://joomalungma.com}
 * @link 		mailto:info@joomalungma.com Support Email
 */

//--No direct access
defined('_JEXEC') or die('No Direct Access');

jimport('joomla.application.component.controller');

class jefsController extends JController
{
	function __construct()
	{
		parent::__construct();
		$this->registerTask('download', 'forceDownload');
		$this->registerTask('donate', 'purchase');
    }

	function forceDownload(){
		
		// Do a check to make sure the logged in user can download this file
//		$fileid = JRequest::getVar('acid', '0'); 
//		$model = $this->getModel("Download");
		$params =JComponentHelper::getParams('com_jefs');		// get params
		$loginlink = $params->get('loginlink', 'index.php?option=com_users&view=login');
        $allowNoRegister = $params->def('allowNoRegisterDownload',0);
        $input = new JInput();
		$fileid = $input->get('acid', '0','integer');
        $downloadHash = $input->get('downloadHash', '');

		$model = $this->getModel("Download");
        JEFSHelper::logEntry('Download hash = ' . $downloadHash, JLog::INFO, 'component');
        if(!empty($downloadHash))
        {
            JEFSHelper::logEntry('Calling getFileByDownloadHash', JLog::INFO, 'component');
            $file =& $model->getFileByDownloadHash($downloadHash);
        }
		else
        {
            JEFSHelper::logEntry('Calling getFile', JLog::INFO, 'component');
            $file =& $model->getFile($fileid, false);
        }

		if(!$file) return false;
		if($file->link_type == 'inherit') {
			$file = jefshelper::inheritParent($file, $file->parent);
			$accessid = $file->parent;
		}
		else $accessid = $file->id;
		$download = ($file->link_type == 'download') ? true : false;
		$register = ($file->link_type == 'register') ? true : false;
		$donate = ($file->link_type == 'donate') ? true : false;
		
		$requiredonatelogin = true;
		if($donate) {
			// if type=donate then lets set whether the user needs to be logged in based on minimum price
			$price_options = unserialize($file->price_options);
			$currency_code = $file->price_options['default'];
			if($currency_code == 'HUF' || $currency_code == 'JPY') $roundby = 0;	// These currencies require to be round to nearest whole number
			else $roundby = 2;
			$amount = number_format($file->price_options[$currency_code]['price'], $roundby, '.', '');	// make sure amount is a number
			if((int)$amount == 0) $requiredonatelogin = false;
		}

		// check user
		$user		=JFactory::getUser();
		$userid		= (int) $user->get('id', 0);
		
		// If the file isn't download only then we must check if user is logged in. If not, redirect.
		if(empty($userid) && $download == false && $requiredonatelogin == true && empty($downloadHash)) $this->setRedirect($loginlink, JText::_( 'JEFS_NOT_LOGIN_ERROR' ), 'error');
        else if(empty($userid) && $download == false && !empty($downloadHash))
        {
            $filepath = $file->path.DS.$file->filename;

            if (!$this->downloadFile($filepath,$file->id)) {
                JError::raiseError('', JText::_( 'JEFS_DOWNLOAD_FAILED' ));
            }
        }
		else {
			// check user access
			$db =JFactory::getDBO();

            $app = JFactory::getApplication();
            $menu = $app->getMenu();
            $item = $menu->getActive();
            $url = JRoute::_($item->link . '&Itemid=' . $item->id, false);

            $query = 'SELECT access FROM #__jefs_users WHERE id = '.$db->quote($userid);
			$db->setQuery( $query );
			$access = unserialize($db->loadResult());

			if(!$access) $access = array('files' => '');
			
			if(in_array($accessid,$access['files']) || $download == true || $register == true || $requiredonatelogin == false) {
				$filepath = $file->path.DS.$file->filename;
				
				if (!$this->downloadFile($filepath,$file->id)) {
                    JError::raiseError('', JText::_( 'JEFS_DOWNLOAD_FAILED' ));
                }
			}
			else {
				$this->setRedirect(JRoute::_($url), JText::_( "JEFS_USER_NOT_AUTHORIZED" ), 'error');
			}
		}
	}
	
	function downloadFile($filepath,$fileid)
	{	
        JEFSHelper::logEntry('Start download File:..', JLog::INFO, 'component');
		ob_clean();
		if (connection_status()!=0) return(FALSE);
		if (function_exists("set_time_limit")) @set_time_limit(0);

		$filename = basename($filepath);
        JEFSHelper::logEntry('File name = ' . $filename, JLog::INFO, 'component');
		$expirytime = gmdate("D, d M Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m"), date("d"), date("Y")));
        JEFSHelper::logEntry('Expirity time = ' . $expirytime, JLog::INFO, 'component');
		$modifiedtime = gmdate("D, d M Y H:i:s");
        JEFSHelper::logEntry('Modified time = ' . $modifiedtime, JLog::INFO, 'component');
		
		// Get mime content type
		if (function_exists('mime_content_type')) {
			$contentType = mime_content_type($filepath);
		}
		else if (function_exists('finfo_file')) {
		        $fileinfo    = finfo_open(FILEINFO_MIME);
		        $contentType = finfo_file($fileinfo, $filepath);
		        finfo_close($fileinfo);
		}
		else {
			$contentType = "application/octet-stream";
		}
        JEFSHelper::logEntry('Content type = ' . $contentType, JLog::INFO, 'component');
		// Make sure filename is not changed by Internet Explorer adding square brackets
		if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) $tempfilename = preg_replace('/\./', '%2e', $filename, substr_count($filename, '.') - 1);
		else $tempfilename = $filename;
        JEFSHelper::logEntry('Temp file name = ' . $tempfilename, JLog::INFO, 'component');

        header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header("Expires: ".$expirytime." GMT");
		header("Last-Modified: ".$modifiedtime." GMT");
		header("Content-Description: File Transfer");
		header("Content-Transfer-Encoding: binary");
		header('Content-Type: ' . $contentType);
		header("Content-Disposition: attachment; filename=\"$tempfilename\"");
		header("Accept-Ranges: bytes");

		$range = 0;
		$size=filesize($filepath);
        JEFSHelper::logEntry('File size = ' . $size, JLog::INFO, 'component');
        if(isset($_SERVER['HTTP_RANGE']))
		{
			list($a, $range)=explode("=",$_SERVER['HTTP_RANGE']);
			str_replace($range, "-", $range);
			$size2=$size-1;
			$new_length=$size-$range;
			header("HTTP/1.1 206 Partial Content");
			header("Content-Length: $new_length");
			header("Content-Range: bytes $range$size2/$size");
		}
		else
		{
			$size2=$size-1;
			header("HTTP/1.0 200 OK");
			header("Content-Range: bytes 0-$size2/$size");
			header("Content-Length: ".$size);
		}

		if ($size == 0 ) {
			JError::raiseError (500, 'File is Empty');
			exit;
		}
		set_magic_quotes_runtime(0);

		$fp=fopen("$filepath","rb");
		fseek($fp,$range);
		while(!feof($fp) && connection_status() == 0)
		{
			print(fread($fp,1024*8));
			flush();
			ob_flush();	
		}
		sleep(1);
		fclose($fp);
		
		// Add download to db
		$model = $this->getModel("jefs");
		$model->addDownload($fileid);
		
		return((connection_status()==0) and !connection_aborted());
	}
	
	function purchase()
	{
        $model = $this->getModel('jefs');
		$view =$this->getView( 'Download', 'html' );
		$view->setModel( $model, true );
		$view->purchaseRedirect();
    }
	
	function success()
	{
		$custom = urldecode(JRequest::getVar('custom',''));
		$currentSession = JSession::getInstance('none',array());
		$link = urldecode(JRequest::getVar('requesturl',''));
		$type = JRequest::getWord('type','purchase');
		if($custom != '') {
			$custom = explode('&',$custom);
			$urlvar = explode('=',$custom[2]);
			$url = $urlvar[1];
		}
		else $url =  $currentSession->get("aclastrequest");
		
		if($link != '') $url = $link;

//		$currentSession = JSession::getInstance('none',array());
//		$link = $currentSession->get("aclastrequest");
		if($type == 'donate') $msg = JText::_( "JEFS_DONATE_THANKS" );
		else if($type == 'purchase')$msg = JText::_( 'JEFS_ORDER_THANKS' );
        else if($type == 'unregistereddonate'){
            $msg = JText::_( "JEFS_DONATE_UNREG_THANKS" );
            $url = JURI::base();
        }
        else if($type == 'unregisteredpurchase')
        {
            $msg = JText::_( 'JEFS_ORDER_UNREG_THANKS' );
            $url = JURI::base();
        }
		$this->setRedirect(JRoute::_($url), $msg);
	}
	
	function cancel()
	{
		//$currentSession = JSession::getInstance('none',array());
		//$link = $currentSession->get("aclastrequest");
		$link = urldecode(JRequest::getVar('requesturl','index.php'));
		$msg = JText::_('JEFS_ORDER_CANCEL');
	
		$this->setRedirect(JRoute::_($link), $msg);
	}
    function validate()
    {
        jefshelper::logEntry('JEFS: Task Validate started',JLog::INFO,'component');
        $paymentplugin = JRequest::getVar('payment');
        if(empty($paymentplugin)) return;
        jefshelper::logEntry('JEFS: Payment plugin - ' . $paymentplugin ,JLog::INFO,'component');
        $className = 'plgjefs'.$paymentplugin;
        if(class_exists($className) && method_exists($className, 'validate'))
        {
            $dispatcher  =JDispatcher::getInstance();
            $plugin      = new $className($dispatcher, array());
            call_user_func_array(array($plugin, 'validate'), array());
        }
    }

    function display($cachable = false, $urlparams = false)
	{
		parent::display($cachable = false, $urlparams = false);
	}// function

    function test2()
    {
        $url = "http://www.mobilehtml5.ru/index.php/component/jefs/download/2";
        jefshelper::logEntry('JEFSHELPER: Test 2 STARTED'  ,JLog::INFO,'component');
        jefshelper::fileProcess('2','0','3.00','EUR','purchase',$url,$url,'PayPal','2aa701a1fddf51b285866e3f346c66f7');
        jefshelper::logEntry('JEFSHELPER: Test 2 FINISHED'  ,JLog::INFO,'component');
    }
	
}// class